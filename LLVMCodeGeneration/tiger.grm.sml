functor TigerLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Tiger_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
structure A = Absyn
structure S = Symbol

(* [these functions and similar ones may be convenient
 * for the creation of abstract syntax trees] *)

datatype lvaluePartSpec = Field of S.symbol
                        | Subscript of A.exp

fun makeLvaluePartSpec (v, pos, l::r) =
  (case l
    of Field idsym =>
       makeLvaluePartSpec (A.FieldVar (v, idsym, pos), pos, r)
     | Subscript exp =>
       makeLvaluePartSpec (A.SubscriptVar (v, exp, pos), pos,r))
  | makeLvaluePartSpec (v, _, nil) = v

fun makeBinop (exp1, bop, exp2, position) =
    A.OpExp  { 
              left = exp1
             , oper = bop
             , right = exp2
             , pos = position
             }

fun makeIf (et, en, el, p) =
    A.IfExp  {
       test = et
             , thn = en
             , els = el
             , pos = p
             }


fun makeLet (decs, bod, po) =
    A.LetExp {
               decls=decs
             , body=bod
             , pos=po
             }

fun makeArray (ty, si, ini, po) =
    A.ArrayExp { 
                 typ=ty
               , size=si
               , init=ini
               , pos=po
               }

fun makeAssign (va, ex, po)=
    A.AssignExp { 
                  var=va
                , exp=ex
                , pos=po
                }

fun makeVarDecl (idsym, ty, e, p) =
    A.VarDec { name = idsym
             , escape = ref true
             , typ = ty
             , init = e
             , pos = p}

fun makeTypeDec (nm, t, po) =
              { 
                name=nm
              , ty=t
              , pos=po
              } : A.tydecldata

fun makeRecord (fields_param, ty, po)=
    A.RecordExp { 
                  fields=fields_param
                , typ=ty
                , pos=po
                }

fun makeField (nm, ty, po) =
             { 
               name = nm
             , escape = ref true
             , typ = ty
             , pos = po
             } : A.fielddata

fun makeWhile (condition, stuff, posi) =
    A.WhileExp { 
                 test = condition
               , body = stuff
               , pos = posi
               }

fun makeFor (index, start_value, end_value, bod, po) =
    A.ForExp { 
              var=index
             , escape=ref true
             , lo=start_value
             , hi=end_value
             , body=bod
             , pos=po
             }

fun makeCall (fung, arg, po)=
    A.CallExp { 
                func=fung
              , args=arg
              , pos=po
              }

fun makeFundecl (fun_name, ps, rty, e, p) =
             { 
               name = fun_name
             , params = ps
             , result = rty
             , body = e
             , pos = p
             } : A.fundecldata




end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\000\000\000\000\
\\001\000\001\000\212\000\006\000\212\000\008\000\212\000\010\000\212\000\
\\012\000\212\000\014\000\212\000\016\000\032\000\017\000\031\000\
\\019\000\030\000\020\000\029\000\027\000\022\000\028\000\212\000\
\\029\000\212\000\033\000\212\000\034\000\212\000\037\000\212\000\
\\038\000\212\000\040\000\212\000\041\000\212\000\045\000\212\000\
\\046\000\212\000\047\000\212\000\000\000\
\\001\000\001\000\213\000\006\000\213\000\008\000\213\000\010\000\213\000\
\\012\000\213\000\014\000\213\000\016\000\032\000\017\000\031\000\
\\019\000\030\000\020\000\029\000\027\000\022\000\028\000\213\000\
\\029\000\213\000\033\000\213\000\034\000\213\000\037\000\213\000\
\\038\000\213\000\040\000\213\000\041\000\213\000\045\000\213\000\
\\046\000\213\000\047\000\213\000\000\000\
\\001\000\001\000\214\000\006\000\214\000\008\000\214\000\010\000\214\000\
\\012\000\214\000\014\000\214\000\016\000\032\000\017\000\031\000\
\\019\000\030\000\020\000\029\000\027\000\022\000\028\000\214\000\
\\029\000\214\000\033\000\214\000\034\000\214\000\037\000\214\000\
\\038\000\214\000\040\000\214\000\041\000\214\000\045\000\214\000\
\\046\000\214\000\047\000\214\000\000\000\
\\001\000\001\000\215\000\006\000\215\000\008\000\215\000\010\000\215\000\
\\012\000\215\000\014\000\215\000\016\000\032\000\017\000\031\000\
\\019\000\030\000\020\000\029\000\027\000\022\000\028\000\215\000\
\\029\000\215\000\033\000\215\000\034\000\215\000\037\000\215\000\
\\038\000\215\000\040\000\215\000\041\000\215\000\045\000\215\000\
\\046\000\215\000\047\000\215\000\000\000\
\\001\000\001\000\216\000\006\000\216\000\008\000\216\000\010\000\216\000\
\\012\000\216\000\014\000\216\000\016\000\032\000\017\000\031\000\
\\019\000\030\000\020\000\029\000\027\000\022\000\028\000\216\000\
\\029\000\216\000\033\000\216\000\034\000\216\000\037\000\216\000\
\\038\000\216\000\040\000\216\000\041\000\216\000\045\000\216\000\
\\046\000\216\000\047\000\216\000\000\000\
\\001\000\001\000\217\000\006\000\217\000\008\000\217\000\010\000\217\000\
\\012\000\217\000\014\000\217\000\016\000\032\000\017\000\031\000\
\\019\000\030\000\020\000\029\000\027\000\022\000\028\000\217\000\
\\029\000\217\000\033\000\217\000\034\000\217\000\037\000\217\000\
\\038\000\217\000\040\000\217\000\041\000\217\000\045\000\217\000\
\\046\000\217\000\047\000\217\000\000\000\
\\001\000\002\000\018\000\003\000\017\000\005\000\016\000\009\000\015\000\
\\010\000\048\000\017\000\014\000\032\000\013\000\035\000\012\000\
\\036\000\011\000\039\000\010\000\043\000\009\000\044\000\008\000\000\000\
\\001\000\002\000\018\000\003\000\017\000\005\000\016\000\009\000\015\000\
\\010\000\086\000\017\000\014\000\032\000\013\000\035\000\012\000\
\\036\000\011\000\039\000\010\000\043\000\009\000\044\000\008\000\000\000\
\\001\000\002\000\018\000\003\000\017\000\005\000\016\000\009\000\015\000\
\\017\000\014\000\032\000\013\000\035\000\012\000\036\000\011\000\
\\039\000\010\000\043\000\009\000\044\000\008\000\000\000\
\\001\000\002\000\042\000\000\000\
\\001\000\002\000\071\000\000\000\
\\001\000\002\000\072\000\000\000\
\\001\000\002\000\073\000\000\000\
\\001\000\002\000\080\000\000\000\
\\001\000\002\000\107\000\013\000\106\000\031\000\105\000\000\000\
\\001\000\002\000\109\000\000\000\
\\001\000\002\000\131\000\000\000\
\\001\000\002\000\136\000\000\000\
\\001\000\002\000\138\000\000\000\
\\001\000\002\000\140\000\000\000\
\\001\000\002\000\146\000\000\000\
\\001\000\002\000\151\000\000\000\
\\001\000\007\000\090\000\030\000\089\000\000\000\
\\001\000\007\000\124\000\000\000\
\\001\000\007\000\135\000\021\000\134\000\000\000\
\\001\000\007\000\149\000\000\000\
\\001\000\009\000\091\000\000\000\
\\001\000\010\000\077\000\000\000\
\\001\000\010\000\101\000\000\000\
\\001\000\010\000\123\000\000\000\
\\001\000\012\000\100\000\016\000\032\000\017\000\031\000\019\000\030\000\
\\020\000\029\000\021\000\028\000\022\000\027\000\023\000\026\000\
\\024\000\025\000\025\000\024\000\026\000\023\000\027\000\022\000\
\\028\000\021\000\029\000\020\000\000\000\
\\001\000\012\000\127\000\016\000\032\000\017\000\031\000\019\000\030\000\
\\020\000\029\000\021\000\028\000\022\000\027\000\023\000\026\000\
\\024\000\025\000\025\000\024\000\026\000\023\000\027\000\022\000\
\\028\000\021\000\029\000\020\000\000\000\
\\001\000\014\000\098\000\000\000\
\\001\000\014\000\132\000\000\000\
\\001\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\033\000\076\000\000\000\
\\001\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\037\000\112\000\000\000\
\\001\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\038\000\075\000\000\000\
\\001\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\038\000\137\000\000\000\
\\001\000\021\000\088\000\000\000\
\\001\000\021\000\099\000\000\000\
\\001\000\021\000\144\000\000\000\
\\001\000\021\000\145\000\000\000\
\\001\000\030\000\074\000\000\000\
\\001\000\030\000\122\000\000\000\
\\001\000\040\000\070\000\000\000\
\\001\000\041\000\103\000\000\000\
\\001\000\042\000\120\000\000\000\
\\154\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\155\000\000\000\
\\156\000\030\000\019\000\000\000\
\\157\000\000\000\
\\158\000\000\000\
\\159\000\000\000\
\\160\000\000\000\
\\161\000\027\000\022\000\000\000\
\\162\000\000\000\
\\163\000\000\000\
\\164\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\000\000\
\\165\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\000\000\
\\166\000\000\000\
\\167\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\168\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\169\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\170\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\034\000\113\000\000\000\
\\171\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\172\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\173\000\000\000\
\\174\000\000\000\
\\175\000\000\000\
\\176\000\000\000\
\\177\000\047\000\039\000\000\000\
\\178\000\045\000\041\000\046\000\040\000\047\000\039\000\000\000\
\\179\000\000\000\
\\180\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\181\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\182\000\000\000\
\\183\000\000\000\
\\184\000\000\000\
\\185\000\000\000\
\\186\000\000\000\
\\187\000\000\000\
\\188\000\002\000\018\000\003\000\017\000\005\000\016\000\009\000\015\000\
\\017\000\014\000\032\000\013\000\035\000\012\000\036\000\011\000\
\\039\000\010\000\043\000\009\000\044\000\008\000\000\000\
\\189\000\000\000\
\\190\000\008\000\079\000\016\000\032\000\017\000\031\000\019\000\030\000\
\\020\000\029\000\021\000\028\000\022\000\027\000\023\000\026\000\
\\024\000\025\000\025\000\024\000\026\000\023\000\027\000\022\000\
\\028\000\021\000\029\000\020\000\000\000\
\\191\000\000\000\
\\192\000\002\000\111\000\000\000\
\\193\000\000\000\
\\194\000\006\000\142\000\000\000\
\\195\000\000\000\
\\196\000\000\000\
\\197\000\009\000\053\000\011\000\052\000\013\000\051\000\015\000\050\000\000\000\
\\197\000\011\000\097\000\015\000\050\000\000\000\
\\197\000\011\000\097\000\015\000\050\000\042\000\118\000\000\000\
\\198\000\000\000\
\\199\000\000\000\
\\200\000\006\000\102\000\016\000\032\000\017\000\031\000\019\000\030\000\
\\020\000\029\000\021\000\028\000\022\000\027\000\023\000\026\000\
\\024\000\025\000\025\000\024\000\026\000\023\000\027\000\022\000\
\\028\000\021\000\029\000\020\000\000\000\
\\201\000\000\000\
\\202\000\000\000\
\\203\000\002\000\082\000\000\000\
\\204\000\000\000\
\\205\000\006\000\129\000\016\000\032\000\017\000\031\000\019\000\030\000\
\\020\000\029\000\021\000\028\000\022\000\027\000\023\000\026\000\
\\024\000\025\000\025\000\024\000\026\000\023\000\027\000\022\000\
\\028\000\021\000\029\000\020\000\000\000\
\\206\000\000\000\
\\207\000\019\000\030\000\020\000\029\000\027\000\022\000\000\000\
\\208\000\019\000\030\000\020\000\029\000\027\000\022\000\000\000\
\\209\000\027\000\022\000\000\000\
\\210\000\027\000\022\000\000\000\
\\211\000\000\000\
\\218\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\219\000\016\000\032\000\017\000\031\000\019\000\030\000\020\000\029\000\
\\021\000\028\000\022\000\027\000\023\000\026\000\024\000\025\000\
\\025\000\024\000\026\000\023\000\027\000\022\000\028\000\021\000\
\\029\000\020\000\000\000\
\\220\000\000\000\
\\221\000\000\000\
\"
val actionRowNumbers =
"\009\000\056\000\060\000\050\000\
\\068\000\048\000\051\000\067\000\
\\072\000\010\000\009\000\009\000\
\\009\000\007\000\054\000\049\000\
\\091\000\009\000\009\000\009\000\
\\009\000\009\000\009\000\009\000\
\\009\000\009\000\009\000\009\000\
\\009\000\009\000\009\000\070\000\
\\076\000\071\000\069\000\072\000\
\\045\000\011\000\012\000\013\000\
\\043\000\037\000\035\000\055\000\
\\028\000\084\000\053\000\090\000\
\\014\000\099\000\009\000\008\000\
\\062\000\059\000\058\000\107\000\
\\006\000\005\000\004\000\003\000\
\\002\000\001\000\106\000\105\000\
\\104\000\103\000\077\000\073\000\
\\082\000\039\000\023\000\027\000\
\\009\000\009\000\009\000\052\000\
\\083\000\009\000\092\000\033\000\
\\040\000\031\000\029\000\096\000\
\\110\000\046\000\015\000\009\000\
\\016\000\086\000\036\000\065\000\
\\064\000\084\000\094\000\009\000\
\\098\000\009\000\093\000\111\000\
\\009\000\057\000\081\000\047\000\
\\086\000\078\000\074\000\044\000\
\\030\000\024\000\009\000\009\000\
\\085\000\032\000\101\000\095\000\
\\009\000\097\000\017\000\034\000\
\\009\000\025\000\018\000\038\000\
\\063\000\092\000\100\000\019\000\
\\061\000\080\000\079\000\075\000\
\\009\000\020\000\088\000\009\000\
\\041\000\108\000\042\000\087\000\
\\021\000\066\000\009\000\009\000\
\\026\000\101\000\109\000\022\000\
\\102\000\088\000\089\000\000\000"
val gotoT =
"\
\\001\000\151\000\002\000\005\000\015\000\004\000\016\000\003\000\
\\018\000\002\000\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\036\000\006\000\035\000\007\000\034\000\008\000\033\000\
\\009\000\032\000\022\000\031\000\000\000\
\\000\000\
\\002\000\041\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\042\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\043\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\045\000\003\000\044\000\015\000\004\000\016\000\003\000\
\\018\000\002\000\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\017\000\047\000\000\000\
\\002\000\052\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\053\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\054\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\055\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\056\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\057\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\058\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\059\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\060\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\061\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\062\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\063\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\064\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\065\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\009\000\066\000\000\000\
\\000\000\
\\005\000\067\000\006\000\035\000\007\000\034\000\008\000\033\000\
\\009\000\032\000\022\000\031\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\076\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\019\000\079\000\000\000\
\\002\000\081\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\083\000\013\000\082\000\015\000\004\000\016\000\003\000\
\\018\000\002\000\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\045\000\003\000\085\000\015\000\004\000\016\000\003\000\
\\018\000\002\000\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\090\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\091\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\092\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\002\000\093\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\017\000\094\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\010\000\102\000\000\000\
\\002\000\106\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\011\000\108\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\112\000\000\000\
\\000\000\
\\002\000\113\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\002\000\114\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\017\000\115\000\000\000\
\\000\000\
\\002\000\083\000\013\000\117\000\015\000\004\000\016\000\003\000\
\\018\000\002\000\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\011\000\119\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\123\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\124\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\020\000\126\000\000\000\
\\000\000\
\\002\000\128\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\131\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\017\000\115\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\137\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\012\000\139\000\000\000\
\\002\000\141\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\145\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\002\000\146\000\015\000\004\000\016\000\003\000\018\000\002\000\
\\021\000\001\000\000\000\
\\000\000\
\\020\000\148\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\012\000\150\000\000\000\
\\000\000\
\\000\000\
\"
val numstates = 152
val numrules = 68
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | STRING of unit ->  (string) | REAL of unit ->  (real)
 | INT of unit ->  (int) | ID of unit ->  (string)
 | fundec of unit ->  (A.fundecldata list)
 | operator of unit ->  (A.exp)
 | record'' of unit ->  ( ( S.symbol * A.exp * A.pos )  list)
 | record' of unit ->  ( ( S.symbol * A.exp * A.pos )  list)
 | record of unit ->  (A.exp)
 | lvalue' of unit ->  (lvaluePartSpec list)
 | lvalue of unit ->  (A.var) | funcall of unit ->  (A.exp)
 | exp_list of unit ->  (A.exp)
 | params of unit ->  ( ( A.exp * A.pos )  list)
 | fields' of unit ->  (A.fielddata list)
 | fields of unit ->  (A.fielddata list) | ty of unit ->  (A.ty)
 | tydec' of unit ->  (A.tydecldata)
 | tydec of unit ->  (A.tydecldata list) | vardec of unit ->  (A.decl)
 | dec of unit ->  (A.decl) | decs of unit ->  (A.decl list)
 | seqexp' of unit ->  ( ( A.exp * A.pos )  list)
 | seqexp of unit ->  ( ( A.exp * A.pos )  list)
 | exp of unit ->  (A.exp) | program of unit ->  (A.exp)
end
type svalue = MlyValue.svalue
type result = A.exp
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn (T 34) => true | (T 35) => true | (T 36) => true | (T 42) => true
 | (T 38) => true | (T 39) => true | (T 40) => true | (T 44) => true
 | (T 45) => true | (T 46) => true | (T 30) => true | (T 31) => true
 | (T 32) => true | (T 33) => true | (T 37) => true | (T 41) => true
 | (T 43) => true | _ => false
val preferred_change : (term list * term list) list = 
(nil
,nil
 $$ (T 32))::
(nil
,nil
 $$ (T 33))::
(nil
,nil
 $$ (T 8))::
nil
val noShift = 
fn (T 0) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "ID"
  | (T 2) => "INT"
  | (T 3) => "REAL"
  | (T 4) => "STRING"
  | (T 5) => "COMMA"
  | (T 6) => "COLON"
  | (T 7) => "SEMICOLON"
  | (T 8) => "LPAREN"
  | (T 9) => "RPAREN"
  | (T 10) => "LBRACK"
  | (T 11) => "RBRACK"
  | (T 12) => "LBRACE"
  | (T 13) => "RBRACE"
  | (T 14) => "DOT"
  | (T 15) => "PLUS"
  | (T 16) => "MINUS"
  | (T 17) => "UMINUS"
  | (T 18) => "TIMES"
  | (T 19) => "DIVIDE"
  | (T 20) => "EQ"
  | (T 21) => "NEQ"
  | (T 22) => "LT"
  | (T 23) => "LE"
  | (T 24) => "GT"
  | (T 25) => "GE"
  | (T 26) => "CARET"
  | (T 27) => "AND"
  | (T 28) => "OR"
  | (T 29) => "ASSIGN"
  | (T 30) => "ARRAY"
  | (T 31) => "IF"
  | (T 32) => "THEN"
  | (T 33) => "ELSE"
  | (T 34) => "WHILE"
  | (T 35) => "FOR"
  | (T 36) => "TO"
  | (T 37) => "DO"
  | (T 38) => "LET"
  | (T 39) => "IN"
  | (T 40) => "END"
  | (T 41) => "OF"
  | (T 42) => "BREAK"
  | (T 43) => "NIL"
  | (T 44) => "FUNCTION"
  | (T 45) => "VAR"
  | (T 46) => "TYPE"
  | (T 47) => "LOWESTPREC"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn (T 1) => MlyValue.ID(fn () => ("bogus")) | 
(T 2) => MlyValue.INT(fn () => (1)) | 
(T 4) => MlyValue.STRING(fn () => ("")) | 
_ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 47) $$ (T 46) $$ (T 45) $$ (T 44) $$ (T 43) $$ (T 42) $$ (T 41)
 $$ (T 40) $$ (T 39) $$ (T 38) $$ (T 37) $$ (T 36) $$ (T 35) $$ (T 34)
 $$ (T 33) $$ (T 32) $$ (T 31) $$ (T 30) $$ (T 29) $$ (T 28) $$ (T 27)
 $$ (T 26) $$ (T 25) $$ (T 24) $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20)
 $$ (T 19) $$ (T 18) $$ (T 17) $$ (T 16) $$ (T 15) $$ (T 14) $$ (T 13)
 $$ (T 12) $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ 
(T 5) $$ (T 0)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.exp exp1, exp1left, exp1right)) :: rest671)
) => let val  result = MlyValue.program (fn _ => let val  (exp as exp1
) = exp1 ()
 in (exp)
end)
 in ( LrTable.NT 0, ( result, exp1left, exp1right), rest671)
end
|  ( 1, ( ( _, ( MlyValue.INT INT1, INT1left, INT1right)) :: rest671))
 => let val  result = MlyValue.exp (fn _ => let val  (INT as INT1) = 
INT1 ()
 in (A.IntExp(INT))
end)
 in ( LrTable.NT 1, ( result, INT1left, INT1right), rest671)
end
|  ( 2, ( ( _, ( MlyValue.lvalue lvalue1, lvalue1left, lvalue1right))
 :: rest671)) => let val  result = MlyValue.exp (fn _ => let val  (
lvalue as lvalue1) = lvalue1 ()
 in (A.VarExp(lvalue))
end)
 in ( LrTable.NT 1, ( result, lvalue1left, lvalue1right), rest671)
end
|  ( 3, ( ( _, ( _, NIL1left, NIL1right)) :: rest671)) => let val  
result = MlyValue.exp (fn _ => (A.NilExp))
 in ( LrTable.NT 1, ( result, NIL1left, NIL1right), rest671)
end
|  ( 4, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.seqexp 
seqexp1, _, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let val 
 result = MlyValue.exp (fn _ => let val  (seqexp as seqexp1) = seqexp1
 ()
 in (A.SeqExp(seqexp))
end)
 in ( LrTable.NT 1, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 5, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( _, LPAREN1left, _)) ::
 rest671)) => let val  result = MlyValue.exp (fn _ => (A.SeqExp([])))
 in ( LrTable.NT 1, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 6, ( ( _, ( MlyValue.STRING STRING1, (STRINGleft as STRING1left),
 STRING1right)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  (STRING as STRING1) = STRING1 ()
 in (A.StringExp(STRING, STRINGleft))
end)
 in ( LrTable.NT 1, ( result, STRING1left, STRING1right), rest671)
end
|  ( 7, ( ( _, ( MlyValue.exp exp1, expleft, exp1right)) :: ( _, ( _, 
MINUS1left, _)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  exp1 = exp1 ()
 in (makeBinop(A.IntExp(0), A.MinusOp, exp1, expleft))
end)
 in ( LrTable.NT 1, ( result, MINUS1left, exp1right), rest671)
end
|  ( 8, ( ( _, ( MlyValue.operator operator1, operator1left, 
operator1right)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  (operator as operator1) = operator1 ()
 in (operator)
end)
 in ( LrTable.NT 1, ( result, operator1left, operator1right), rest671)

end
|  ( 9, ( ( _, ( _, _, END1right)) :: ( _, ( MlyValue.seqexp seqexp1,
 _, _)) :: _ :: ( _, ( MlyValue.decs decs1, decsleft, _)) :: ( _, ( _,
 LET1left, _)) :: rest671)) => let val  result = MlyValue.exp (fn _ =>
 let val  (decs as decs1) = decs1 ()
 val  (seqexp as seqexp1) = seqexp1 ()
 in (makeLet(decs, A.SeqExp(seqexp), decsleft))
end)
 in ( LrTable.NT 1, ( result, LET1left, END1right), rest671)
end
|  ( 10, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeIf(exp1, exp2, SOME (A.IntExp(0)), exp1left))
end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 11, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeIf(exp1, A.IntExp(1), SOME exp2, exp1left))
end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.record record1, record1left, record1right))
 :: rest671)) => let val  result = MlyValue.exp (fn _ => let val  (
record as record1) = record1 ()
 in (record)
end)
 in ( LrTable.NT 1, ( result, record1left, record1right), rest671)
end
|  ( 13, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: _ :: ( _, 
( MlyValue.exp exp1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, (IDleft
 as ID1left), _)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  (ID as ID1) = ID1 ()
 val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeArray(S.symbol(ID), exp1, exp2, IDleft))
end)
 in ( LrTable.NT 1, ( result, ID1left, exp2right), rest671)
end
|  ( 14, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.lvalue lvalue1, (lvalueleft as lvalue1left), _)) :: rest671))
 => let val  result = MlyValue.exp (fn _ => let val  (lvalue as 
lvalue1) = lvalue1 ()
 val  (exp as exp1) = exp1 ()
 in (makeAssign(lvalue, exp, lvalueleft))
end)
 in ( LrTable.NT 1, ( result, lvalue1left, exp1right), rest671)
end
|  ( 15, ( ( _, ( MlyValue.exp exp3, _, exp3right)) :: _ :: ( _, ( 
MlyValue.exp exp2, _, _)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left,
 _)) :: ( _, ( _, IF1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 val  exp3 = exp3 ()
 in (makeIf(exp1, exp2, SOME exp3, exp1left))
end)
 in ( LrTable.NT 1, ( result, IF1left, exp3right), rest671)
end
|  ( 16, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: ( _, ( _, IF1left, _)) :: rest671)
) => let val  result = MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeIf(exp1, exp2, NONE, exp1left))
end)
 in ( LrTable.NT 1, ( result, IF1left, exp2right), rest671)
end
|  ( 17, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: ( _, ( _, WHILE1left, _)) :: 
rest671)) => let val  result = MlyValue.exp (fn _ => let val  exp1 = 
exp1 ()
 val  exp2 = exp2 ()
 in (makeWhile(exp1, exp2, exp1left))
end)
 in ( LrTable.NT 1, ( result, WHILE1left, exp2right), rest671)
end
|  ( 18, ( ( _, ( MlyValue.exp exp3, _, exp3right)) :: _ :: ( _, ( 
MlyValue.exp exp2, _, _)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left,
 _)) :: _ :: ( _, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, FOR1left, _))
 :: rest671)) => let val  result = MlyValue.exp (fn _ => let val  (ID
 as ID1) = ID1 ()
 val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 val  exp3 = exp3 ()
 in (makeFor(S.symbol(ID), exp1, exp2, exp3, exp1left))
end)
 in ( LrTable.NT 1, ( result, FOR1left, exp3right), rest671)
end
|  ( 19, ( ( _, ( _, (BREAKleft as BREAK1left), BREAK1right)) :: 
rest671)) => let val  result = MlyValue.exp (fn _ => (
A.BreakExp(BREAKleft)))
 in ( LrTable.NT 1, ( result, BREAK1left, BREAK1right), rest671)
end
|  ( 20, ( ( _, ( MlyValue.funcall funcall1, funcall1left, 
funcall1right)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  (funcall as funcall1) = funcall1 ()
 in (funcall)
end)
 in ( LrTable.NT 1, ( result, funcall1left, funcall1right), rest671)

end
|  ( 21, ( ( _, ( MlyValue.vardec vardec1, vardec1left, vardec1right))
 :: rest671)) => let val  result = MlyValue.dec (fn _ => let val  (
vardec as vardec1) = vardec1 ()
 in (vardec)
end)
 in ( LrTable.NT 5, ( result, vardec1left, vardec1right), rest671)
end
|  ( 22, ( ( _, ( MlyValue.fundec fundec1, fundec1left, fundec1right))
 :: rest671)) => let val  result = MlyValue.dec (fn _ => let val  (
fundec as fundec1) = fundec1 ()
 in (A.FunctionDec(fundec))
end)
 in ( LrTable.NT 5, ( result, fundec1left, fundec1right), rest671)
end
|  ( 23, ( ( _, ( MlyValue.tydec tydec1, tydec1left, tydec1right)) :: 
rest671)) => let val  result = MlyValue.dec (fn _ => let val  (tydec
 as tydec1) = tydec1 ()
 in (A.TypeDec(tydec))
end)
 in ( LrTable.NT 5, ( result, tydec1left, tydec1right), rest671)
end
|  ( 24, ( rest671)) => let val  result = MlyValue.decs (fn _ => ([]))
 in ( LrTable.NT 4, ( result, defaultPos, defaultPos), rest671)
end
|  ( 25, ( ( _, ( MlyValue.decs decs1, _, decs1right)) :: ( _, ( 
MlyValue.dec dec1, dec1left, _)) :: rest671)) => let val  result = 
MlyValue.decs (fn _ => let val  (dec as dec1) = dec1 ()
 val  (decs as decs1) = decs1 ()
 in (dec :: decs)
end)
 in ( LrTable.NT 4, ( result, dec1left, decs1right), rest671)
end
|  ( 26, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.ID ID1, _, _)) :: ( _, ( _, (VARleft as VAR1left), _)) :: 
rest671)) => let val  result = MlyValue.vardec (fn _ => let val  (ID
 as ID1) = ID1 ()
 val  (exp as exp1) = exp1 ()
 in (makeVarDecl(S.symbol(ID), NONE, exp, VARleft))
end)
 in ( LrTable.NT 6, ( result, VAR1left, exp1right), rest671)
end
|  ( 27, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.ID ID2, ID2left, _)) :: _ :: ( _, ( MlyValue.ID ID1, _, _))
 :: ( _, ( _, (VARleft as VAR1left), _)) :: rest671)) => let val  
result = MlyValue.vardec (fn _ => let val  ID1 = ID1 ()
 val  ID2 = ID2 ()
 val  (exp as exp1) = exp1 ()
 in (
makeVarDecl(S.symbol(ID1), SOME (S.symbol(ID2), ID2left), exp, VARleft)
)
end)
 in ( LrTable.NT 6, ( result, VAR1left, exp1right), rest671)
end
|  ( 28, ( ( _, ( MlyValue.tydec' tydec'1, tydec'1left, tydec'1right))
 :: rest671)) => let val  result = MlyValue.tydec (fn _ => let val  (
tydec' as tydec'1) = tydec'1 ()
 in ([tydec'])
end)
 in ( LrTable.NT 7, ( result, tydec'1left, tydec'1right), rest671)
end
|  ( 29, ( ( _, ( MlyValue.tydec' tydec'1, _, tydec'1right)) :: ( _, (
 MlyValue.tydec tydec1, tydec1left, _)) :: rest671)) => let val  
result = MlyValue.tydec (fn _ => let val  (tydec as tydec1) = tydec1
 ()
 val  (tydec' as tydec'1) = tydec'1 ()
 in (tydec @ [tydec'])
end)
 in ( LrTable.NT 7, ( result, tydec1left, tydec'1right), rest671)
end
|  ( 30, ( ( _, ( MlyValue.ID ID1, (IDleft as ID1left), ID1right)) :: 
rest671)) => let val  result = MlyValue.ty (fn _ => let val  (ID as 
ID1) = ID1 ()
 in (A.NameTy(S.symbol(ID), IDleft))
end)
 in ( LrTable.NT 9, ( result, ID1left, ID1right), rest671)
end
|  ( 31, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.fields 
fields1, _, _)) :: ( _, ( _, LBRACE1left, _)) :: rest671)) => let val 
 result = MlyValue.ty (fn _ => let val  (fields as fields1) = fields1
 ()
 in (A.RecordTy(fields))
end)
 in ( LrTable.NT 9, ( result, LBRACE1left, RBRACE1right), rest671)
end
|  ( 32, ( ( _, ( MlyValue.ID ID1, IDleft, ID1right)) :: _ :: ( _, ( _
, ARRAY1left, _)) :: rest671)) => let val  result = MlyValue.ty (fn _
 => let val  (ID as ID1) = ID1 ()
 in (A.ArrayTy(S.symbol(ID), IDleft))
end)
 in ( LrTable.NT 9, ( result, ARRAY1left, ID1right), rest671)
end
|  ( 33, ( ( _, ( MlyValue.ty ty1, _, ty1right)) :: _ :: ( _, ( 
MlyValue.ID ID1, IDleft, _)) :: ( _, ( _, TYPE1left, _)) :: rest671))
 => let val  result = MlyValue.tydec' (fn _ => let val  (ID as ID1) = 
ID1 ()
 val  (ty as ty1) = ty1 ()
 in (makeTypeDec(S.symbol(ID), ty, IDleft))
end)
 in ( LrTable.NT 8, ( result, TYPE1left, ty1right), rest671)
end
|  ( 34, ( rest671)) => let val  result = MlyValue.seqexp (fn _ => ([]
))
 in ( LrTable.NT 2, ( result, defaultPos, defaultPos), rest671)
end
|  ( 35, ( ( _, ( MlyValue.seqexp' seqexp'1, _, seqexp'1right)) :: ( _
, ( MlyValue.exp exp1, (expleft as exp1left), _)) :: rest671)) => let
 val  result = MlyValue.seqexp (fn _ => let val  (exp as exp1) = exp1
 ()
 val  (seqexp' as seqexp'1) = seqexp'1 ()
 in ((exp, expleft) :: seqexp')
end)
 in ( LrTable.NT 2, ( result, exp1left, seqexp'1right), rest671)
end
|  ( 36, ( rest671)) => let val  result = MlyValue.seqexp' (fn _ => (
[]))
 in ( LrTable.NT 3, ( result, defaultPos, defaultPos), rest671)
end
|  ( 37, ( ( _, ( MlyValue.seqexp' seqexp'1, _, seqexp'1right)) :: ( _
, ( MlyValue.exp exp1, expleft, _)) :: ( _, ( _, SEMICOLON1left, _))
 :: rest671)) => let val  result = MlyValue.seqexp' (fn _ => let val 
 (exp as exp1) = exp1 ()
 val  (seqexp' as seqexp'1) = seqexp'1 ()
 in ((exp, expleft) :: seqexp')
end)
 in ( LrTable.NT 3, ( result, SEMICOLON1left, seqexp'1right), rest671)

end
|  ( 38, ( rest671)) => let val  result = MlyValue.fields (fn _ => ([]
))
 in ( LrTable.NT 10, ( result, defaultPos, defaultPos), rest671)
end
|  ( 39, ( ( _, ( MlyValue.fields' fields'1, _, fields'1right)) :: ( _
, ( MlyValue.ID ID2, ID2left, _)) :: _ :: ( _, ( MlyValue.ID ID1, 
ID1left, _)) :: rest671)) => let val  result = MlyValue.fields (fn _
 => let val  ID1 = ID1 ()
 val  ID2 = ID2 ()
 val  (fields' as fields'1) = fields'1 ()
 in (
makeField(S.symbol(ID1), (S.symbol(ID2), ID2left), ID1left) :: fields'
)
end)
 in ( LrTable.NT 10, ( result, ID1left, fields'1right), rest671)
end
|  ( 40, ( rest671)) => let val  result = MlyValue.fields' (fn _ => (
[]))
 in ( LrTable.NT 11, ( result, defaultPos, defaultPos), rest671)
end
|  ( 41, ( ( _, ( MlyValue.fields' fields'1, _, fields'1right)) :: ( _
, ( MlyValue.ID ID2, ID2left, _)) :: _ :: ( _, ( MlyValue.ID ID1, 
ID1left, _)) :: ( _, ( _, COMMA1left, _)) :: rest671)) => let val  
result = MlyValue.fields' (fn _ => let val  ID1 = ID1 ()
 val  ID2 = ID2 ()
 val  (fields' as fields'1) = fields'1 ()
 in (
makeField(S.symbol(ID1), (S.symbol(ID2), ID2left), ID1left) :: fields'
)
end)
 in ( LrTable.NT 11, ( result, COMMA1left, fields'1right), rest671)

end
|  ( 42, ( ( _, ( MlyValue.lvalue' lvalue'1, _, lvalue'1right)) :: ( _
, ( MlyValue.ID ID1, (IDleft as ID1left), _)) :: rest671)) => let val 
 result = MlyValue.lvalue (fn _ => let val  (ID as ID1) = ID1 ()
 val  (lvalue' as lvalue'1) = lvalue'1 ()
 in (
makeLvaluePartSpec(A.SimpleVar(S.symbol(ID), IDleft), IDleft, lvalue')
)
end)
 in ( LrTable.NT 15, ( result, ID1left, lvalue'1right), rest671)
end
|  ( 43, ( rest671)) => let val  result = MlyValue.lvalue' (fn _ => (
[]))
 in ( LrTable.NT 16, ( result, defaultPos, defaultPos), rest671)
end
|  ( 44, ( ( _, ( MlyValue.lvalue' lvalue'1, _, lvalue'1right)) :: ( _
, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, DOT1left, _)) :: rest671)) =>
 let val  result = MlyValue.lvalue' (fn _ => let val  (ID as ID1) = 
ID1 ()
 val  (lvalue' as lvalue'1) = lvalue'1 ()
 in (Field(S.symbol(ID)) :: lvalue')
end)
 in ( LrTable.NT 16, ( result, DOT1left, lvalue'1right), rest671)
end
|  ( 45, ( ( _, ( MlyValue.lvalue' lvalue'1, _, lvalue'1right)) :: _
 :: ( _, ( MlyValue.exp exp1, _, _)) :: ( _, ( _, LBRACK1left, _)) :: 
rest671)) => let val  result = MlyValue.lvalue' (fn _ => let val  (exp
 as exp1) = exp1 ()
 val  (lvalue' as lvalue'1) = lvalue'1 ()
 in (Subscript(exp) :: lvalue')
end)
 in ( LrTable.NT 16, ( result, LBRACK1left, lvalue'1right), rest671)

end
|  ( 46, ( ( _, ( MlyValue.exp exp1, (expleft as exp1left), exp1right)
) :: rest671)) => let val  result = MlyValue.params (fn _ => let val 
 (exp as exp1) = exp1 ()
 in ((exp, expleft) :: [])
end)
 in ( LrTable.NT 12, ( result, exp1left, exp1right), rest671)
end
|  ( 47, ( ( _, ( MlyValue.params params1, _, params1right)) :: _ :: (
 _, ( MlyValue.exp exp1, (expleft as exp1left), _)) :: rest671)) =>
 let val  result = MlyValue.params (fn _ => let val  (exp as exp1) = 
exp1 ()
 val  (params as params1) = params1 ()
 in ((exp, expleft) :: params)
end)
 in ( LrTable.NT 12, ( result, exp1left, params1right), rest671)
end
|  ( 48, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.record' 
record'1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, (IDleft as ID1left),
 _)) :: rest671)) => let val  result = MlyValue.record (fn _ => let
 val  (ID as ID1) = ID1 ()
 val  (record' as record'1) = record'1 ()
 in (makeRecord(record', S.symbol(ID), IDleft))
end)
 in ( LrTable.NT 17, ( result, ID1left, RBRACE1right), rest671)
end
|  ( 49, ( rest671)) => let val  result = MlyValue.record' (fn _ => (
[]))
 in ( LrTable.NT 18, ( result, defaultPos, defaultPos), rest671)
end
|  ( 50, ( ( _, ( MlyValue.record'' record''1, _, record''1right)) :: 
( _, ( MlyValue.exp exp1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, (
IDleft as ID1left), _)) :: rest671)) => let val  result = 
MlyValue.record' (fn _ => let val  (ID as ID1) = ID1 ()
 val  (exp as exp1) = exp1 ()
 val  (record'' as record''1) = record''1 ()
 in ((S.symbol(ID), exp, IDleft) :: record'')
end)
 in ( LrTable.NT 18, ( result, ID1left, record''1right), rest671)
end
|  ( 51, ( rest671)) => let val  result = MlyValue.record'' (fn _ => (
[]))
 in ( LrTable.NT 19, ( result, defaultPos, defaultPos), rest671)
end
|  ( 52, ( ( _, ( MlyValue.record'' record''1, _, record''1right)) :: 
( _, ( MlyValue.exp exp1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, 
IDleft, _)) :: ( _, ( _, COMMA1left, _)) :: rest671)) => let val  
result = MlyValue.record'' (fn _ => let val  (ID as ID1) = ID1 ()
 val  (exp as exp1) = exp1 ()
 val  (record'' as record''1) = record''1 ()
 in ((S.symbol(ID), exp, IDleft) :: record'')
end)
 in ( LrTable.NT 19, ( result, COMMA1left, record''1right), rest671)

end
|  ( 53, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.PlusOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 54, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.MinusOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 55, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.TimesOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 56, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.DivideOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 57, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.ExponentOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 58, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.EqOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 59, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.NeqOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 60, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.LtOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 61, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.LeOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 62, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.GtOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 63, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.operator (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (makeBinop(exp1, A.GeOp, exp2, exp1left))
end)
 in ( LrTable.NT 20, ( result, exp1left, exp2right), rest671)
end
|  ( 64, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: _ :: ( _, 
( MlyValue.fields fields1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, _, _
)) :: ( _, ( _, (FUNCTIONleft as FUNCTION1left), _)) :: rest671)) =>
 let val  result = MlyValue.fundec (fn _ => let val  (ID as ID1) = ID1
 ()
 val  (fields as fields1) = fields1 ()
 val  (exp as exp1) = exp1 ()
 in (makeFundecl(S.symbol(ID), fields, NONE, exp, FUNCTIONleft) :: [])

end)
 in ( LrTable.NT 21, ( result, FUNCTION1left, exp1right), rest671)
end
|  ( 65, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.ID ID2, ID2left, _)) :: _ :: _ :: ( _, ( MlyValue.fields 
fields1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, (
FUNCTIONleft as FUNCTION1left), _)) :: rest671)) => let val  result = 
MlyValue.fundec (fn _ => let val  ID1 = ID1 ()
 val  (fields as fields1) = fields1 ()
 val  ID2 = ID2 ()
 val  (exp as exp1) = exp1 ()
 in (
makeFundecl(S.symbol(ID1), fields, SOME (S.symbol(ID2), ID2left), exp, FUNCTIONleft) :: []
)
end)
 in ( LrTable.NT 21, ( result, FUNCTION1left, exp1right), rest671)
end
|  ( 66, ( ( _, ( _, _, RPAREN1right)) :: _ :: ( _, ( MlyValue.ID ID1,
 (IDleft as ID1left), _)) :: rest671)) => let val  result = 
MlyValue.funcall (fn _ => let val  (ID as ID1) = ID1 ()
 in (makeCall(S.symbol(ID), [], IDleft))
end)
 in ( LrTable.NT 14, ( result, ID1left, RPAREN1right), rest671)
end
|  ( 67, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.params 
params1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, (IDleft as ID1left), _
)) :: rest671)) => let val  result = MlyValue.funcall (fn _ => let
 val  (ID as ID1) = ID1 ()
 val  (params as params1) = params1 ()
 in (makeCall(S.symbol(ID), params, IDleft))
end)
 in ( LrTable.NT 14, ( result, ID1left, RPAREN1right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : Tiger_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.ID (fn () => i),p1,p2))
fun INT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.INT (fn () => i),p1,p2))
fun REAL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.REAL (fn () => i),p1,p2))
fun STRING (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.STRING (fn () => i),p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun COLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMICOLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun DOT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun UMINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun DIVIDE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun NEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun LT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun LE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun GT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun GE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.VOID,p1,p2))
fun CARET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VOID,p1,p2))
fun AND (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun OR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun ASSIGN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun ARRAY (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.VOID,p1,p2))
fun WHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 34,(
ParserData.MlyValue.VOID,p1,p2))
fun FOR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 35,(
ParserData.MlyValue.VOID,p1,p2))
fun TO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 36,(
ParserData.MlyValue.VOID,p1,p2))
fun DO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 37,(
ParserData.MlyValue.VOID,p1,p2))
fun LET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 38,(
ParserData.MlyValue.VOID,p1,p2))
fun IN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 39,(
ParserData.MlyValue.VOID,p1,p2))
fun END (p1,p2) = Token.TOKEN (ParserData.LrTable.T 40,(
ParserData.MlyValue.VOID,p1,p2))
fun OF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 41,(
ParserData.MlyValue.VOID,p1,p2))
fun BREAK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 42,(
ParserData.MlyValue.VOID,p1,p2))
fun NIL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 43,(
ParserData.MlyValue.VOID,p1,p2))
fun FUNCTION (p1,p2) = Token.TOKEN (ParserData.LrTable.T 44,(
ParserData.MlyValue.VOID,p1,p2))
fun VAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 45,(
ParserData.MlyValue.VOID,p1,p2))
fun TYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 46,(
ParserData.MlyValue.VOID,p1,p2))
fun LOWESTPREC (p1,p2) = Token.TOKEN (ParserData.LrTable.T 47,(
ParserData.MlyValue.VOID,p1,p2))
end
end
