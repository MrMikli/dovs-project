type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token

type lexresult = (svalue, pos) token

val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos

val inside_string = ref 0
val comment_nest_level = ref 0

val stringBuffer : string ref = ref ""
val stringStart = ref 0

fun dopos token yypos yylen = token (yypos, yypos + yylen)
fun dopos3 token value yypos yylen = token (value, yypos, yypos + yylen)

fun convertFromAscii str =
        let val subStr = String.substring(str, 1, 3)
            val intVal = valOf(Int.fromString(subStr))
            val charVal = chr intVal
        in Char.toString charVal end

fun s2i t pos =
    let
        val opti = (Int.fromString t)
        handle Overflow =>
                   (ErrorMsg.error pos "Integer too large"; SOME 0)
        fun s2i_aux NONE = (ErrorMsg.error pos "Ill-formed integer"; 0)
          | s2i_aux (SOME n) = n
    in
        s2i_aux opti
    end

fun eof () =
    let
        val pos = hd (!linePos)
    in
        if(!comment_nest_level > 0) then ErrorMsg.error pos ("Error Unclosed Comment")
        else if (!inside_string = 1) then ErrorMsg.error pos ("Error Unclosed String")
        else();

        ErrorMsg.lineNum := 1;
        comment_nest_level := 0;
        Tokens.EOF (pos,pos)
	end

%%

%header (functor TigerLexFun (structure Tokens: Tiger_TOKENS));

idchars    = [A-Za-z’_0-9];
id         = [A-Za-z]{idchars}*;
digit      = [0-9];
digits     = {digit}+;
whitespace = [\ \t]+;
escchars=[nt\"\\];
eschatchars=[\^][A-Z@\[\]\\\^_];

%s COMMENT STRING ESCAPE;
%full
%%

"\n"	                          => (lineNum := !lineNum+1;
                                   linePos := yypos :: !linePos;
                                   continue());

<INITIAL>{whitespace}           => (continue());
<INITIAL>"var"                  => (dopos Tokens.VAR yypos 3);
<INITIAL>{digits}               => (dopos3 Tokens.INT (s2i yytext yypos) yypos (size yytext));
<INITIAL>"while"                => (dopos Tokens.WHILE yypos 5);
<INITIAL>"for"                  => (dopos Tokens.FOR yypos 3);
<INITIAL>"to"                   => (dopos Tokens.TO yypos 2);
<INITIAL>"break"                => (dopos Tokens.BREAK yypos 5);
<INITIAL>"let"                  => (dopos Tokens.LET yypos 3);
<INITIAL>"in"                   => (dopos Tokens.IN yypos 2);
<INITIAL>"end"                  => (dopos Tokens.END yypos 3);
<INITIAL>"function"             => (dopos Tokens.FUNCTION yypos 8);
<INITIAL>"var"                  => (dopos Tokens.VAR yypos 3);
<INITIAL>"type"                 => (dopos Tokens.TYPE yypos 4);
<INITIAL>"array"                => (dopos Tokens.ARRAY yypos 5);
<INITIAL>"if"                   => (dopos Tokens.IF yypos 2);
<INITIAL>"then"                 => (dopos Tokens.THEN yypos 4);
<INITIAL>"else"                 => (dopos Tokens.ELSE yypos 4);
<INITIAL>"do"                   => (dopos Tokens.DO yypos 2);
<INITIAL>"of"                   => (dopos Tokens.OF yypos 2);
<INITIAL>"nil"                  => (dopos Tokens.NIL yypos 3);
<INITIAL>"-"                    => (dopos Tokens.MINUS yypos 1);
<INITIAL>"("                    => (dopos Tokens.LPAREN yypos 1);
<INITIAL>")"                    => (dopos Tokens.RPAREN yypos 1);
<INITIAL>"{"                    => (dopos Tokens.LBRACE yypos 1);
<INITIAL>"}"                    => (dopos Tokens.RBRACE yypos 1);
<INITIAL>"]"                    => (dopos Tokens.RBRACK yypos 1);
<INITIAL>"["                    => (dopos Tokens.LBRACK yypos 1);
<INITIAL>","                    => (dopos Tokens.COMMA yypos 1);
<INITIAL>"."                    => (dopos Tokens.DOT yypos 1);
<INITIAL>";"                    => (dopos Tokens.SEMICOLON yypos 1);
<INITIAL>":"                    => (dopos Tokens.COLON yypos 1);
<INITIAL>"^"                    => (dopos Tokens.CARET yypos 1);
<INITIAL>"/"                    => (dopos Tokens.DIVIDE yypos 1);
<INITIAL>"&"                    => (dopos Tokens.AND yypos 1);
<INITIAL>"|"                    => (dopos Tokens.OR yypos 1);
<INITIAL>{id}                   => (dopos3 Tokens.ID yytext yypos (size yytext));
<INITIAL>">="                   => (dopos Tokens.GE yypos 2);
<INITIAL>"*"                    => (dopos Tokens.TIMES yypos 1);
<INITIAL>"+"                    => (dopos Tokens.PLUS yypos 1);
<INITIAL>"="                    => (dopos Tokens.EQ yypos 1);
<INITIAL>">"                    => (dopos Tokens.GT yypos 1);
<INITIAL>"<="                   => (dopos Tokens.LE yypos 2);
<INITIAL>"<>"                   => (dopos Tokens.NEQ yypos 2);
<INITIAL>"<"                    => (dopos Tokens.LT yypos 1);
<INITIAL>":="                   => (dopos Tokens.ASSIGN yypos 2);

<INITIAL>\"                     => (inside_string := 1;
				                            YYBEGIN STRING;
                                    stringStart := yypos; stringBuffer := "";
				                            continue());

<STRING>[ -!#-\[\]-~]*          => (stringBuffer := !stringBuffer ^ yytext; continue());
<STRING>\\n                     => (stringBuffer := !stringBuffer ^ "\n"; continue());
<STRING>\\t                     => (stringBuffer := !stringBuffer ^ "\t"; continue());
<STRING>\\\"                    => (stringBuffer := !stringBuffer ^ "\""; continue());
<STRING>\\\\                    => (stringBuffer := !stringBuffer ^ "\\"; continue());
<STRING>\\[0-9][0-9][0-9]       => (stringBuffer := !stringBuffer ^ convertFromAscii(yytext); continue());
<STRING>\\[\n\t \f]+\\          => (continue());
<STRING>\"                      => (inside_string := 0; YYBEGIN INITIAL; Tokens.STRING(!stringBuffer, !stringStart, yypos));
<STRING>\n                      => (ErrorMsg.error yypos ("illegal newline character "); continue());
<STRING>.                       => (ErrorMsg.error yypos ("illegal character " ^ yytext); continue());

<INITIAL>.                      => (ErrorMsg.error yypos ("illegal character " ^ yytext); continue());

<INITIAL,COMMENT>"/*"           => (comment_nest_level := (!comment_nest_level + 1);
				                            if (!comment_nest_level = 1) then YYBEGIN COMMENT else ();
				                            continue());
<COMMENT>"*/"                   => (comment_nest_level := (!comment_nest_level - 1);
				                            if (!comment_nest_level = 0) then YYBEGIN INITIAL else ();
				                            continue());
<COMMENT>.                      => (continue());

.                               => (ErrorMsg.error yypos (" illegal char " ^ yytext);
                                   (*the point should always be the last rule*)
                                   continue());
