structure A = Absyn
structure S = Symbol

(* [these functions and similar ones may be convenient
 * for the creation of abstract syntax trees] *)

datatype lvaluePartSpec = Field of S.symbol
                        | Subscript of A.exp

fun makeLvaluePartSpec (v, pos, l::r) =
  (case l
    of Field idsym =>
       makeLvaluePartSpec (A.FieldVar (v, idsym, pos), pos, r)
     | Subscript exp =>
       makeLvaluePartSpec (A.SubscriptVar (v, exp, pos), pos,r))
  | makeLvaluePartSpec (v, _, nil) = v

fun makeBinop (exp1, bop, exp2, position) =
    A.OpExp  { 
              left = exp1
             , oper = bop
             , right = exp2
             , pos = position
             }

fun makeIf (et, en, el, p) =
    A.IfExp  {
       test = et
             , thn = en
             , els = el
             , pos = p
             }


fun makeLet (decs, bod, po) =
    A.LetExp {
               decls=decs
             , body=bod
             , pos=po
             }

fun makeArray (ty, si, ini, po) =
    A.ArrayExp { 
                 typ=ty
               , size=si
               , init=ini
               , pos=po
               }

fun makeAssign (va, ex, po)=
    A.AssignExp { 
                  var=va
                , exp=ex
                , pos=po
                }

fun makeVarDecl (idsym, ty, e, p) =
    A.VarDec { name = idsym
             , escape = ref true
             , typ = ty
             , init = e
             , pos = p}

fun makeTypeDec (nm, t, po) =
              { 
                name=nm
              , ty=t
              , pos=po
              } : A.tydecldata

fun makeRecord (fields_param, ty, po)=
    A.RecordExp { 
                  fields=fields_param
                , typ=ty
                , pos=po
                }

fun makeField (nm, ty, po) =
             { 
               name = nm
             , escape = ref true
             , typ = ty
             , pos = po
             } : A.fielddata

fun makeWhile (condition, stuff, posi) =
    A.WhileExp { 
                 test = condition
               , body = stuff
               , pos = posi
               }

fun makeFor (index, start_value, end_value, bod, po) =
    A.ForExp { 
              var=index
             , escape=ref true
             , lo=start_value
             , hi=end_value
             , body=bod
             , pos=po
             }

fun makeCall (fung, arg, po)=
    A.CallExp { 
                func=fung
              , args=arg
              , pos=po
              }

fun makeFundecl (fun_name, ps, rty, e, p) =
             { 
               name = fun_name
             , params = ps
             , result = rty
             , body = e
             , pos = p
             } : A.fundecldata



%%
%term
    EOF
  | ID of string
  | INT of int 
  | REAL of real
  | STRING of string
  | COMMA | COLON | SEMICOLON | LPAREN | RPAREN | LBRACK | RBRACK
  | LBRACE | RBRACE | DOT
  | PLUS | MINUS | UMINUS | TIMES | DIVIDE | EQ | NEQ | LT | LE | GT | GE
  | CARET | AND | OR | ASSIGN
  | ARRAY | IF | THEN | ELSE | WHILE | FOR | TO | DO | LET | IN | END | OF
  | BREAK | NIL
  | FUNCTION | VAR | TYPE | LOWESTPREC

(* nonterminals; semantic values *)

%nonterm program of A.exp 
       | exp of A.exp | seqexp of (A.exp * A.pos) list | seqexp' of (A.exp * A.pos) list 
       | decs of A.decl list | dec of A.decl
       | vardec of A.decl 
       | tydec of A.tydecldata list |  tydec' of A.tydecldata
       | ty of A.ty 
       | fields of A.fielddata list
       | fields' of A.fielddata list 
       | params of (A.exp * A.pos) list | exp_list of A.exp | funcall of A.exp
       | lvalue of A.var | lvalue' of lvaluePartSpec list
       | record of A.exp | record' of (S.symbol * A.exp * A.pos) list
       | record'' of (S.symbol * A.exp * A.pos) list | operator of A.exp
       | fundec of A.fundecldata list


%pos int
%verbose
%start program
%eop EOF
%noshift EOF

%name Tiger

%keyword WHILE FOR TO BREAK LET IN END FUNCTION
         VAR TYPE ARRAY IF THEN ELSE DO OF NIL

%prefer THEN ELSE LPAREN

%value ID ("bogus")
%value INT (1)
%value STRING ("")

(* precedence/associativity, least tight binding first *)
%nonassoc   LOWESTPREC

%nonassoc   FUNCTION VAR TYPE THEN DO OF ASSIGN

%right      ELSE

%left       OR
%left       AND

%nonassoc   EQ NEQ LT GT LE GE

%left       PLUS MINUS
%left       TIMES DIVIDE
%left       UMINUS
%left       CARET


%%

(* Top level constructs *)

program: exp                                                (exp)

exp: INT                                                    (A.IntExp(INT))
   | lvalue                                                 (A.VarExp(lvalue))
   | NIL                                                    (A.NilExp)
   | LPAREN seqexp RPAREN                                   (A.SeqExp(seqexp))
   | LPAREN RPAREN                                          (A.SeqExp([]))
   | STRING                                                 (A.StringExp(STRING, STRINGleft))
   | MINUS exp %prec UMINUS                                 (makeBinop(A.IntExp(0), A.MinusOp, exp1, expleft))
   | operator                                               (operator)
   | LET decs IN seqexp END                                 (makeLet(decs, A.SeqExp(seqexp), decsleft))
   | exp AND exp                                            (makeIf(exp1, exp2, SOME (A.IntExp(0)), exp1left))
   | exp OR exp                                             (makeIf(exp1, A.IntExp(1), SOME exp2, exp1left))
   | record                                                 (record)
   | ID LBRACK exp RBRACK OF exp                            (makeArray(S.symbol(ID), exp1, exp2, IDleft))
   | lvalue ASSIGN exp                                      (makeAssign(lvalue, exp, lvalueleft))
   | IF exp THEN exp ELSE exp                               (makeIf(exp1, exp2, SOME exp3, exp1left))
   | IF exp THEN exp                                        (makeIf(exp1, exp2, NONE, exp1left))
   | WHILE exp DO exp                                       (makeWhile(exp1, exp2, exp1left))
   | FOR ID ASSIGN exp TO exp DO exp                        (makeFor(S.symbol(ID), exp1, exp2, exp3, exp1left))
   | BREAK                                                  (A.BreakExp(BREAKleft))
   | funcall                                                (funcall)

dec: vardec                                                 (vardec)
   | fundec  %prec LOWESTPREC                               (A.FunctionDec(fundec))
   | tydec   %prec LOWESTPREC                               (A.TypeDec(tydec))

decs:                                                       ([])
    | dec decs                                              (dec :: decs)

vardec: VAR ID ASSIGN exp                                   (makeVarDecl(S.symbol(ID), NONE, exp, VARleft))
      | VAR ID COLON ID ASSIGN exp                          (makeVarDecl(S.symbol(ID1), SOME (S.symbol(ID2), ID2left), exp, VARleft))

tydec: tydec'                                               ([tydec'])
     | tydec tydec'                                         (tydec @ [tydec'])

ty: ID                                                      (A.NameTy(S.symbol(ID), IDleft))
  | LBRACE fields RBRACE                                  (A.RecordTy(fields))
  | ARRAY OF ID                                             (A.ArrayTy(S.symbol(ID), IDleft))


tydec': TYPE ID EQ ty                                       (makeTypeDec(S.symbol(ID), ty, IDleft))

seqexp:                                                     ([])
      | exp seqexp'                                         ((exp, expleft) :: seqexp')

seqexp':                                                    ([])
       | SEMICOLON exp seqexp'                              ((exp, expleft) :: seqexp')



fields:                                                   ([])
        | ID COLON ID fields'                             (makeField(S.symbol(ID1), (S.symbol(ID2), ID2left), ID1left) :: fields')

fields':                                                  ([])
         | COMMA ID COLON ID fields'                      (makeField(S.symbol(ID1), (S.symbol(ID2), ID2left), ID1left) :: fields')


lvalue: ID lvalue'                                          (makeLvaluePartSpec(A.SimpleVar(S.symbol(ID), IDleft), IDleft, lvalue'))

lvalue':                                                    ([])
       | DOT ID lvalue'                                     (Field(S.symbol(ID)) :: lvalue')
       | LBRACK exp RBRACK lvalue'                          (Subscript(exp) :: lvalue')

params: exp                                                 ((exp, expleft) :: [])
      | exp COMMA params                                    ((exp, expleft) :: params)


record: ID LBRACE record' RBRACE                            (makeRecord(record', S.symbol(ID), IDleft))

record':                                                    ([])
       | ID EQ exp record''                                 ((S.symbol(ID), exp, IDleft) :: record'')
record'':                                                   ([])
        | COMMA ID EQ exp record''                          ((S.symbol(ID), exp, IDleft) :: record'')

operator: exp PLUS exp                                      (makeBinop(exp1, A.PlusOp, exp2, exp1left))
        | exp MINUS exp                                     (makeBinop(exp1, A.MinusOp, exp2, exp1left))
        | exp TIMES exp                                     (makeBinop(exp1, A.TimesOp, exp2, exp1left))
        | exp DIVIDE exp                                    (makeBinop(exp1, A.DivideOp, exp2, exp1left))
        | exp CARET exp                                     (makeBinop(exp1, A.ExponentOp, exp2, exp1left))
        | exp EQ exp                                        (makeBinop(exp1, A.EqOp, exp2, exp1left))
        | exp NEQ exp                                       (makeBinop(exp1, A.NeqOp, exp2, exp1left))
        | exp LT exp                                        (makeBinop(exp1, A.LtOp, exp2, exp1left))
        | exp LE exp                                        (makeBinop(exp1, A.LeOp, exp2, exp1left))
        | exp GT exp                                        (makeBinop(exp1, A.GtOp, exp2, exp1left))
        | exp GE exp                                        (makeBinop(exp1, A.GeOp, exp2, exp1left))

fundec: FUNCTION ID LPAREN fields RPAREN EQ exp             (makeFundecl(S.symbol(ID), fields, NONE, exp, FUNCTIONleft) :: [])
        | FUNCTION ID LPAREN fields RPAREN COLON ID EQ exp  (makeFundecl(S.symbol(ID1), fields, SOME (S.symbol(ID2), ID2left), exp, FUNCTIONleft) :: [])


funcall: ID LPAREN RPAREN                                   (makeCall(S.symbol(ID), [], IDleft))
       | ID LPAREN params RPAREN                            (makeCall(S.symbol(ID), params, IDleft))