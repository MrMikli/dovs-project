(* Aarhus University, Compilation 2017  *)
(* DO NOT DISTRIBUTE                    *)
(* SKELETON FILE –                      *)


(* LLCodegen -- Tiger to LLVM-- code generator *)


signature LLCODEGEN =
sig
  val codegen_prog : { exp : OAbsyn.exp , locals: (int * Types.ty) list } -> ll.prog
  val codegen_ll  :  { exp : OAbsyn.exp , locals: (int * Types.ty) list } -> string
end


structure LLCodegen :> LLCODEGEN =
struct

structure A'' = OAbsyn
structure Ty = Types
structure S = Symbol
structure B = CfgBuilder
structure E = CgEnv


exception CodegenFatal (* should not happen *)

exception NotImplemented
fun TODO() = (print "Hit a TODO\n"; raise NotImplemented)


(* variable environment is a mapping of symbols to variable entries in the
coge generation environment  *)

type venv = E.enventry S.table
type tenv = (Ty.ty * ll.ty) S.table


(* representation of the locals context

Given a Tiger code with nested declarations

let function fun1 (...) =
    let function fun2 (...) =
    in ...
    end
in
   ...
end

we represent it at runtime with the locals list of type (ll.tid * ll.ty) list
that might looks like this:

[ (%typeLocalsMain, { LL struct for Main's locals } )
, (%typeLocalsFun1, { LL struct for Fun1's locals } )
, (%typeLocalsFun2, { LL struct for Fun2's locals } ]
]

Here %typeLocalMain, %typeLocalsFun1, etc are LL types that need to be emitted;
the LL structs contain the information about the LL records about the local
variables in each function. The length of the list corresponds to the current
of nested declarations.*)

type cg_ctxt = { venv : venv
               , tenv : tenv
               , locals  : (ll.tid * ll.ty) list  (* the locals *)
               , lid : ll.uid (* the llvm structure keeping the
                                           locals of the current function *)
                                     (* ... there is probably more *)
               }


(* tranlate Tiger type to LL type *)
fun ty_to_llty ( ty: Ty.ty) : ll.ty =
  case ty
   of Ty.NIL                    => ll.Ptr ll.I8
    | Ty.INT                    => ll.I64
    | Ty.STRING                 => ll.Ptr (ll.I8)
    | Ty.ARRAY _                => ll.Ptr (ll.I8)
    | Ty.RECORD _               => ll.Ptr (ll.I8)
    | Ty.NAME (_, ref (SOME t)) => ty_to_llty t
    | Ty.UNIT                   => ll.Void
    | _                         => raise CodegenFatal

(* -------------------------------------------------- *)
(* The result of code generation for expressions is an LL operand
   paired with a function from CFG builders to CFG builders *)


(* -- A brief introduction to Buildlets ------------- 


One of the challenges of generating LLVM code from a tree-like
representation such as Tiger ASTs is that while AST expressions are
tree-like, the LL code is linear. That means that the order in which
we translate different Tiger subexpressions starts to matter.

The "buildlet" approach separates the challenge of code generation for
a given a Tiger expression into two mostly orthogonal concerns:

Concern 1: What is the code for each of the subexpressions?

Concern 2: What is the order in which the code for the subexpressions
must be put together in order to produce the correct code for the
parent expression?

To implement this separation of concerns, we design the translation
function to be a function from a Tiger AST to *a CFG transformer*. CFG
transformers themselves are functions that take a CFG builder
(representing the state of the CFG at the time of putting the LL code
together) as an argument and return a CFG builder (the modified CFG).
The idea is that each subexpression transformer "knows" how to
generate the code for that subexpression, and can be invoked in a
black-box manner as long as we give it the state of the current
translation as an argument.

This approach allows us to translate the subexpressions (concern 1) in
a way that allows us to delay the decision of how to put them together
(concern 2). The benefit is that we can be compositional when working
with the transformers, as the code tends to be a lot less fragile.

----------------------------------------------------- *)


(* we call our CFG transformers buildlets *)
                          
type buildlet = B.cfg_builder -> B.cfg_builder

(* the operand expresses where to find the result assuming that
   the buildlet is invoked *)
type cg_res = (ll.operand * buildlet)


(* some auxiliary functions *)

fun emit_insn insn =
  fn b => B.add_insn b insn

fun emit_term tr =
  fn b => B.term_block b tr

fun emit_lbl lbl =
  fn b => B.set_label b lbl

fun emit_alloca t =
  fn b => B.add_alloca b t

val id_buildlet = fn b => b

fun seq_buildlets (bls: buildlet list) :buildlet =
  fn cfgb_init =>
     List.foldl
         (fn (b, cfgb') => b cfgb') (* apply the next buildlet to the threading CFGBuilder *)
         cfgb_init
         bls


fun build_cfg buildlet = B.get_cfg (buildlet (B.new_env()))

val build_cfg_from_buildlets = (build_cfg o seq_buildlets)

fun mapseq f ls = seq_buildlets (map f ls)


fun emit_uid' (a, b) = emit_insn (SOME a, b)
fun build_store args = emit_insn (NONE, ll.Store args)

(* -------------------------------------------------- *)


val mk_uid = LLEnv.mk_uid
val add_named_type = LLEnv.add_named_type
val mk_label_name  = LLEnv.mk_label_name
val mk_type_name  = LLEnv.mk_type_name

fun emit_mk' ll_env (s, i) =
  let val uid = mk_uid ll_env s
  in (uid, emit_uid' (uid,i))
  end

fun emit_mk'' ll_env (s, i) =
  let val uid = mk_uid ll_env s
  in (ll.Id uid, emit_uid' (uid,i))
  end

fun build_load_op ll_env s args =
  let val uid = mk_uid ll_env s
  in (ll.Id uid, emit_uid' (uid, ll.Load args))
  end

(* -------------------------------------------------- *)

(* our main workhorse function for code generation *)
fun cgExp ll_env cg_ctxt (aug_exp:A''.exp) : cg_res =
  let val exp = #exp aug_exp
      val ty = (#ty o #aug) aug_exp
      val emit_mk'  = emit_mk' ll_env
      val emit_mk'' = emit_mk'' ll_env
      val build_load_op = build_load_op ll_env


      (* convenient function for recursive calls when the
         environment and the context do not change *)
      val (cgE : (A''.exp -> cg_res)) = cgExp ll_env cg_ctxt

  in case exp
      of A''.NilExp   => TODO () (* returned operand should be ll.Null      *)

       | A''.IntExp i => ( ll.Const i, id_buildlet)


       | A''.OpExp {left, oper, right} => TODO ()
       | A''.SeqExp [e]                => cgE e
       | A''.SeqExp exps               => raise NotImplemented
       | A''.LetExp {decls, body}      => TODO ()   (* needs to call cgDecl *)
       | A''.VarExp var                => cgVar ll_env cg_ctxt var
                                            (* delegate to a dedicated function *)
       | A''.AssignExp {var, exp}      => TODO () (* call cgVarLoc *)

       | A''.IfExp {test, thn, els as (SOME els') }  =>
         let val then_lbl = mk_label_name ll_env "then"
             val else_lbl = mk_label_name ll_env "else"
             val merg_lbl = mk_label_name ll_env "merge"

             val (test_oprnd, test_b) = cgE test
             val (then_oprnd, then_b) = cgE thn
             val (else_oprnd, else_b) = cgE els'

             (* branch on test_oprnd, and depending on the branch,
                jump to either then_lbl or else_lbl *)

             val (test_op', build_icmp) =
		      emit_mk'  ("test"
			   , ll.Icmp ( ll.Ne
				     , ll.I64
				     , test_oprnd
				     , ll.Const 0))

             val build_cbr =
                 emit_term (ll.Cbr ( ll.Id test_op'
				   , then_lbl
				   , else_lbl ))

             val uid = mk_uid ll_env "ifthenelse_result"
             val llty = ty_to_llty ty
             val b_res_alloca = emit_alloca (uid, llty)
             fun b_store_r r = build_store (llty, r , ll.Id uid)

	     val (res_val, b_load) =
    		 build_load_op "res" (llty, ll.Id uid)

	     val b_br_merge = emit_term (ll.Br merg_lbl)

             val b_ = seq_buildlets [
                     b_res_alloca

                   , test_b
                   , build_icmp
                   , build_cbr

                   , emit_lbl then_lbl
                   , then_b
                   , b_store_r then_oprnd
                   , b_br_merge

                   , emit_lbl else_lbl
                   , else_b
                   , b_store_r else_oprnd
                   , b_br_merge

                   , emit_lbl merg_lbl
                   , b_load ]
         in
             (res_val, b_) (* *)
         end

       | A''.CallExp {func, args}                    => TODO ()
       | A''.RecordExp {fields}                      => TODO ()
       | A''.IfExp {test, thn, els as NONE }         => TODO ()
       | A''.WhileExp { test, body}                  => TODO ()
       | A''.BreakExp                                => TODO ()
       | A''.ForExp {var, escape, lo, hi, body, aug} => TODO ()
       | A''.ArrayExp {size, init}                   => TODO ()
       | A''.StringExp s                             => TODO ()
       | A''.ErrorExp => raise CodegenFatal
  end

(* code generation of declarations *)
and cgDecl ll_env cg_ctxt aug_decl =
    case aug_decl
     of A''.VarDec { name, escape, aug as {ty, offset}, init } => TODO ()
      | A''.FunctionDec fns                                    => TODO ()
      | A''.TypeDec tds                                        => TODO ()

(* cgVarLoc returns the location and a type for the variable to be
   used for subsequent load /saves --------------------------------------- *)

and cgVarLoc ll_env cg_ctxt (var_aug:A''.var) =
    let val var = #var var_aug
    in case var
	of A''.SimpleVar s => TODO ()
	 | A''.FieldVar (var, s) => TODO ()
	 | A''.SubscriptVar (var, exp) => TODO ()
    end

(* code generation of variables ---------------------------------------------- *)
and cgVar ll_env  cg_ctxt var_aug =
    let val loc = (* call cgVarLoc to get the location of the variable *) TODO ()
	val var = #var var_aug
	val ty  =  (#ty o #aug) var_aug
    in TODO ()
    end

(* coge generation for main -------------------------------------------------- *)
fun cgMain (ll_env: LLEnv.ll_env)
	   { exp:A''.exp, locals: (int * Ty.ty) list } =
  let val ty_: ll.ty list = (* we put I1 as a placeholder *)
	  ll.I1 :: (map ty_to_llty ((#2 o ListPair.unzip) locals))

      val locs_struct = ll.Struct ty_
      val locs_name   = LLEnv.mk_type_name ll_env "tigermain"
      val lid         = LLEnv.mk_uid ll_env "locals"

      val cg_ctxt = {
          venv = E.baseVenv
        , tenv = E.baseTenv
        , locals = [ (locs_name, locs_struct) ]
        , lid = lid
      }

      (* emit the type declaration *)
      val _ = LLEnv.add_named_type ll_env (locs_name, locs_struct)

      (* builder for allocating the locals in the current block *)
      val build_alloca = emit_alloca (lid, ll.Namedt locs_name)

      (* codegen the rest of the expression *)
      val (oprnd, build_exp) = cgExp ll_env cg_ctxt exp

      (* terminate the last (potentially the same as the first) block *)
      val (rt, tr) = case (( #ty o #aug) exp ) of
                         Ty.UNIT => (ll.Void, ll.Ret (ll.Void, NONE))
                       | t       => (ty_to_llty t,
                                     ll.Ret (ty_to_llty t, SOME oprnd))


      (* make the terminator *)
      val build_ret = emit_term tr

      (* put things together to get the final CFG for the main *)

      val cfg = build_cfg_from_buildlets [  build_alloca
                                          , build_exp
                                          , build_ret ]

      (* generate the function declaration *)
      val (fd:ll.fdecl) = { fty = ( [], rt), param = [], cfg = cfg}

  in
      (* emit the function to the generated LL environment *)
      LLEnv.add_fun_decl ll_env (S.symbol "tigermain", fd)
  end


(* ------------------------------------------------------------------ *)
(* The rest of the code below is does not require any changes         *)
(* though you are free to change it if needed e.g., for debugging     *)

fun newline s = s^"\n"

val runtime_fns =
    let val fns = [ "i8* @allocRecord(i64)"   (* runtime functions *)
		  , "i8* @initArray (i64, i64, i8*)"
		  , "i64 @stringEqual (i8*, i8*)"
		  , "i64 @stringNotEq (i8*, i8*)"
		  , "i64 @stringLess (i8*, i8*)"
		  , "i64 @stringLessEq (i8*, i8*)"
		  , "i64 @stringGreater (i8*, i8*)"
		  , "i64 @stringGreaterEq (i8*, i8*)"


		  , "void @print    (i8*, i8*)"   (* user visible functions; note SL argument *)
		  , "void @flush    (i8*)"
		  , "i8*  @getChar  (i8*)"
		  , "i64  @ord      (i8*, i8*)"
		  , "i8*  @chr      (i8*, i64)"
		  , "i64  @size     (i8*, i8*)"
		  , "i8*  @substring(i8*, i8*, i64, i64)"
		  , "i8*  @concat   (i8*, i8*, i8*)"
		  , "i64  @not      (i8*, i64)"
		  , "void @exit     (i8*, i64)"
		  ]
	fun mkDeclare s = "declare " ^ s ^ "\n"
    in String.concat (map mkDeclare fns)
    end

val target_triple =
  case LocalOS.os of
     LocalOS.Darwin => "target triple = \"x86_64-apple-macosx10.13.0\""
   | LocalOS.Linux  => "target triple = \"x86_64-pc-linux-gnu\""


fun codegen_prog (aug_offset as { exp:A''.exp, locals: (int * Ty.ty) list }) =
  let val ll_env  = LLEnv.init_ll_env ()
      val _    = cgMain ll_env aug_offset
      val prog = LLEnv.prog_of_env ll_env
  in prog
  end

(* a wrapper around codegen_prog *)
fun codegen_ll aug_offset =
  let val prog = codegen_prog aug_offset
      val s = ll.string_of_prog prog
      val s' = s ^ "\n" ^ runtime_fns ^ (newline target_triple)
  in s'
  end

end (* struct LLCodegen *)
