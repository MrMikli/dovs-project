// AU Compilation course
// Please do not distribute
//

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int tigermain(void *, int);

int arrLenError(int len)
{
    printf("Error: attempt to create array with negative length (%d)\n",len);
    exit(1);
    return 0; /* keeping gcc happy */
}

/* p164 */
void *initArray(long size, long elem_size, void* init)
{
    /* Array layout: [S, elm0, elm1, ..., elmS-1].  Returning pointer
     * to elm0, which means that the size S may be ignored---but it
     * is available in case array bounds checking code is generated */

  long i;
  void *a;
  void *p;

  if (size<0)
    arrLenError(size);

  a = calloc( size,  elem_size);

  p = a;
  for (i = 0; i <= size - 1; i++) {
    memcpy (p, init, elem_size );
    p += elem_size;
  }
  
  return a;
}

int arrInxError(int index)
{
    printf("Error: array index (%d) out of range\n",index);
    exit(1);
    return 0; /* keeping gcc happy */
}

void *allocRecord(int size)
{
  return calloc (size, 1) ;
}

int recFieldError()
{
    printf("Error: record field lookup applied to nil\n");
    exit(1);
    return 0; /* keeping gcc happy */
}

struct string {
    long length;
    unsigned char chars[1];
};

long stringEqual(struct string *s, struct string *t)
{
    long i;
    if (s == t)
        return 1;
    if (s->length != t->length)
        return 0;
    for (i = 0; i < s->length; i++)
        if (s->chars[i] != t->chars[i])
            return 0;
    return 1;

}

long stringNotEq(struct string *s, struct string *t)
{
    return !stringEqual(s, t);
}

long stringLessEq(struct string *s, struct string *t)
{
    long i,len;
    if (s == t)
        return 1;
    len = s->length <= t->length? s->length : t->length;
    for (i = 0; i < len; i++) {
        if (s->chars[i] < t->chars[i]) return 1;
        if (s->chars[i] > t->chars[i]) return 0;
        /* s->chars[j] == t->chars[j] for all j, 0<=j<=i */
    }
    return (s->length <= t->length);
}

long stringLess(struct string *s, struct string *t)
{
    return !stringLessEq(t, s);
}

long stringGreater(struct string *s, struct string *t)
{
    return !stringLessEq(s, t);
}

long stringGreaterEq(struct string *s, struct string *t)
{
    return stringLessEq(t, s);
}

void print(void *static_link, struct string *s)
{
    long i;
    unsigned char *p = s->chars;
    for (i = 0; i < s->length; i++, p++) {
      putchar(*p);
    }
}

void flush(void *static_link)
{
    fflush(stdout);
}

struct string consts[256];
struct string empty = { 0, "" };

int main(int argc, char *argv[])
{
    int i;
    int result;

    for (i = 0; i < 256; i++) {
        consts[i].length = 1;
        consts[i].chars[0] = i;
    }
    /* args to tigermain: 0 is the static link, 1000 is unused, but
     * easy to see on the stack, nice when debugging frames */
    result = tigermain(0, 1000);
    return result;
}

long ord(void *static_link, struct string *s)
{
    if (s->length == 0)
        return -1;
    else
        return s->chars[0];
}

struct string *chr(void *static_link, long i)
{
    if (i < 0 || i >= 256) {
        printf("Error: chr(%ld) out of range\n",i);
        exit(1);
    }
    return consts+i;
}

long size(void *static_link, struct string *s)
{
    return s->length;
}

struct string *substring(void *static_link, struct string *s, long first, long n)
{
    if (first < 0 || first+n>s->length) {
        printf("Error: substring([%ld],%ld,%ld) out of range\n",s->length,first,n);
        exit(1);
    }
    if (n == 1)
        return consts+s->chars[first];
    {
        struct string *t = (struct string *)malloc(sizeof(int)+n);
        long i;
        t->length = n;
        for (i = 0; i < n; i++)
            t->chars[i] = s->chars[first+i];
        return t;
    }
}

struct string *concat(void *static_link, struct string *a, struct string *b)
{
    if (a->length == 0)
        return b;
    else if (b->length == 0)
        return a;
    else {
        long i, n = a->length+b->length;
        struct string *t = (struct string *)malloc(sizeof(int)+n);
        t->length = n;
        for (i = 0; i < a->length; i++)
            t->chars[i] = a->chars[i];
        for(i = 0; i < b->length; i++)
            t->chars[i+a->length] = b->chars[i];
        return t;
     }
}

long not(void *static_link, int i)
{
    return !i;
}

struct string *getChar(void *static_link)
{
    int i = getc(stdin);
    if (i == EOF)
        return &empty;
    else
        return consts+i;
}

long exponent(long base, long expn) {
    long x = base;
    long n = expn;
    
    if (n == 0){
        return 1;
    }
    if (n == 1){
        return x;
    }
    if (n < 0){
        x = 1/x;
        n = -n;
    }
    long y = 1;
    while (n > 1){
        if (n % 2 == 0){
            x *= x;
            n /= 2;
        }
        else{
            y *= x;
            x *= x;
            n = (n - 1)/ 2;
        }
    }
    return y*x;
}