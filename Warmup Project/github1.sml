
(* straight line program interpreter *)

structure SLgrammar = struct

type id = string

datatype binop = Plus | Minus | Times | Div

datatype stm = CompoundStm of stm * stm
             | AssignStm of id * exp
             | PrintStm of exp list
     and exp = IdExp of id
             | NumExp of int
             | OpExp of exp * binop * exp
             | EseqExp of stm * exp      
end 

(* Write an ML function (maxargs : stm -> int) that tells the maximum *)
(* number of arguments of any print statement within any subexpression *)
(* of a given statement. *)

structure G = SLgrammar

fun max (x, y) = if x > y then x else y

fun maxargs (G.PrintStm args) = length args
    | maxargs (G.AssignStm (_, e)) = maxargs_exp e
    | maxargs (G.CompoundStm (s1, s2)) = max (maxargs s1, maxargs s2)
and maxargs_exp (G.IdExp _) = 0
    | maxargs_exp (G.NumExp _) = 0
    | maxargs_exp (G.OpExp (e1, _, e2)) = max (maxargs_exp e1, maxargs_exp e2)
    | maxargs_exp (G.EseqExp (s, e)) = max (maxargs s, maxargs_exp e)

val prog =
    G.CompoundStm (
    G.AssignStm ("a", G.OpExp (G.NumExp 5, G.Plus, G.NumExp 3)),
    G.CompoundStm (
      G.AssignStm ("b", G.EseqExp (
        G.PrintStm [G.IdExp "a", G.OpExp (G.IdExp "a", G.Minus, G.NumExp 1)],
        G.OpExp (G.NumExp 10, G.Times, G.IdExp "a"))),
      G.PrintStm [G.IdExp "b"]))

val 2 = maxargs prog

type table = (id * int) list

(* Write an ML function interp : stm -> unit that "interprets" a *)
(* program in this language... in a "functional" style -- without *)
(* assignment (:=) or arrays... *)

(* book specifies type of lookup to be table * id -> int, and not *)
(* table * int -> int option, so throw an exception if there's no *)
(* binding *)
exception NoSuchBinding
fun lookup (nil, x) = raise NoSuchBinding
  | lookup ((k, v)::rest, x) = if x = k then v else lookup (rest, x)

fun update (t, k, v) = (k, v)::t

fun apply (n:int, m:int, b:G.binop):int =
    case b
     of G.Plus => n + m
      | G.Times => n * m
      | G.Minus => n - m
      | G.Div => n div m

(* interpStm : stm * table -> table *)
fun interpStm (G.CompoundStm (s1, s2), t) = interpStm (s2, interpStm (s1, t))
  | interpStm (G.AssignStm (i, e), t) =
    let val (n:int, t':table) = interpExp (e, t) in
        update (t', i, n)
    end
  | interpStm (G.PrintStm args, t) =
    case args
     of nil => (print "\n"; t)
      | head::tail =>
        let val (n:int, t':table) = interpExp (head, t) in
            (print (Int.toString n); print " "; interpStm (G.PrintStm tail, t'))
        end
(* interpExp : exp * table -> int * table *)
and interpExp (G.IdExp i, t)  = (lookup (t, i), t)
  | interpExp (G.NumExp n, t) = (n, t)
  | interpExp (G.OpExp (e1, b, e2), t) =
    let val (n:int, t':table) = interpExp (e1, t)
        val (m:int, t'':table) = interpExp (e2, t')
    in
        (apply (n, m, b), t'')
    end
  | interpExp (G.EseqExp (s, e), t) =
    let val t':table = interpStm (s, t) in
        interpExp (e, t')
    end

fun interp (s:G.stm):unit = (interpStm (s, nil); ())

local val t = [("a",1)]
in
    val (1,t) = interpExp(G.IdExp("a"),t)
    val (2,t) = interpExp(G.OpExp(G.IdExp("a"),G.Plus,G.IdExp("a")),t)
end

local val t = [("a",1)]
      val t' = interpStm(G.AssignStm("a",G.NumExp(2)),t)
in
    val 2 = lookup(t',"a")
end
