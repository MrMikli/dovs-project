(* ---------- Initial code for hand-in 1, dOvs 2018: warmup.sml ---------- *)

(* Before you start working:

   1. Read the documentation on the course webpage AND the description
      of the assignment in the textbook.

   2. Read this file until the end.

   If there are things that are unclear about this file, consider
   posting your questions on the webboard; it is likely that others
   may have the same concerns.                                             *)


structure SLgrammar = struct

type id = string
type table = (id * int) list

datatype binop = Plus | Minus | Times | Div

datatype stm = CompoundStm of stm * stm
             | AssignStm of id * exp
             | PrintStm of exp list
     and exp = IdExp of id
             | NumExp of int
             | OpExp of exp * binop * exp
             | EseqExp of stm * exp
                     
end (* SLgrammar *) 

(* ----- Fill in: Interpret SLgrammar as specified ----------------------- *)

structure G = SLgrammar

exception NotImplemented (* when the entire assignment is complete, the
                            code should compile with this exception
                            declaration removed                            *)

(* ----------------------------------------------------------------------- *)
(* -- placeholder declarations for not implemented functions ------------- *)

              
(* -- Function stringOfStm: ---------------------------------------------- *)
(* Input: a value of type G.stm that corresponds to a statement in our
   language.
   
   Output: a string representation of that statement.
   
   Note: the assginment does not specify _how_ the string is
   formatted. There may be in general several ways of doing this. The
   example below is just one possibility.

   Example: For example program 'prog' (defined below) a possible output
   is string: "a := 5+3; b := (print(a,a-1), 10*a); print(b)"              *)

fun stringOfStm (s: G.stm) : string = (* Return string representation of a statement using recursion *)
    case s of 
         G.CompoundStm(s1, s2) => stringOfStm(s1) ^ "; " ^ stringOfStm(s2)
       | G.AssignStm(id, e) => id ^ " := " ^ stringOfExp(e)
       | G.PrintStm es => "print(" ^ stringOfExps(es) ^ ")"
and stringOfExp (e: G.exp) : string = (* Return string representation of an expression using recursion *)
    case e of
         G.IdExp id => id
       | G.NumExp i => Int.toString(i) 
       | G.OpExp (e1, binop, e2) => stringOfExp(e1) ^ stringOfBinop(binop) ^ stringOfExp(e2)
       | G.EseqExp (s, e3) => "(" ^ stringOfStm(s) ^ ", " ^ stringOfExp(e3) ^ ")"
and stringOfExps (es : G.exp list) : string = (* Recursively go through list and print the expressions with a comma *)
    case es of                                (* unless it's the last expression in the list, then just do it without a comma *)
          nil => ""
       | (e::rest) => if length(rest) = 0 then stringOfExp(e) ^ stringOfExps(rest)
                      else stringOfExp(e) ^ ", " ^ stringOfExps(rest)
and stringOfBinop (bo: G.binop) : string = (* Returns string representation of a binop *)
    case bo of
         G.Plus => " + "
       | G.Minus => " - "
       | G.Times => " * "
       | G.Div => " / " 
and length
    [] = 0 (* Extra function just to calcutate length of a list *)
  | length (x::xs) = 1 + length xs

fun maxargs (s: G.stm) : int = 
    case s of
        G.CompoundStm (s1, s2) => Int.max(maxargs(s1), maxargs(s2))
      | G.AssignStm (_, e) => maxargsexp(e)
      | G.PrintStm es => Int.max(List.length es, maxargsexps(es))
and maxargsexp (e: G.exp) : int =
    case e of
        G.OpExp (e1, _, e2  ) => Int.max(maxargsexp e1, maxargsexp e2)
      | G.EseqExp (s, e3) => Int.max(maxargs s, maxargsexp e3)
      | _ => 0
and maxargsexps (es: G.exp list) : int =
    case es of
        nil => 0
      | (e :: rest) => Int.max(maxargsexp e, maxargsexps rest)
(*
(G.CompoundStm stm1, stm2) = stringOfStm(stm1) ^ ";" ^ stringOfStm(stm2)
  |   (G.AssignStm id, exp) = id ^ " := " ^ stringOfStm(exp)
  |   (G.PrintStm args) = "print(" ^ map (fn x => x = stringOfStm(x)) args ^ ")"
  |   (G.IdExp id) = id
  |   (G.OpExp exp1 binop exp2) = stringOfStm(exp1) ^ stringOfBinop(binop) ^ stringOfStm(exp2)
  |   (G.EseqExp stm, exp) = "(" ^ stringOfStm(stm) ^ "," ^ stringOfStm(exp) ^ ")"

*)
(* -- Function interpStm ------------------------------------------------- *)          
(* Funtion interpStm interprets a G.stm - Implementation hints can be
   found in the book                                                       *)
          
(* lookup: We start off with a table and the key we are looking for, 
    then we look if the first pair contains the key we are looking for, if not, we recursively call
    lookup with the rest of the list as the argument (with the key we are looking for of course)*)
fun lookup ((k, v)::rest, key) = if key = k then v else lookup (rest, key)

(* update: We put the key and value into a pair, and append them to the front of the table*)
fun update (table, key, value) = (key, value)::table 
(* update (c, 10, [(c, 4), (a, 5)]) = [(c, 10)::(c, 4), (a, 5)] = [(c, 10), (a ,5), (c, 4)] *)  

(* -- Function interpStm ------------------------------------------------- *)          
(* Funtion interpStm interprets a G.stm - Implementation hints can be
   found in the book                                                       *)
          
fun interpStm (s:G.stm, tab:G.table) : G.table =
  case s of
    G.CompoundStm(stm1, stm2)  => interpStm(stm2, interpStm(stm1, tab))
    (* We interpret the whole first statement, until we get to a leaf, which will bring us back up the tree *)
    | G.AssignStm(id, exp) => (*We have an id and an expression*)
      let 
        val (n:int, t:G.table) = interpExp(exp, tab)
        (* We let n (int) be the interpretation of the expression, and t be the table*)
      in 
        update(t, id, n) (* We update the table with the value we interpreted on the id that was assigned in the AssignStm *)
      end
    | G.PrintStm nil  => tab
    | G.PrintStm(exp :: restOfExpList) => let val (v, t) = interpExp(exp, tab)
                                (*We let v be the value that was interpretted and t be the table after the interpretation*)
                                              val valuesOfList = List.map (fn x => first (interpExp(x, t)))
                                                                  (exp::restOfExpList);
                                          (* We use the simple function "first" to only get the integer of the exp tuple,*)
                                          in            (* which is then returned to us as a list*)
                                              List.map (fn x => print( Int.toString x ^ " ")) valuesOfList;
                                              (* We then print all of the values in the list *)
                                              (* and returns the unchanged t afterwards *)
                                              (print("\n"); t)
                                          end 
                                              
and interpExp(e: G.exp, t:G.table) : (int * G.table) = 
  case e of 
    G.IdExp i                    => (lookup(t, i), t)
   | G.NumExp i                  => (i, t)
   | G.OpExp (exp1, binop, exp2) => let val (a, t1) = interpExp(exp1, t)
                                        val (b, t2) = interpExp(exp2, t)
                                    in case binop of 
                                      G.Plus => (a + b, t)
                                    | G.Minus => (a - b, t)
                                    | G.Times => (a * b, t)
                                    | G.Div => (a div b, t)
                                    end
   | G.EseqExp (s, exp)          => interpExp(exp, interpStm(s, t))
and first (a,b) = a

fun interp (s: G.stm): G.table =
    (print( "Executing: " ^ stringOfStm s ^ "\nMaxArgs for prog: " ^
            Int.toString( maxargs s ) ^ "\nResult of program is: \n"); interpStm(s, []))
    (* Current return is just printing this. Should run interpStm and print invidiually in that call. *)
    (* Worked before pulling the interp functions *)

(* -- Example for testing ------------------------------------------------ *)

val prog : G.stm =
  (* a := 5+3; b := (print(a,a-1), 10*a); print(b) *)
  G.CompoundStm (
    G.AssignStm ("a", G.OpExp ( G.NumExp 5, G.Plus, G.NumExp 3)),
    G.CompoundStm (
      G.AssignStm ("b", G.EseqExp (
        G.PrintStm [ G.IdExp "a", G.OpExp ( G.IdExp "a", G.Minus, G.NumExp 1)],
        G.OpExp ( G.NumExp 10, G.Times, G.IdExp "a"))),
      G.PrintStm [ G.IdExp "b"]))

val prog2 : G.stm = 
    (*  c:= 5 x 8; d:= c + 2; print(c+d) *)
    (*  Expected result is: 82   *)
    G.CompoundStm(
      G.AssignStm("c", G.OpExp(G.NumExp 5, G.Times, G.NumExp 8)),
      G.CompoundStm(
        G.AssignStm("d", G.OpExp(G.IdExp "c", G.Plus, G.NumExp 2)), 
        G.PrintStm[G.OpExp(G.IdExp "c", G.Plus, G.IdExp "d")]
      )
    )

val prog3 : G.stm =
    (* a:= 10; b:= a / 2; print(a x b, a); a:= 100; print(a)  *)
    (* Expected result is: 50 10
                           100      *)
    G.CompoundStm(
      G.AssignStm("a", G.NumExp 10),
      G.CompoundStm(
        G.AssignStm("b", G.OpExp(G.IdExp "a", G.Div, G.NumExp 2)),
        G.CompoundStm( 
          G.PrintStm[G.OpExp(G.IdExp "a", G.Times, G.IdExp "b"), G.IdExp "a"],
          G.CompoundStm(
            G.AssignStm("a", G.NumExp 100),
            G.PrintStm[G.IdExp "a"]
          )
        )
      )
    )

val prog4 : G.stm =
(* a := 5; print(a * 2); b := 6; c:= 7; print(a + b + c) *)
(* Expected result is: 10
                       18      *)
    G.CompoundStm(
      G.AssignStm("a", G.NumExp 5),
      G.CompoundStm(
        G.PrintStm[G.OpExp(G.IdExp "a", G.Times, G.NumExp 2)],
        G.CompoundStm(
          G.AssignStm("b", G.NumExp 6),
          G.CompoundStm(
            G.AssignStm("c", G.NumExp 7),
            G.PrintStm[G.OpExp(G.OpExp(G.IdExp "a", G.Plus, G.IdExp "b"), G.Plus, G.IdExp "c")]
          )
        )
      )
    )

val prog5 : G.stm =
(* a:= 0; b:= 10; c := a / b; a:= b; c:= c + b; print(a, b, c) *)
(* Expected result is: 10 10 10 *)
    G.CompoundStm(
      G.AssignStm("a", G.NumExp 0),
      G.CompoundStm(
        G.AssignStm("b", G.NumExp 10),
        G.CompoundStm(
          G.AssignStm("c", G.OpExp(G.IdExp "a", G.Div, G.IdExp "b")),
          G.CompoundStm(
            G.AssignStm("a", G.IdExp "b"),
            G.CompoundStm(
              G.AssignStm("c", G.OpExp(G.IdExp "c", G.Plus, G.IdExp "b")),
              G.PrintStm[G.IdExp "a", G.IdExp "b", G.IdExp "c"]
            )
          )
        )
      )
    )

val prog6 : G.stm =
(* a:= 0; b= a + 1; a:= b+2; b:= a + 3; a:= b + 4; print(a + b) *)
(* Expected result is: 16 *)
    G.CompoundStm(
      G.AssignStm("a", G.NumExp 0),
      G.CompoundStm(
        G.AssignStm("b", G.OpExp(G.IdExp "a", G.Plus, G.NumExp 1)),
        G.CompoundStm(
          G.AssignStm("a", G.OpExp(G.IdExp "b", G.Plus, G.NumExp 2)),
          G.CompoundStm(
            G.AssignStm("b", G.OpExp(G.IdExp "a", G.Plus, G.NumExp 3)),
            G.CompoundStm(
              G.AssignStm("a", G.OpExp(G.IdExp "b", G.Plus, G.NumExp 4)),
              G.PrintStm[G.OpExp(G.IdExp "a", G.Plus, G.IdExp "b")]
            )
          )
        )
      )
    )
(* Calling the interpreter on the example programs. *) 

val result = interp prog
val result2 = interp prog2
val result3 = interp prog3
val result4 = interp prog4
val result5 = interp prog5
val result6 = interp prog6