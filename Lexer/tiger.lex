type pos = int
type lexresult = Tokens.token

val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos

val openString = ref 0
val openComments = ref 0
val stringRep = ref ""
val stringStart = ref 0

fun err (p1,p2) = ErrorMsg.error p1

fun eof () =
    let
        val pos = hd (!linePos)
    in
        if(0 < !openComments) then ErrorMsg.error pos ("Unclosed comments in file.")
        else if(0 < !openString) then ErrorMsg.error pos ("Unclosed string in file.")
        else ();
        Tokens.EOF (pos,pos)
    end

fun s2i t pos =
    let
        val opti = (Int.fromString t) 
            handle Overflow => 
                   (ErrorMsg.error pos "Integer too large"; SOME 0)
        fun s2i_aux NONE = (ErrorMsg.error pos "Ill-formed integer"; 0)
          | s2i_aux (SOME n) = n
    in
        s2i_aux opti
    end

fun dopos token yypos yylen = token (yypos, yypos + yylen)
fun dopos3 token value yypos yylen = token (value, yypos, yypos + yylen)

%%
letter=[a-zA-Z];
digits=[0-9]+;
idchars=[a-zA-Z0-9_]*; 
quote=[\"];
ascii=[0-9]{3};
%s COM STR ESC;
%%


<INITIAL, COM> [\ \t]+               => (continue());
<INITIAL, COM> [\n\r]+	             => (lineNum := !lineNum+1; linePos := yypos :: !linePos; continue());

<INITIAL> {digits}                   => (dopos3 Tokens.INT (s2i yytext yypos) yypos (size yytext));

<INITIAL> "var"                      => (dopos Tokens.VAR yypos 3);
<INITIAL> "type"                     => (dopos Tokens.TYPE yypos 4);
<INITIAL> "function"                 => (dopos Tokens.FUNCTION yypos 8);
<INITIAL> "break"                    => (dopos Tokens.BREAK yypos 5);
<INITIAL> "of"                       => (dopos Tokens.OF yypos 2);
<INITIAL> "end"                      => (dopos Tokens.END yypos 3);
<INITIAL> "in"                       => (dopos Tokens.IN yypos 2);
<INITIAL> "nil"                      => (dopos Tokens.NIL yypos 3);
<INITIAL> "let"                      => (dopos Tokens.LET yypos 3);
<INITIAL> "do"                       => (dopos Tokens.DO yypos 2);
<INITIAL> "to"                       => (dopos Tokens.TO yypos 2);
<INITIAL> "for"                      => (dopos Tokens.FOR yypos 3);
<INITIAL> "while"                    => (dopos Tokens.WHILE yypos 5);
<INITIAL> "else"                     => (dopos Tokens.ELSE yypos 4);
<INITIAL> "then"                     => (dopos Tokens.THEN yypos 4);
<INITIAL> "if"                       => (dopos Tokens.IF yypos 2);
<INITIAL> "array"                    => (dopos Tokens.ARRAY yypos 4);


<INITIAL> ":="                       => (dopos Tokens.ASSIGN yypos 1);
<INITIAL> "|"                        => (dopos Tokens.OR yypos 1);
<INITIAL> "&"                        => (dopos Tokens.AND yypos 1);
<INITIAL> "="                        => (dopos Tokens.EQ yypos 1);
<INITIAL> "^"                        => (dopos Tokens.CARET yypos 1);
<INITIAL> ">="                       => (dopos Tokens.GE yypos 2);
<INITIAL> ">"                        => (dopos Tokens.GT yypos 1);
<INITIAL> "<="                       => (dopos Tokens.LE yypos 2);
<INITIAL> "<"                        => (dopos Tokens.LT yypos 1);
<INITIAL> "<>"                       => (dopos Tokens.NEQ yypos 2);
<INITIAL> "="                        => (dopos Tokens.EQ yypos 1);
<INITIAL> "/"                        => (dopos Tokens.DIVIDE yypos 1);
<INITIAL> "*"                        => (dopos Tokens.TIMES yypos 1);
<INITIAL> "-"                        => (dopos Tokens.MINUS yypos 1);
<INITIAL> "+"                        => (dopos Tokens.PLUS yypos 1);
<INITIAL> "."                        => (dopos Tokens.DOT yypos 1);
<INITIAL> {quote}                    => (stringRep :=""; (openString := !openString + 1);
                                         stringStart := yypos; YYBEGIN STR; continue()); 

<INITIAL> "{"                        => (dopos Tokens.LBRACE yypos 1);
<INITIAL> "}"                        => (dopos Tokens.RBRACE yypos 1);
<INITIAL> "]"                        => (dopos Tokens.RBRACK yypos 1);
<INITIAL> "["                        => (dopos Tokens.LBRACK yypos 1);
<INITIAL> ")"                        => (dopos Tokens.RPAREN yypos 1);
<INITIAL> "("                        => (dopos Tokens.LPAREN yypos 1);
<INITIAL> ":"                        => (dopos Tokens.COLON yypos 1);
<INITIAL> ";"                        => (dopos Tokens.SEMICOLON yypos 1);
<INITIAL> ","                        => (dopos Tokens.COMMA yypos 1);

<INITIAL> " */"                      => (ErrorMsg.error yypos
                                        ("Your file seems to have closed a comment it never opened.");
                                         continue());
<INITIAL> " */*"                     => (openComments := (!openComments + 1); YYBEGIN COM;
                                         Tokens.TIMES(yypos, yypos + 1));
<INITIAL, COM> "/*"                  => (openComments := (!openComments + 1); YYBEGIN COM; continue());
<COM> "*/"                           => (openComments := (!openComments - 1); if (!openComments = 0)
                                                                              then YYBEGIN INITIAL
                                                                              else (); continue());
<COM> .                              => (continue());

<STR> {quote}                        => (YYBEGIN INITIAL; (openString := !openString - 1);
                                         Tokens.STRING(!stringRep, !stringStart, yypos));
<STR> "\\"                           => (YYBEGIN ESC; continue());
<STR> \\[\n\t \f]+\\                 => (continue());
<STR> [\n\t\f]+                      => (ErrorMsg. error yypos ("Illegal non visual character in middle of string.");
                                         continue());
<STR> .                              => (stringRep := !stringRep ^ yytext; continue());


<ESC> "\\"                           => (stringRep := !stringRep ^ "\\"; YYBEGIN STR; continue());
<ESC> "n"                            => (stringRep := !stringRep ^ "\n"; YYBEGIN STR; continue());
<ESC> "t"                            => (stringRep := !stringRep ^ "\t"; YYBEGIN STR; continue());
<ESC> {quote}                        => (stringRep := !stringRep ^ "\""; YYBEGIN STR; continue());
<ESC> {ascii}                        => (stringRep := !stringRep ^
                                         String.str(Char.chr(valOf(Int.fromString yytext)));
                                         YYBEGIN STR; continue());
<ESC> \^[a-zA-Z\@\[\\\]\^\_]{1}      => (stringRep := !stringRep ^ (valOf(String.fromString yytext));
                                         YYBEGIN STR; continue());
<ESC> \^\?                           => (stringRep := !stringRep ^ "^" ^
                                         String.str((valOf(Char.fromCString("?"))));
                                         YYBEGIN STR; continue());
<ESC> .                              => (ErrorMsg.error yypos ("illegal escape char " ^ yytext); continue());
<INITIAL> {letter} ({letter}
                  | {digits} | "_")* => (Tokens.ID(yytext,yypos,yypos+(String.size yytext)));
<INITIAL> .                          => (ErrorMsg.error yypos ("illegal char " ^ yytext); continue());

