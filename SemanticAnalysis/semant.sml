(* AU Compilation 2015.
 *
 * This the main working file for Part 4 - Semantic Analysis
 *
 *)



(* Our signature that for the semantic interface exposes only one function. *)

signature SEMANT =
sig
  val transProg: Absyn.exp -> TAbsyn.exp
end

structure Semant :> SEMANT =
struct


structure S = Symbol
structure A = Absyn
structure E = Env
structure Ty = Types
structure PT = PrintTypes
structure TAbs = TAbsyn

(* Use the extra record to add more information when traversing the tree
   It should become obvious when you a c t u a l l y need it, what to do.
   Alternatively, you have to add extra parameters to your functions *)

type extra = {} (* TODO: Figure out what the hell this is for *)

(* placeholder for declarations, the final code should compile without this *)
val TODO_DECL = TAbs.TypeDec [] (* Delete when possible *)

(* Error messages *)

val err = ErrorMsg.error

fun out msg pos = err pos msg

fun errorInt (pos, ty) =
    err pos ("INT required, " ^ PT.asString ty ^ " provided")

fun errorUnit (pos, ty) =
    err pos ("UNIT required, " ^ PT.asString ty ^ " provided")

fun errorStr (pos, ty) =
    err pos ("STRING required, " ^ PT.asString ty ^ " provided")

fun errorNil (pos, id) =
    err pos ("need to give " ^ S.name id ^ " a type when assigning the value nil")

fun errorSameNameRecordField (pos, id) =
    err pos (S.name id ^ " is used multiple times as a field in the record")
(* Write additional error messages here *)


(* -- Helper functions -- *)

(* Use the below three funs like:
   throw errorUnit (pos,ty) ifFalse isUnit(_) *)

fun ifTrue test err args = if test
                           then err args
                           else ()

fun ifFalse test err args = if test
                            then ()
                            else err args
 
fun throw err args testFun test  = testFun test err args


fun lookupTy tenv sym pos =
    let
        val tyOpt = S.look (tenv, sym)
    in
        (case tyOpt of
            SOME ty' => ty'
          | NONE => (err pos ("type " ^ S.name sym ^ " not defined"); Ty.ERROR))
    end

fun actualTy (Ty.NAME (s, ty), pos) =
    (case !ty of
        NONE => (err pos "name type without type"; Ty.ERROR)
      | SOME t => actualTy(t, pos))
  | actualTy(t, _) = t

fun checkInt ({exp, ty}, pos) =
    ((case ty
     of Ty.INT => ()
      | Ty.ERROR => ()
      | _ => errorInt (pos, ty));
      {exp = exp, ty = ty})

fun isUnit (ty) =
    case ty
     of Ty.UNIT => true
      | Ty.ERROR => true
      | _ => false

fun checkString ({exp, ty}, pos) =
    ((case ty of
        Ty.STRING => ()
      | Ty.ERROR => ()
      | _ => errorStr (pos, ty));
      exp)

fun checkAssignable (declared: Ty.ty, assigned: Ty.ty, pos, msg) =
    let
        val aDeclared = actualTy(declared, pos)
        val aAssigned = actualTy(assigned, pos)
    in
        if
        aDeclared = aAssigned
        then true
        else
        (case (aDeclared, aAssigned) of
        (Ty.RECORD(_, _), Ty.NIL) => true
      | (Ty.NIL, Ty.RECORD(_, _)) => true
      | (Ty.ERROR, _) => true
      | (_, Ty.ERROR) => true
      | _ => false)
        
    end

fun transTy (tenv, t) = case t of
    A.NameTy(sym, pos) => lookupTy tenv sym pos
  | A.ArrayTy( sym, pos) => 
        let
            val ty = lookupTy tenv sym pos
        in
            Ty.ARRAY(ty,ref())
        end
  |A.RecordTy(fieldlist)  => 
        let
            fun createRecord (usedNames, {name, escape, typ = (t, p), pos}::records) =
                let
                   val _ = throw errorSameNameRecordField (pos, name) ifTrue (List.exists (fn y => name = y)
                                                                                           usedNames)
                    val ty = lookupTy tenv t pos
                    val record_list = (name, ty)
                in
                  record_list :: createRecord (name :: usedNames, records)
                end
            | createRecord (_, []) = []
        in
            Ty.RECORD (createRecord ([], fieldlist), ref ())
        end                          


fun transExp (venv, tenv, extra : extra) =
    let
        val TODO = {exp = TAbs.ErrorExp, ty = Ty.ERROR}

        fun trexp (A.NilExp) = {exp = TAbs.NilExp, ty=Types.NIL}
          | trexp (A.VarExp var) = trvar var
          | trexp (A.IntExp i) = {exp=TAbs.IntExp i, ty=Types.INT}
          | trexp (A.StringExp (str, _)) = {exp=TAbs.StringExp str, ty=Types.STRING}
          | trexp (A.OpExp {left, oper, right, pos}) =
            if oper = A.DivideOp orelse oper = A.TimesOp orelse oper = A.PlusOp orelse oper = A.MinusOp
            then
                let
                    val l = trexp left
                    val r = trexp right
                in  
                    (checkInt(l, pos);
                     checkInt(r, pos);
                    {exp= TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Types.INT})
                end
            else if oper = A.LeOp orelse oper = A.GtOp orelse oper = A.GeOp orelse oper = A.LtOp
            then
                let
                    val l = trexp left
                    val r = trexp right
                in
                    (case #ty l of
                        Ty.STRING =>
                        (checkString(l, pos);
                         checkString(r, pos);
                         {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT})
                      | Ty.INT =>
                        (checkInt(l, pos);
                         checkInt(r, pos);
                         {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT})
                      | _ => (err pos "cannot perform this comparison due to typing mismatch";
                              {exp = TAbs.ErrorExp, ty = Ty.ERROR})) 
                end
            else if oper = A.EqOp orelse oper = A.NeqOp
            then
                let
                    val l = trexp left
                    val r = trexp right
                in
                    (case actualTy(#ty l, pos) of
                        Ty.STRING =>
                        (checkString(l, pos);
                         checkString(r, pos);
                         {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT})
                      | Ty.INT =>
                        (checkInt(l, pos);
                         checkInt(r, pos);
                         {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT})
                      | Ty.ARRAY(_, _) => 
                                (if checkAssignable(#ty l, actualTy(#ty r, pos), pos, "comparing array with invalid type")
                                then
                                    {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT} 
                                else
                                    (err pos "array types dont match";
                                    {exp = TAbs.ErrorExp, ty = Ty.ERROR})
                                )
                      | Ty.RECORD(_ , _) =>
                            (if checkAssignable(#ty l, #ty r, pos, "comparing record with invalid type")
                            then
                                {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT}
                            else 
                                (err pos "record types dont match";
                                {exp = TAbs.ErrorExp, ty = Ty.ERROR}))
                      | _ => (err pos "cannot perform this comparison due to typing mismatch";
                              {exp = TAbs.ErrorExp, ty = Ty.ERROR}))
                end
            else
                (err pos "error in operation"; {exp = TAbs.ErrorExp, ty = Ty.ERROR})
          | trexp (A.CallExp {func, args, pos}) = 
                (case S.look (venv, func) of
                    SOME (E.FunEntry {formals, result}) =>
                    (let
                        val argstype = List.map(fn (exp, pos) => (#ty (trexp exp))) args
                        fun compareTypes ([], exps, pos) = ()
                          | compareTypes (tys, [], pos) = ()
                          | compareTypes (ty1::rest1, ty2::rest2, pos) =
                            (checkAssignable(ty1, ty2, pos, "type mismatch"); compareTypes(rest1, rest2, pos))

                    in
                        (compareTypes(formals, argstype, pos);
                        {exp= TAbs.CallExp({func = func, args = List.map(fn (exp, pos) => (trexp exp)) args}),
                            ty=actualTy(result, pos)}) 
                        
                    end)
                  | _ => (err pos ("function undeclared " ^ S.name(func)); {exp = TAbs.ErrorExp, ty = Ty.ERROR}))

          | trexp (A.AssignExp {var, exp, pos}) =
                let
                    val {exp = exp', ty} = trvar var
                    val exp''  = trexp exp
                    val var' = (case exp' of
                                    TAbs.VarExp var => var
                                 | _ => (err pos "invalid variable"; {var = TAbs.SimpleVar (S.symbol "error")
                                                                     , ty = Ty.ERROR}))  
                in
                    (checkAssignable(ty, #ty exp'', pos, "invalid variable assignment")
                      ;{exp = TAbs.AssignExp {var = var', exp = exp''}, ty = Ty.UNIT})
                end
          | trexp (A.LetExp {decls, body, pos}) =
                let
                    val tcdecs as {decls = d, tenv = t, venv = v} = transDecs (venv, tenv, decls, {})
                    val tcexp as {exp = e, ty = t}= transExp (v, t, {}) body
                    val letdata = {decls = d, body = tcexp}
                in
                    {exp = TAbs.LetExp letdata, ty = t}
                end
          | trexp (A.IfExp{test, thn, els = SOME elseExp, pos}) =
                (let   
                    val test' = trexp test
                    val thn' = trexp thn
                    val elseExp' = trexp elseExp
                    val testType = #ty test' 
                    val thenType = #ty thn'
                    val elseType = #ty elseExp'
                in
                    (case testType of
                        Ty.INT => ()
                      | _ => (err pos "if-test is not an int"));
                    
                    if (checkAssignable(thenType, elseType, pos,
                        "then type is " ^ PT.asString(thenType) ^ " and else type is " ^ PT.asString(elseType)))
                    then
                        {exp = TAbs.IfExp {test = test', thn = thn', els = SOME elseExp' }, ty= thenType}
                    else
                        (err pos ("then type is " ^ PT.asString(thenType) ^ " and else type is " ^
                            PT.asString(elseType));
                        {exp = TAbs.ErrorExp, ty = Ty.ERROR})
                end)
          | trexp (A.IfExp{test, thn, els = NONE, pos}) =
                (let   
                    val test' = trexp test
                    val thn' = trexp thn
                    val testType = #ty test' 
                    val thenType = #ty thn'
                in
                    (case testType of
                        Ty.INT => ()
                      | _ => (err pos "if-test is not an int"));
                    
                    if (thenType = Ty.UNIT)
                    then
                        {exp = TAbs.IfExp {test = test', thn = thn', els = NONE }, ty= thenType}
                    else
                        (err pos "then type is not unit"; {exp = TAbs.ErrorExp, ty = Ty.ERROR})
                end)       
            | trexp (A.BreakExp(pos)) =
                {exp = TAbs.BreakExp, ty = Ty.UNIT}
            | trexp (A.WhileExp{test, body, pos}) =
                (let
                    val test' = trexp test
                    val body' = trexp body
                    val testType = #ty test'
                    val bodyType = #ty body'
                in
                    case testType of
                        Ty.INT => ()
                      | _ => (err pos "while-test is not an int");
                    
                    case bodyType of
                        Ty.UNIT => ()
                      | _ => (err pos "while-body is not a unit");

                    {exp = TAbs.WhileExp {test = test', body = body'}, ty = Ty.UNIT }
                end)
            | trexp (A.ForExp{var, escape, lo, hi, body, pos}) =
                (let
                    val venv' = S.enter(venv, var, E.VarEntry{ty=Ty.INT})
                    val hi' = trexp hi
                    val lo' = trexp lo
                    val hiType = #ty hi'
                    val loType = #ty lo'
                    val body' = transExp(venv', tenv, {}) body
                    val bodyType = #ty body'
                in
                    case hiType of
                        Ty.INT => ()
                      | _ => (err pos "high end of for-condition is not an int");

                    case loType of 
                        Ty.INT => ()
                      | _ => (err pos "low end of for-condition is not an int");
                    
                    case bodyType of
                        Ty.UNIT => ()
                      | _ => (err pos "for-body is not a unit");

                      {exp = TAbs.ForExp {var = var, escape = escape, lo = lo', hi = hi', body = body'},
                        ty = Ty.UNIT}
                end)
            | trexp (A.RecordExp{fields, typ, pos}) =
                let
                    fun trFields(fields) = 
                        (case fields of
                            (sym, exp, pos)::rest => (sym, trexp exp)::trFields rest
                          | [] => [])
                in
                    {exp = TAbs.RecordExp{fields = trFields(fields)}, ty = (actualTy((lookupTy tenv typ pos), pos))}
                end
            | trexp (A.ArrayExp{typ, size, init, pos}) =
                let
                    val size' = trexp size
                    val init' = trexp init
                    val typ' = actualTy((lookupTy tenv typ pos), pos)
                in
                    {exp = TAbs.ArrayExp {size = size', init = init'}, ty = typ'}
                end
            | trexp (A.SeqExp lst) = 
                let 
                    fun trList(lst) =
                        (case lst of
                            (exp, pos)::rest => trexp exp::trList(rest)
                          | [] => [])

                    fun findType(lst) =
                        (case lst of
                            [] => Ty.UNIT
                          | (exp, pos)::[] =>
                                let
                                    val {exp = _, ty = ty'} = trexp exp
                                in
                                    ty'
                                end
                          | (exp, pos)::rest => findType(rest))
                    val trList' = trList(lst)
                in
                    {exp = TAbs.SeqExp (rev trList'), ty = findType(lst)}
                end
        and trvar (A.SimpleVar (id, pos)) = 
                    (case S.look(venv, id) of
                        SOME (E.VarEntry{ty}) =>
                             {exp = TAbs.VarExp {var = TAbs.SimpleVar id, ty = actualTy(ty,pos)},
                                ty = actualTy(ty, pos)}
                      | NONE => (err pos "undefined variable";
                                 {exp = TAbs.ErrorExp, ty = Ty.ERROR})
                      | _ => (err pos "something went wrong while looking up a simplevar";
                              {exp = TAbs.ErrorExp, ty = Ty.ERROR} ))
          | trvar (A.FieldVar (var, id, pos)) =
                    let
                        val {exp, ty} = trvar var
                        val var' = (case exp of
                                    TAbs.VarExp var => var
                                 | _ => (err pos "invalid variable"; {var = TAbs.SimpleVar (S.symbol "error"),
                                                                       ty = Ty.ERROR}))
                        val fields' = (case (actualTy(ty,pos)) of
                                        Ty.RECORD(fields, _) => fields
                                      | _ => (err pos "not a record"; []))

                        val field' = List.find(fn (id', ty')  => S.name id = S.name id') fields'

                        val ty' = 
                            (case field' of
                                SOME(id, ty) => actualTy(ty, pos) 
                              | NONE => (err pos "field not in record"; Ty.ERROR))
                    in
                        {exp = TAbs.VarExp {var = TAbs.FieldVar (var', id), ty = ty'}, ty = ty'}
                    end
          | trvar (A.SubscriptVar (var, exp, pos)) =
                    let
                        val {exp = exp', ty} = trvar var
                        val var' = (case exp' of
                                    TAbs.VarExp var => var
                                 | _ => (err pos "invalid variable"; {var = TAbs.SimpleVar (S.symbol "error"),
                                                                       ty = Ty.ERROR}))
                        val ty' = 
                            (case ty of
                                Ty.ARRAY(ty,_) => actualTy(ty, pos)
                              | _ => (err pos "not an array"; Ty.ERROR)
                            )
                    in 
                        {exp = TAbs.VarExp {var = TAbs.SubscriptVar (var',  checkInt(trexp exp, pos)),
                                             ty = ty'},
                          ty = ty'}
                    end
        and createOpExp (left, oper, right, pos) =
            case oper
             (* These operators take only ints *)
             of A.PlusOp => ({left = left, oper = TAbs.PlusOp, right = right})
             |  A.MinusOp => ({left = left, oper = TAbs.MinusOp, right = right})
             |  A.TimesOp => ({left = left, oper = TAbs.TimesOp, right = right})
             |  A.DivideOp => ({left = left, oper = TAbs.DivideOp, right = right})
             |  A.ExponentOp =>({left = left, oper = TAbs.ExponentOp, right = right})
             (* These operators take ints or strings, but only one of them *)
             |  A.LtOp => ({left = left, oper = TAbs.LtOp, right = right})
             |  A.LeOp => ({left = left, oper = TAbs.LeOp, right = right})
             |  A.GtOp => ({left = left, oper = TAbs.GtOp, right = right})
             |  A.GeOp => ({left = left, oper = TAbs.GeOp, right = right})
             (* These operators work on ints, strings, records or arrays *)
             |  A.EqOp => ({left = left, oper = TAbs.EqOp, right = right}) 
             |  A.NeqOp => ({left = left, oper = TAbs.NeqOp, right = right})

    in
        trexp
    end

and transDec ( venv, tenv
             , A.VarDec {name, escape, typ = NONE, init, pos}, extra : extra) =
     let
        val init' = transExp (venv, tenv, extra) init
        val initType = #ty init'
        val venv' = S.enter(venv, name, E.VarEntry {ty = initType})
     in
        {decl = TAbs.VarDec { name = name, escape = escape, ty = initType, init = init'}, tenv = tenv, venv = venv'}
     end
  | transDec ( venv, tenv, A.VarDec {name, escape, typ = SOME (s, pos), init, pos=pos1}, extra) =
     let
        val typ' = lookupTy tenv s pos 
        val init' = transExp (venv, tenv, extra) init
        val initType = #ty init'
        val aType = actualTy(typ', pos)
        val venv' = S.enter(venv, name, E.VarEntry {ty = aType})
     in
        (checkAssignable(typ', aType, pos, "assigned type doesnt match type-constraint");
        {decl = TAbs.VarDec {name = name, escape = escape, ty = aType, init = init'}, tenv = tenv, venv = venv'})
     end

  | transDec (venv, tenv, A.TypeDec tydecls, extra) =
    let 
        fun buildEnv({name, ty, pos}::rest, tenv) =
                S.enter( buildEnv(rest, tenv), name, Ty.NAME(name, ref NONE))
          | buildEnv([], tenv) = tenv
        and updateEnv({name, ty, pos}::rest, tenv) =
                let
                    val ty' = transTy(tenv, ty)
                in
                    (case S.look(tenv, name) of
                        SOME(Ty.NAME(name', refoption)) => (refoption := SOME(ty')) 
                    | _ => () );
                    updateEnv(rest, tenv)
                end
          | updateEnv([], tenv) = tenv

        val tenv'' = buildEnv(tydecls, tenv)
        val tydecls' = (updateEnv(tydecls, tenv''); map (fn {name, ty, pos} => 
            {name = name, ty = transTy(tenv'', ty)}) tydecls) 
    in  
        {decl = TAbs.TypeDec(tydecls'), tenv = tenv'', venv = venv}
    end

  | transDec (venv, tenv, A.FunctionDec fundecls, extra) = 
    let 
        fun trParams(params,venv) = 
            case params of
                {name = name', escape = escape', typ = (sym, pos'), pos = pos''}::rest =>
                let
                    val ty' = (actualTy (transTy (tenv, A.NameTy(sym, pos')), pos''))
                    val venv' = S.enter(venv, name', E.VarEntry{ty = ty'})
                    val (rest', venv'') = trParams(rest, venv')
                in
                    ({name = name', escape = escape', ty = ty'}::rest', venv'')
                end
              | [] => ([], venv)
        and trDecs(fundecls, venv) = 
            case fundecls of
                {name = name', params = params', result = result', body = body', pos = pos'}::rest =>
                let 
                    val (params'', venv') = trParams(params', venv)
                    val resultType = 
                        (case result' of
                            NONE => Ty.UNIT
                          | SOME(sym, pos) => (actualTy((lookupTy tenv sym pos), pos)))
                    val exp' = (transExp(venv', tenv, extra)) body'  
                in
                    {name = name', params = params'', resultTy= resultType, body = exp'}::trDecs(rest, venv)
                end
              | [] => []
        and updateEnv([], venv) = venv
          | updateEnv(fundecls, venv) =
                let
                    val {name = name', params = params', result = result', body = body', pos = pos'}::rest =
                        fundecls
                    val (params'' , venv') = trParams(params', venv)
                    val resultType = 
                        (case result' of
                            NONE => Ty.UNIT
                          | SOME(sym, pos) => (actualTy((lookupTy tenv sym pos), pos)))
                    val paramTypes = getParamTypes(params'')
                    val funEntry = E.FunEntry{formals = paramTypes, result = resultType}
                    val venv'' = S.enter(venv', name', funEntry)
                in
                    updateEnv(rest, venv'')
                end
        
        and getParamTypes(params) =
            case params of
                {name = name', escape = escape', ty = ty'}::rest => ty'::getParamTypes(rest)
              | [] => []
        val venv'' = updateEnv(fundecls, venv)
        val trdecs = trDecs(fundecls, venv'')
    in
        {decl = TAbs.FunctionDec trdecs, tenv = tenv, venv = venv''}
    end

and transDecs (venv, tenv, decls, extra : extra) =
    let fun visit venv tenv decls result =
            case decls
             of [] => {decls = result, venv = venv, tenv = tenv}
              | (d::ds) =>
                let
                    val { decl = decl
                        , venv = venv'
                        , tenv = tenv'} = transDec (venv, tenv, d, extra)
                in
                    visit venv' tenv' ds (result @ (decl :: []))
                end
    in
        visit venv tenv decls []
    end

fun transProg absyn =
    transExp (Env.baseVenv, Env.baseTenv, {}) absyn

(*
Tests that should give errors:
 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26
 28, 29, 31, 32, 33, 34, 35, 36, 38, 39, 40, 43, 45, 49
 *)

end (* Semant *)
