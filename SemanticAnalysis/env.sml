structure Env :> ENV = 
struct

structure Sy = Symbol
structure Ty = Types

type access = unit ref
type ty = Ty.ty

datatype enventry = VarEntry of {ty: ty}
                  | FunEntry of {formals: ty list, result: ty}

fun enter ((symbol, entry), env) = Sy.enter (env, symbol, entry)

val baseTenv =
    foldl enter Sy.empty 
          [ (Sy.symbol "int", Ty.INT)
          , (Sy.symbol "string", Ty.STRING)]

val baseVenv = foldl enter Sy.empty [

  (Sy.symbol "print",               FunEntry{ formals = [Ty.STRING],
                                              result = Ty.UNIT}),
  (Sy.symbol "flush",               FunEntry{ formals = [],
                                              result = Ty.UNIT}),
  (Sy.symbol("getchar"),            FunEntry{ formals=[],
                                              result=Types.STRING}),
  (Sy.symbol("ord"),                FunEntry{ formals=[Types.STRING],
                                              result=Types.INT}),
  (Sy.symbol("chr"),                FunEntry{ formals=[Types.INT],
                                              result=Types.STRING}),
  (Sy.symbol("size"),               FunEntry{ formals=[Types.STRING],
                                              result=Types.INT}),
  (Sy.symbol("substring"),          FunEntry{ formals=[Types.STRING,Types.INT,Types.INT],
                                              result=Types.STRING}),
  (Sy.symbol("concat"),             FunEntry{ formals=[Types.STRING,Types.STRING],
                                              result=Types.STRING}),
  (Sy.symbol("not"),                FunEntry{ formals=[Types.INT],
                                              result=Types.INT}),
  (Sy.symbol("exit"),               FunEntry{ formals=[Types.INT],
                                              result=Types.UNIT})
]

end (* Env *)

