#!/bin/bash
: ${1?"Usage: $0 <filename>"}

if ! [ -x "$(command -v a2ps)" ]; 
then 
    echo "Error: a2ps is not installed" >&2
    exit 1
fi 

if ! [ -x "$(command -v ps2pdf)" ];
then    
    echo "Error: ps2pdf is not installed" >&2
    exit 1
fi

case "$(uname -s)" in 
   Linux*) if [ -x "$(command -v evince)" ]; 
                then PDFVIEW=evince
                else if [ -x "$(command -v atril)" ];
                         then PDFVIEW=atril
                         else 
                              echo "Error: cannot detect pdf viewer" >&2
                         exit 1
                     fi  
                fi;;
   Darwin*) PDFVIEW=open
esac

TMPNAME=`mktemp` 
a2ps --medium=a4 -2 --line-numbers=1 --chars-per-line=80 --margin=0 --highlight-level=heavy $1 -o $TMPNAME.ps
ps2pdf -sPAPERSIZE=a4  -sOutputFile=$TMPNAME.pdf $TMPNAME.ps
$PDFVIEW $TMPNAME.pdf & 

