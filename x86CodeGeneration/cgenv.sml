structure CgEnv :> CGENV =
struct

structure Ty = Types
structure S = Symbol

datatype enventry
  = VarEntry of
    { ty     : Ty.ty
    , escape : bool ref
    , level  : int (* level and offset encapsulate the access *)
    , offset : int (* information  *)
    }
  | FunEntry of
    { formals: Ty.ty list
    , result : Ty.ty
    , gid    : ll.gid
    , sl_type: ll.ty
    , level  : int }

val baseTenv =
  S.enter (
      S.enter ( S.empty
	      , S.symbol ("int"), (Ty.INT, ll.I64)),
      S.symbol ("string"), (Ty.STRING, ll.Ptr (ll.I8) ))



val standard_lib_fns =
      [("print",     "print",     [Ty.STRING],               Ty.UNIT),
       ("flush",     "flush",     [],                       Ty.UNIT),
       ("getchar",   "getChar",   [],                       Ty.STRING),
       ("ord",       "ord",       [Ty.STRING],               Ty.INT),
       ("chr",       "chr",       [Ty.INT],                  Ty.STRING),
       ("size",      "size",      [Ty.STRING],               Ty.INT),
       ("substring", "substring", [Ty.STRING, Ty.INT, Ty.INT], Ty.STRING),
       ("concat",    "concat",    [Ty.STRING, Ty.STRING],     Ty.STRING),
       ("not",       "not",       [Ty.INT],                  Ty.INT),
       ("exit",      "exit_tig",  [Ty.INT],                  Ty.UNIT)
      ]



val baseVenv =
      let fun fn_aux ((sym, funName, fs, r), venv) =
            S.enter (venv, S.symbol (sym),
                     FunEntry{ formals=fs
			     , result = r
			     , gid    = S.symbol (funName) (* our runtime needs to match this *)
			     , sl_type = ll.Ptr (ll.I8)
			     , level  = 0
      })
      in foldl fn_aux S.empty standard_lib_fns
      end

end
