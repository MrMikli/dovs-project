(* AU Compilation 2016                  *)
(* DO NOT DISTRIBUTE                    *)


(* LLVM-- to x86 backend skeleton file  *)
(* Based on LLVMLite X86 backend@UPenn  *)




(* ll ir compilation -------------------------------------------------------- *)


signature X86BACKEND =
sig
  val compile_prog: ll.prog -> X86.prog
end


structure X86Backend :> X86BACKEND =
struct


structure S = Symbol

open X86
open ll


exception NotImplemented

fun TODO() = raise NotImplemented

(* Helpers ------------------------------------------------------------------ *)



fun convertOpToSymbol(oper) =
    case oper
     of ll.Gid (gid) => gid
      | ll.Id (uid) => uid
      | ll.Null => S.symbol("0")
      | ll.Const(i) => S.symbol (Int.toString i)

(* Platform-specific generation of symbols *)

val mangle =
 fn s =>
    (case LocalOS.os of
	LocalOS.Linux =>   (Symbol.name s)
     |  LocalOS.Darwin => "_" ^ (Symbol.name s))


(* Map ll comparison operations to X86 condition codes *)
fun compile_cnd (c:ll.cnd) : X86.cnd =
  case c of
    ll.Eq  => X86.Eq
  | ll.Ne  => X86.Neq
  | ll.Slt => X86.Lt
  | ll.Sle => X86.Le
  | ll.Sgt => X86.Gt
  | ll.Sge => X86.Ge



(* locals and layout -------------------------------------------------------- *)
(* We call the datastructure that maps each %uid to its stack slot a
   'stack layout'.  A stack layout maps a uid to an X86 operand for
   accessing its contents.  For this compilation strategy, the operand
   is always an offset from rbp (in bytes) that represents a storage slot in
   the stack.
*)

type layout = (ll.uid * X86.operand) list

(* A context contains the global type declarations (needed for getelementptr
   calculations) and a stack layout. *)
type ctxt = { tdecls : (ll.tid * ll.ty) list
            , layout : layout
            , poffset : int
            , pushes: int
            }

(* useful for looking up items in tdecls or layouts *)
fun lookup m x = let val (_, r) = (valOf (List.find (fn (y, _) => x = y) m))
                 in r
                 end

(* Get the int from a ll constant *)
fun i64toInt (ll.Const i) = i
(* should not happen *)
| i64toInt _ = 0






fun updateStack (ctxt as {tdecls = tdecls, layout = layout, poffset, pushes}) (lbl, oper) =
        (Movq, [oper, (lookup layout lbl)])
(* compiling operands  ------------------------------------------------------ *)

(* LLVM IR instructions support several kinds of operands.

   LL local %uids live in stack slots, whereas global ids live at
   global addresses that must be computed from a label.  Constants are
   immediately available, and the operand Null is the 64-bit 0 value.

     NOTE: two important facts about global identifiers:

     (1) You should use (mangle gid) to obtain a string
     suitable for naming a global label on your platform (macOS expects
     "_main" while linux expects "main").

     (2) 64-bit assembly labels are not allowed as immediate operands.
     That is, the X86 code: movq _gid %rax which looks like it should
     put the address denoted by _gid into %rax is not allowed.
     Instead, you need to compute an %rip-relative address using the
     leaq instruction:   leaq _gid(%rip).

   One strategy for compiling instruction operands is to use a
   designated register (or registers) for holding the values being
   manipulated by the LLVM IR instruction. You might find it useful to
   implement the following helper function, whose job is to generate
   the X86 instruction that moves an LLVM operand into a designated
   destination (usually a register).
*)


fun allocateStack (ctxt as {tdecls, layout, poffset, pushes}:ctxt) (lbl) =
    ({tdecls = tdecls, layout = (lbl, Ind3(Lit(poffset), Rbp))::layout, poffset = poffset - 8, pushes = pushes})

fun getFromStack {tdecls = tdecls, layout = layout, poffset, pushes} dest lbl =
        [(Movq, [lookup layout lbl, dest])] 

fun getPointerFromStack {tdecls = tdecls, layout = layout, poffset, pushes} dest oprnd =
        [(Movq, [(lookup layout (convertOpToSymbol oprnd)), dest])]



fun getGidFromStack {tdecls = tdecls, layout = layout, poffset, pushes} dest lbl =
let
  val offsetIndirect = lookup layout lbl
  val movvalue_inst = (Leaq, [offsetIndirect, dest])
in
  [movvalue_inst]
end

fun resetStack func (ctxt as {tdecls, layout, poffset, pushes}) =
          [(Leaq, [Ind3(Lit(8), Rbp),(Reg(Rsp))])] @ (getFromStack ctxt (Reg(Rbp)) func)

fun convertOpToX86 (ctxt:ctxt) operand =
  case operand of (ll.Const i) => (Imm(Lit i), [])
  | ll.Id(uid) => ((Reg(Rax), (getPointerFromStack ctxt (Reg(Rax)) operand)))
  | ll.Gid(gid) => (Reg(Rax), (getGidFromStack ctxt (Reg(Rax)) gid))
  | ll.Null => (Reg(Rax), [(Movq, [Imm(Lit(0)), Reg(Rax)])])

fun compile_operand (ctxt:ctxt) dest (oper:ll.operand) : ins list =
  let
    val (x86Op, insts) = convertOpToX86 ctxt (oper)
    val move_inst = (Movq, [x86Op, dest])
  in
    insts@[move_inst]
  end


fun compile_operator(bop) =
  case bop of ll.Add => Addq
    | ll.Sub   => Subq
    | ll.Mul  => Imulq
    | ll.SDiv => Idivq
    | ll.Shl  => Shlq
    | ll.Lshr => Shrq
    | ll.Ashr => Sarq
    | ll.And  => Andq
    | ll.Or   => Orq
    | ll.Xor  => Xorq

fun isOperatorReverse (bop) =
case bop of ll.Sub => true
| ll.Shl => true
| ll.Lshr => true
| ll.Ashr => true
| _ => false

(* compiling call  ---------------------------------------------------------- *)

(* You will probably find it helpful to implement a helper function that
   generates code for the LLVM IR call instruction.

   The code you generate should follow the x64 System V AMD64 ABI
   calling conventions, which places the first six 64-bit (or smaller)
   values in registers and pushes the rest onto the stack.  Note that,
   since all LLVM IR operands are 64-bit values, the first six
   operands will always be placed in registers.  (See the notes about
   compiling fdecl below.)

   [ NOTE: It is the caller's responsibility to clean up arguments
   pushed onto the stack, so you must free the stack space after the
   call returns. ]

   [ NOTE: Don't forget to preserve caller-save registers (only if
   needed). ]
*)


 fun llOpToSymbol (oper) =
   case oper of ll.Gid (gid) => gid
   | ll.Id (uid) => uid
   | ll.Null => S.symbol("0")
   | ll.Const(i) => S.symbol (Int.toString i)

fun mapPushes (ctxt:ctxt) (arg:(ty * ll.operand)) =
let
  val oper = compile_operand ctxt (Reg(Rax)) (#2 arg)
  val push_inst = (Pushq,[(Reg(Rax))])
in
  (oper@[push_inst])
end

fun load_args(ctxt,name,args:(ty * ll.operand) list) =
let
  val arg1 = if (List.length(args)) >= 1 then
            compile_operand ctxt (Reg(Rdi)) (#2 (List.nth (args,0)))
          else []
  val arg2 = if (List.length(args)) >= 2 then
            compile_operand ctxt (Reg(Rsi)) (#2 (List.nth (args,1)))
          else []
  val arg3 = if (List.length(args)) >= 3 then
            compile_operand ctxt (Reg(Rdx)) (#2 (List.nth (args,2)))
          else []
  val arg4 = if (List.length(args)) >= 4 then
            compile_operand ctxt (Reg(Rcx)) (#2 (List.nth (args,3)))
          else []
  val arg5 = if (List.length(args)) >= 5 then
            compile_operand ctxt (Reg(R08)) (#2 (List.nth (args,4)))
          else []
  val arg6 = if (List.length(args)) >= 6 then
            compile_operand ctxt (Reg(R09)) (#2 (List.nth (args,5)))
          else []
in
  arg1 @ arg2 @ arg3 @ arg4 @ arg5 @ arg6
end

fun compile_call (ctxt as{tdecls, layout, poffset, pushes}
:ctxt,uid,retTy,name,args:(ty * ll.operand) list) = 
 let
    val load_args = load_args(ctxt,name,args)
    val callAddress = (Leaq, [Ind3(Lbl(S.name(llOpToSymbol name)),Rip),(Reg(Rax))])
    val call = (Callq, [Reg(Rax)])
  
    val push_inst = if (S.name uid) = "" then [] else  [updateStack ctxt (uid,(Reg(Rax)))]
    (* if the args list is bigger than 6 then we separate it *)
    val pushRest = if List.length args > 6 then map (mapPushes ctxt) (List.rev(List.drop (args, 6)))
    else []

    val argPushes = List.length pushRest
    val popArgs = (Addq, [Imm(Lit(8*argPushes)), (Reg(Rsp))])

    val {tdecls = tdecls, layout = layout, poffset = poffset, pushes = pushes} = ctxt
    val newCtxt = {tdecls = tdecls, layout = layout, poffset = poffset, pushes = argPushes}
(* subtract offset but - 8 because it has ben increased by 8 from the call instruction *)
  in
    (load_args @ List.concat(pushRest) @[callAddress, call] @  push_inst @ [popArgs], newCtxt)
  end



(* compiling getelementptr (gep)  ------------------------------------------- *)

(* The getelementptr instruction computes an address by indexing into
   a datastructure, following a path of offsets.  It computes the
   address based on the size of the data, which is dictated by the
   data's type.

   To compile getelmentptr, you must generate x86 code that performs
   the appropriate arithemetic calculations.
*)




(* Function size_ty maps an LLVMlite type to a size in bytes.
   (needed for getelementptr)

   - the size of a struct is the sum of the sizes of each component
   - the size of an array of t's with n elements is n * the size of t
   - all pointers, I1, and I64 are 8 bytes
   - the size of a named type is the size of its definition

   - Void, i8, and functions have undefined sizes according to LLVMlite
     your function should simply return 0
*)

fun size_ty tdecls t : int = 
  case t
   of ll.Array(sz, ty) => sz * (size_ty tdecls ty)
    | ll.Struct(f) => size_str tdecls f
    | ll.Namedt(tid) => size_ty tdecls (lookup tdecls tid)
    | ll.Void => 0
    | ll.Fun(_) => 0
    | _ => 8


(* Need a recursive function to go through struct *)
and size_str tdecls (hd::tl) = (size_ty tdecls hd) + (size_str tdecls tl)
| size_str tdecls ([]) = 0


fun gepType (ctxt as {tdecls, layout, poffset, pushes}:ctxt) reg ty opers =
  case ty 
   of ll.Struct(fields) => gepStruct ctxt reg fields opers
    | ll.Namedt(tid) => gepType ctxt reg (lookup tdecls tid) opers
    | _ => gepArray ctxt reg ty opers
and gepStruct (ctxt as {tdecls, layout, poffset, pushes}:ctxt) reg fields opers =
  case opers
   of (hd::tl) => 
      let
        val operFields = List.take(fields, (i64toInt hd)) (* Gepping anything but an int in a record is illegal *)
        val sizeOffields = map (size_ty tdecls) operFields
        val sizeOfOpers = foldl Int.+ 0 sizeOffields (* Sums the size of the fields, Int.+ is just a function version of +, 0 is the initial value *)
      in
        [(Addq, [Imm(Lit(sizeOfOpers)), Reg(reg)])] @ (gepType ctxt reg (List.nth(fields, (i64toInt hd))) tl)
      end 
    | ([]) => []
and gepArray (ctxt as {tdecls, layout, poffset, pushes}:ctxt) reg ty opers =
  case opers
   of (hd::tl) =>
      let
        val cOp = compile_operand ctxt (Reg(R09)) hd
        val sizeOfcOp = (Imulq, [Imm(Lit(size_ty tdecls ty)), Reg(R09)])
      in
        cOp @ [sizeOfcOp, (Addq, [Reg(R09), Reg(reg)])] @ (gepType ctxt reg ty tl)
      end
    | ([]) => []


(* Function compile_gep generates code that computes a pointer value.

   1. op must be of pointer type: t*

   2. the value of op is the base address of the calculation

   3. the first index in the path is treated as the index into an array
     of elements of type t located at the base address

   4. subsequent indices are interpreted according to the type t:

     - if t is a struct, the index must be a constant n and it
       picks out the n'th element of the struct. [ NOTE: the offset
       within the struct of the n'th element is determined by the
       sizes of the types of the previous elements ]

     - if t is an array, the index can be any operand, and its
       value determines the offset within the array.

     - if t is any other type, the path is invalid

   5. if the index is valid, the remainder of the path is computed as
      in (4), but relative to the type f the sub-element picked out
      by the path so far
*)

 fun compile_gep (ctxt as {tdecls, layout, poffset, pushes})
                (res_uid)
                (ty, oper)
                (opers: ll.operand list) =
    let
      val getbase_inst = getPointerFromStack ctxt (Reg(Rdx)) oper
      val gep_inst = gepArray ctxt Rdx ty opers
      val result = updateStack ctxt (res_uid, Reg(Rdx))
    in
      if oper = ll.Null then
        (* TODO: Find out what the fuck this is, check report? *)
        ((getbase_inst  @ gep_inst @ [(Subq, [Reg(Rbp), Reg(Rdx)]), result]), ctxt)
      else
        ((getbase_inst  @ gep_inst @ [result]), ctxt)
    end




(* compiling instructions  -------------------------------------------------- *)

(* The result of compiling a single LLVM instruction might be many x86
   instructions.  We have not determined the structure of this code
   for you. Some of the instructions require only a couple assembly
   instructions, while others require more.  We have suggested that
   you need at least compile_operand, compile_call, and compile_gep
   helpers; you may introduce more as you see fit.

   Here are a few notes:

   - Icmp:  the Set instruction may be of use.  Depending on how you
     compile Cbr, you may want to ensure that the value produced by
     Icmp is exactly 0 or 1.

   - Load & Store: these need to dereference the pointers. Const and
     Null operands aren't valid pointers.  Don't forget to mmangle
     the global identifier.

   - Alloca: needs to return a pointer into the stack

   - Bitcast, Ptrtoint, Zext: do nothing interesting at the assembly level
*)

fun compile_alloca (ctxt as {tdecls, layout, poffset, pushes}:ctxt) uid ty =
case ty of ll.Struct(fields) => 
    (* Need to allocate memoery for each field. *)
    let
      val numberOfFields = List.length(fields)
      val sizeOfTy = size_ty tdecls (ll.Struct(fields))
      val alloca = updateStack ctxt (uid,(Reg(Rsp)))
    in
      ([(Subq, [Imm(Lit(sizeOfTy)), Reg(Rsp)])],
       {tdecls = tdecls, layout = layout, poffset = poffset-sizeOfTy, pushes = pushes})
    end
 | ll.Array(size,ty) =>
    let
      val alloca = updateStack ctxt (uid,(Reg(Rsp)))
      val sizeOfTy = size_ty tdecls ty 
    in
      ([(Subq, [Imm(Lit(sizeOfTy)), Reg(Rsp)]), alloca],
      {tdecls = tdecls, layout = layout, poffset = poffset-sizeOfTy, pushes = pushes})
    end
 | ll.Namedt(tid) => compile_alloca ctxt uid (lookup tdecls tid)
 | ll.I64 => alloca_int ctxt uid 
 | ll.I8 => alloca_int ctxt uid 
 | ll.I1 => alloca_int ctxt uid 
 | ll.Ptr(_) => alloca_int ctxt uid 
 | _ => ([], ctxt)

(* All integers can be allocated in the same way, so we make a separate function *)
and alloca_int (ctxt as {tdecls, layout, poffset, pushes}:ctxt) uid =
    let
      val alloca = updateStack ctxt (uid, (Reg(Rsp)))
    in
      ([(Subq, [Imm(Lit(8)), Reg(Rsp)]) , alloca],
      {tdecls = tdecls, layout = layout, poffset = poffset-8, pushes = pushes})
    end
fun compile_insn (ctxt as {tdecls, layout, poffset, pushes}) (uid_opt, i) : (X86.ins list * ctxt) =
let
  val uid = if uid_opt = NONE then S.symbol "" else Option.valOf uid_opt
  val instruction =
    case i of ll.Binop(ll.SDiv,ty,op1,op2) =>  
    let
      (* val reg1 = *)
      (* val reg2 = *)
      val src_reg = Reg(Rdi)
      val des_reg = Reg(Rsi)
      val op1_inst = compile_operand ctxt (src_reg) (op1)
      val op2_inst = compile_operand ctxt (des_reg) (op2)
      val operator = Idivq
      val moveop1_inst = (Movq, [src_reg, Reg(Rax)])
      val extend_inst = (Cqto, [])
      val add_inst = (operator, [des_reg])
      val push_inst = updateStack ctxt (uid, Reg(Rax))
      val move_inst = (Movq, [Reg(Rax), (Reg(Rax))]) (*in case of bugs check here. looks pretty suspicius*)
    in
      ((op1_inst@op2_inst@[moveop1_inst, extend_inst, add_inst
      , push_inst, move_inst]), ctxt)
    end
    
    | ll.Binop(bop,ty,op1,op2) => 
    let
      val reg1 =Reg(Rdi)
       val reg2 =Reg(Rsi)
      val src_reg = reg1
      val des_reg = reg2
      val op1_inst = compile_operand ctxt (reg1) (op1)
      val op2_inst = compile_operand ctxt (reg2) (op2)
      val operator = compile_operator(bop)
      val src_reg =       if isOperatorReverse bop then reg2 else reg1   (*fix the comparison issues*)
      val des_reg =  if isOperatorReverse bop then reg1 else reg2   (*fix the comparison issues*)
      val add_inst = (operator, [src_reg, des_reg])
      val push_inst = updateStack ctxt (uid, des_reg)
      val move_inst = (Movq, [des_reg, (Reg(Rax))])
    in
    ((op1_inst@op2_inst@[add_inst
      , push_inst, move_inst]), ctxt)
    end
    | ll.Alloca(ty) => compile_alloca ctxt uid ty
    | ll.Store(ty,op1,op2) =>  
    let

    val val_instrctuction  = compile_operand ctxt (Reg(Rbx)) op1
    val destination_instruction = compile_operand ctxt (Reg(Rcx)) op2
    (* get indirect register *)
    val indirect_destination_reg = Ind2(Rcx) 
    (* generate the mov instruction for storing the value *)
    val move_instrutions = (Movq, [Reg(Rbx), indirect_destination_reg])
  in
    (val_instrctuction@destination_instruction@[move_instrutions], ctxt)
  end

    | ll.Load(ty,op1) => 
      let
         val location_instruction  = compile_operand ctxt (Reg(Rbx)) op1
         (* get the indirect reg location *)
         val indirect_loc = Ind2(Rbx)
         val moveVal_inst = (Movq, [indirect_loc, Reg(Rcx)])
         (* add the value to the stack *)
         val push_val_inst = updateStack ctxt (uid, Reg(Rcx))
      in
        (location_instruction@[moveVal_inst, push_val_inst], ctxt)
      end
    | ll.Call (retTy,name,args)     => compile_call(ctxt,uid,retTy,name,args)
    | ll.Bitcast (ty,oper,ty2)       => 
        let
          val oper_label = convertOpToSymbol oper
          val reg = (Reg(Rax))
          val oper_list = (compile_operand ctxt reg oper)
          val pushes = updateStack ctxt (uid, (Reg(Rax)))
        in
          (oper_list @ [pushes:(X86.ins)], ctxt:ctxt)
        end

    | ll.Zext (ty1,oper,ty2)        => 
        let
          (* push load to rename *)
          val opers = compile_operand ctxt (Reg(Rax)) oper
          val pushes = updateStack ctxt (uid, (Reg(Rax)))
        in
          (opers @ [pushes:(X86.ins)], ctxt:ctxt)
        end

    | ll.Icmp (cnd, ty, oper1, oper2) => 
        let
          val compiler_oper1 = compile_operand ctxt  (Reg(Rcx)) oper1
          val compiled_oper2 = compile_operand ctxt  (Reg(Rdx)) oper2
          val compiled_cnd = compile_cnd cnd
          val compare_instruction = (Cmpq,[Reg(Rcx),Reg(Rdx)])
          val prepare_stack_instruction = (Pushq,[Imm(Lit(0))])
          val set_result_instruction = (Set(compiled_cnd),[Ind2(Rsp)])
          val result_instruction = (Movq, [Ind2(Rsp), Reg(Rax)])
          val  updateStack = updateStack ctxt (uid,Reg(Rax))
          val instructions = compiler_oper1 @ compiled_oper2 @ [compare_instruction]

          @[prepare_stack_instruction, set_result_instruction, result_instruction]@[updateStack]
        in
          (instructions, ctxt)
        end
    | ll.Ptrtoint(ty1,oper,ty2) => 
        let
          (* push load to rename *)
          val oper_list = compile_operand ctxt (Reg(Rax)) oper
          (* update the stack *)
          val pushes = updateStack ctxt (uid, (Reg(Rax)))
        in
          (oper_list @ [pushes:(X86.ins)], ctxt:ctxt)
        end

    | ll.Gep (ty, oper, operList)   => (compile_gep ctxt uid (ty, oper) operList)
    (* the implementation uses something with array . we don't have array implementation in llcodegen *)
in
  (instruction)
end


(* compiling terminators  --------------------------------------------------- *)

(* Compile block terminators is not too difficult:

   - Ret should properly exit the function: freeing stack space,
     restoring the value of %rbp, and putting the return value (if
     any) in %rax.

   - Br should jump

   - Cbr branch should treat its operand as a boolean conditional
*)

fun compile_terminator func ctxt t =
(case t
  of ll.Ret(ty, opt) =>
      let
        val ret =
            if opt <> NONE
            then compile_operand ctxt (Reg(Rax)) (Option.valOf opt)
            else []

        (* handles resetting the stack, freeing the space and restoring the value of %rbp *)
        val reset = resetStack func ctxt
      in
        ret
         @ reset @ [(Retq,[])]
      end
   | ll.Br (lbl) => [(Jmp,[Imm(Lbl(S.name lbl))])]
   | ll.Cbr(oper,lbl1,lbl2) => (compile_operand ctxt (Reg(R10)) oper) @
                               [(Cmpq, [Imm(Lit(1)) , Reg(R10)])] @
                               [(J(X86.Neq), [Imm(Lbl(S.name lbl1))])] @
                               [(Jmp , [Imm(Lbl(S.name lbl2))])])



(* compiling blocks --------------------------------------------------------- *)

(* We have left this helper function here for you to complete. *)
fun compile_block func ctxt {insns, terminator} : (ins list * ctxt) = 
    let
      val (insns', ctxt') = foldl do_insns ([], ctxt) insns
      val term = compile_terminator func ctxt' terminator
    in
      (insns' @ term, ctxt')
    end

(* Simple helper function to allow us to told the instructions *)
and do_insns (ins, (insns, ctxt)) =
    let
      val (ins', ctxt') = compile_insn ctxt ins
      val ins'' = insns @ ins' 
    in
      (ins'', ctxt')
    end


fun compile_lbl_block func lbl ctxt blk : (elem * ctxt) =
    let
      val (insns', ctxt') = (compile_block func ctxt blk)
    in
      (Asm.text (S.name lbl) insns', ctxt')
    end
  


(* compile_fdecl ------------------------------------------------------------ *)


(* This helper function computes the location of the nth incoming
   function argument: either in a register or relative to %rbp,
   according to the calling conventions.  You might find it useful for
   compile_fdecl.

   [ NOTE: the first six arguments are numbered 0 .. 5 ]
*)

fun arg_loc (n : int) : X86.operand = TODO ()

(* The code for the entry-point of a function must do several things:
l
   - since our simple compiler maps local %uids to stack slots,
     compiling the control-flow-graph body of an fdecl requires us to
     compute the layout (see the discussion of locals and layout)

   - the function code should also comply with the calling
     conventions, typically by moving arguments out of the parameter
     registers (or stack slots) into local storage space.  For our
     simple compilation strategy, that local storage space should be
     in the stack. (So the function parameters can also be accounted
     for in the layout.)

   - the function entry code should allocate the stack storage needed
     to hold all of the local stack slots.
*)

(* Only handles 6 parameters since we're only given 6 registers. *)

fun function_prologue (ctxt as {tdecls, layout, poffset, pushes}:ctxt) (p, cfg) = 
  let
    val noOfPar =  List.length p
    val (ctxt, arg1) = if noOfPar > 0 then auStack ctxt ((List.nth (p,0)), (Reg(Rdi))) else (ctxt, [])
    val (ctxt, arg2) = if noOfPar > 1 then auStack ctxt ((List.nth (p,1)), (Reg(Rsi))) else (ctxt, [])
    val (ctxt, arg3) = if noOfPar > 2 then auStack ctxt ((List.nth (p,2)), (Reg(Rdx))) else (ctxt, [])
    val (ctxt, arg4) = if noOfPar > 3 then auStack ctxt ((List.nth (p,3)), (Reg(Rcx))) else (ctxt, [])
    val (ctxt, arg5) = if noOfPar > 4 then auStack ctxt ((List.nth (p,4)), (Reg(R08))) else (ctxt, [])
    val (ctxt, arg6) = if noOfPar > 5 then auStack ctxt ((List.nth (p,5)), (Reg(R09))) else (ctxt, [])

    val layout' = if noOfPar > 6
                  then foldArgs (List.drop (p,6)) layout 1
                  else layout
    
    val ctxt' = alloca_l ctxt cfg
  in
    (ctxt', arg1 @ arg2 @ arg3 @ arg4 @ arg5 @arg6)
  end
and function_epilogue ctxt () = ([], ctxt)
and auStack (ctxt:ctxt) (uid, operand) =
  let
    val ctxt' = allocateStack ctxt (uid)
  in
    (ctxt', [updateStack ctxt' (uid, operand)])
  end
and foldArgs params layout i =
  case params
    of (hd::tl) => (hd, Ind3(Lit(8+(8*i)),(Rbp))) :: (foldArgs tl layout (i+1)) @ layout (* change offset by 8 *)
    | ([]) => layout
and alloca_l ctxt (blk, blks) =
  let
    val blocks = map (fn (lbl, blk') => blk') blks
    val blocks' = blk::blocks
    val uids = List.concat(map getUids blocks')
  in
    foldl alloca_uid (ctxt) uids
  end
and getUids { insns: (uid option * insn) list, terminator: terminator } = 
  let
    fun getUid ((SOME(uid), _), ids) = ids@[uid]
    | getUid ((NONE, _), ids) = ids
  in 
    foldl getUid [] insns
  end 
and alloca_uid (uid, (ctxt)) = allocateStack ctxt (uid)
and list_blocks func ((lbl,block), (elems, (ctxt:ctxt))) =
  let
    val (elem, ctxt') = compile_lbl_block func lbl ctxt block
  in
    (elems @ [elem], ctxt')
  end
and pushOnStack (ctxt as {tdecls, layout, poffset, pushes}:ctxt) (lbl, oper) = 
  let
    val layout' = (lbl, Ind3(Lit(poffset), Rbp))::layout
  in
    ({tdecls = tdecls, layout = layout', poffset = poffset - 8, pushes = pushes},
     (Pushq, [oper]))
  end

fun compile_fdecl lo tdecls (name:ll.gid) ({ fty, param, cfg }:ll.fdecl) =
  let
    val ctxt = {layout = (S.symbol("0"), Reg(Rbp))::lo, tdecls = tdecls, poffset = 0, pushes = 0}

    (* Updating frame pointer *)
    val (ctxt', fp_old) = pushOnStack ctxt (name, Reg(Rbp))
    val fp = (Movq, [Reg(Rsp), Reg(Rbp)])

    val (ctxt', params) = function_prologue ctxt' (param, cfg)

    val initialStack = (Addq, [Imm(Lit(#poffset ctxt')), Reg(Rsp)])
    val blockOne = (#1 cfg)
    val (blockInsn, ctxt') = (compile_block name ctxt' blockOne)

    val blockRest:(lbl * block) list = (#2 cfg)
    val (blockInsns, ctxt') = foldl (list_blocks name) ([], ctxt') blockRest

    val (remaining, ctxt') = function_epilogue ctxt' ()
    val returns = [fp_old, fp, initialStack] @ params @ blockInsn @ remaining
  in
    [{asm = Text (returns), global = true, lbl = S.name name}] @ blockInsns
  end



(* compile_gdecl ------------------------------------------------------------ *)

(* Compile a global value into an X86 global data declaration and map
   a global uid to its associated X86 label.
*)

fun compile_ginit GNull        = [Quad (Lit 0)]
  | compile_ginit (GGid gid)   = [Quad (Lbl (mangle gid))]
  | compile_ginit (GInt c)     = [Quad (Lit c)]
  | compile_ginit (GString s)  = [Asciz s]
  | compile_ginit (GArray gs)  = List.concat (List.map compile_gdecl gs)
  | compile_ginit (GStruct gs) = List.concat (List.map compile_gdecl gs)

and compile_gdecl (_, g) = compile_ginit g

(* compile_prog ------------------------------------------------------------- *)

fun compile_prog ({tdecls, gdecls, fdecls}:ll.prog) : X86.prog =
  let
    val layout = map (fn(x,y)=> (x,Ind3(Lbl(mangle x),Rip))) gdecls
    fun g (lbl, gdecl) = Asm.data (mangle lbl) (compile_gdecl gdecl)
    fun f layout (name, fdecl) = compile_fdecl layout tdecls name fdecl
  in
    (List.map g gdecls) @ (List.concat (List.map (f layout) fdecls))
  end

end (* structure X86Backend *)
