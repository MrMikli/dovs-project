%Ttigermain0 = type { i1, i64 }
%TUaddint34 = type { i8*, i64 }
%TUadda910 = type { i8*, i64 }

define i64 @Uadda9 (i8* %statLq, i64 %Lc12) {
 %Ulocals11 = alloca %TUadda910
 %UgepArg13 = getelementptr %TUadda910, %TUadda910* %Ulocals11, i32 0, i32 0
 store i8* %statLq, i8** %UgepArg13
 %UgepArg14 = getelementptr %TUadda910, %TUadda910* %Ulocals11, i32 0, i32 1
 store i64 %Lc12, i64* %UgepArg14
 %Ugetpointer16 = getelementptr %TUadda910, %TUadda910* %Ulocals11, i32 0, i32 0
 %UloadVarPtr17 = load i8*, i8** %Ugetpointer16
 %UbitcastLocals18 = bitcast i8* %UloadVarPtr17 to %TUaddint34*
 %Ugetpointer19 = getelementptr %TUaddint34, %TUaddint34* %UbitcastLocals18, i32 0, i32 0
 %UloadVarPtr20 = load i8*, i8** %Ugetpointer19
 %UbitcastLocals21 = bitcast i8* %UloadVarPtr20 to %Ttigermain0*
 %Ua22 = getelementptr %Ttigermain0, %Ttigermain0* %UbitcastLocals21, i32 0, i32 1
 %UloadVar23 = load i64, i64* %Ua22
 %Uc24 = getelementptr %TUadda910, %TUadda910* %Ulocals11, i32 0, i32 1
 %UloadVar25 = load i64, i64* %Uc24
 %Ubinop26 = add i64 %UloadVar23, %UloadVar25
 ret i64 %Ubinop26
}

define i64 @Uaddint3 (i8* %statLq, i64 %Lb6) {
 %Ulocals5 = alloca %TUaddint34
 %UgepArg7 = getelementptr %TUaddint34, %TUaddint34* %Ulocals5, i32 0, i32 0
 store i8* %statLq, i8** %UgepArg7
 %UgepArg8 = getelementptr %TUaddint34, %TUaddint34* %Ulocals5, i32 0, i32 1
 store i64 %Lb6, i64* %UgepArg8
 %UbitcastLocalsToType29 = bitcast %TUaddint34* %Ulocals5 to i8*
 %Ub27 = getelementptr %TUaddint34, %TUaddint34* %Ulocals5, i32 0, i32 1
 %UloadVar28 = load i64, i64* %Ub27
 %Ucall30 = call i64 @Uadda9 (i8* %UbitcastLocalsToType29, i64 %UloadVar28)
 ret i64 %Ucall30
}

define i64 @tigermain () {
 %Ulocals1 = alloca %Ttigermain0
 %Ua2 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 store i64 5, i64* %Ua2
 %UbitcastLocalsToType31 = bitcast %Ttigermain0* %Ulocals1 to i8*
 %Ucall32 = call i64 @Uaddint3 (i8* %UbitcastLocalsToType31, i64 3)
 ret i64 %Ucall32
}

declare i8* @allocRecord(i64)
declare i8* @initArray (i64, i64, i8*)
declare i64 @stringEqual (i8*, i8*)
declare i64 @stringNotEq (i8*, i8*)
declare i64 @stringLess (i8*, i8*)
declare i64 @stringLessEq (i8*, i8*)
declare i64 @stringGreater (i8*, i8*)
declare i64 @stringGreaterEq (i8*, i8*)
declare void @print    (i8*, i8*)
declare void @flush    (i8*)
declare i8*  @getChar  (i8*)
declare i64  @ord      (i8*, i8*)
declare i8*  @chr      (i8*, i64)
declare i64  @size     (i8*, i8*)
declare i8*  @substring(i8*, i8*, i64, i64)
declare i8*  @concat   (i8*, i8*, i8*)
declare i64  @not      (i8*, i64)
declare void @exit     (i8*, i64)
declare i64  @exponent (i64, i64)
target triple = "x86_64-pc-linux-gnu"
