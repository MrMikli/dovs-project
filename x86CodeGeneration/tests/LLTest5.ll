%Ttigermain0 = type { i1, i64, i64 }

define i64 @tigermain () {
 %Uhi8 = alloca i64
 %Ulocals1 = alloca %Ttigermain0
 %Ua2 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 store i64 0, i64* %Ua2
 %Ui6 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 store i64 0, i64* %Ui6
 %Ui7 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 store i64 9, i64* %Uhi8
 br label %Ltest3
Ltest3:
 %Uit_comp11 = load i64, i64* %Ui7
 %Uhi_comp12 = load i64, i64* %Uhi8
 %Utest13 = icmp sle i64 %Uit_comp11, %Uhi_comp12
 br i1 %Utest13, label %Lbody4, label %Lmerge5
Lbody4:
 %Ua14 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %Ua16 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %UloadVar17 = load i64, i64* %Ua16
 %Ubinop18 = add i64 %UloadVar17, 5
 store i64 %Ubinop18, i64* %Ua14
 %Ua22 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %UloadVar23 = load i64, i64* %Ua22
 %Ucmpop_bool24 = icmp sge i64 %UloadVar23, 25
 %Ucmpop_i25 = zext i1 %Ucmpop_bool24 to i64
 %Utest27 = icmp ne i64 %Ucmpop_i25, 0
 br i1 %Utest27, label %Lthen19, label %Lmerge20
Lthen19:
 br label %Lmerge5
Lextra26:
 br label %Lmerge20
Lmerge20:
 %Uit9 = load i64, i64* %Ui7
 %Uinc10 = add i64 %Uit9, 1
 store i64 %Uinc10, i64* %Ui7
 br label %Ltest3
Lmerge5:
 %Ua28 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %UloadVar29 = load i64, i64* %Ua28
 ret i64 %UloadVar29
}

declare i8* @allocRecord(i64)
declare i8* @initArray (i64, i64, i8*)
declare i64 @stringEqual (i8*, i8*)
declare i64 @stringNotEq (i8*, i8*)
declare i64 @stringLess (i8*, i8*)
declare i64 @stringLessEq (i8*, i8*)
declare i64 @stringGreater (i8*, i8*)
declare i64 @stringGreaterEq (i8*, i8*)
declare void @print    (i8*, i8*)
declare void @flush    (i8*)
declare i8*  @getChar  (i8*)
declare i64  @ord      (i8*, i8*)
declare i8*  @chr      (i8*, i64)
declare i64  @size     (i8*, i8*)
declare i8*  @substring(i8*, i8*, i64, i64)
declare i8*  @concat   (i8*, i8*, i8*)
declare i64  @not      (i8*, i64)
declare void @exit     (i8*, i64)
declare i64  @exponent (i64, i64)
target triple = "x86_64-pc-linux-gnu"
