%Ttigermain0 = type { i1, i64 }
%TUa36 = type { i8*, i64, i64 }
%TUb422 = type { i8*, i64, i64, i64 }
%TUc3637 = type { i8* }
%TUc547 = type { i8*, i64 }
%TUfoo5253 = type { i8*, i64 }

define i64 @Ua3 (i8* %statLq) {
 %Ulocals7 = alloca %TUa36
 %UgepArg8 = getelementptr %TUa36, %TUa36* %Ulocals7, i32 0, i32 0
 store i8* %statLq, i8** %UgepArg8
 %Ugetpointer9 = getelementptr %TUa36, %TUa36* %Ulocals7, i32 0, i32 0
 %UloadVarPtr10 = load i8*, i8** %Ugetpointer9
 %UbitcastLocals11 = bitcast i8* %UloadVarPtr10 to %Ttigermain0*
 %UbitcastLocalsToType12 = bitcast %Ttigermain0* %UbitcastLocals11 to i8*
 %Ucall13 = call i64 @Uc5 (i8* %UbitcastLocalsToType12, i64 0)
 %Ures14 = getelementptr %TUa36, %TUa36* %Ulocals7, i32 0, i32 1
 store i64 %Ucall13, i64* %Ures14
 %Ufive15 = getelementptr %TUa36, %TUa36* %Ulocals7, i32 0, i32 2
 store i64 5, i64* %Ufive15
 %Ures17 = getelementptr %TUa36, %TUa36* %Ulocals7, i32 0, i32 1
 %UloadVar18 = load i64, i64* %Ures17
 %Ufive19 = getelementptr %TUa36, %TUa36* %Ulocals7, i32 0, i32 2
 %UloadVar20 = load i64, i64* %Ufive19
 %Ubinop21 = mul i64 %UloadVar18, %UloadVar20
 ret i64 %Ubinop21
}

define i64 @Uc36 (i8* %statLq) {
 %Ulocals38 = alloca %TUc3637
 %UgepArg39 = getelementptr %TUc3637, %TUc3637* %Ulocals38, i32 0, i32 0
 store i8* %statLq, i8** %UgepArg39
 %Ugetpointer40 = getelementptr %TUc3637, %TUc3637* %Ulocals38, i32 0, i32 0
 %UloadVarPtr41 = load i8*, i8** %Ugetpointer40
 %UbitcastLocals42 = bitcast i8* %UloadVarPtr41 to %TUb422*
 %Ub43 = getelementptr %TUb422, %TUb422* %UbitcastLocals42, i32 0, i32 3
 %UloadVar44 = load i64, i64* %Ub43
 ret i64 %UloadVar44
}

define i64 @Ub4 (i8* %statLq, i64 %Larg124, i64 %Larg225) {
 %Ulocals23 = alloca %TUb422
 %UgepArg26 = getelementptr %TUb422, %TUb422* %Ulocals23, i32 0, i32 0
 store i8* %statLq, i8** %UgepArg26
 %UgepArg27 = getelementptr %TUb422, %TUb422* %Ulocals23, i32 0, i32 1
 store i64 %Larg124, i64* %UgepArg27
 %UgepArg28 = getelementptr %TUb422, %TUb422* %Ulocals23, i32 0, i32 2
 store i64 %Larg225, i64* %UgepArg28
 %Uarg130 = getelementptr %TUb422, %TUb422* %Ulocals23, i32 0, i32 1
 %UloadVar31 = load i64, i64* %Uarg130
 %Uarg232 = getelementptr %TUb422, %TUb422* %Ulocals23, i32 0, i32 2
 %UloadVar33 = load i64, i64* %Uarg232
 %Ubinop34 = add i64 %UloadVar31, %UloadVar33
 %Ub35 = getelementptr %TUb422, %TUb422* %Ulocals23, i32 0, i32 3
 store i64 %Ubinop34, i64* %Ub35
 %UbitcastLocalsToType45 = bitcast %TUb422* %Ulocals23 to i8*
 %Ucall46 = call i64 @Uc36 (i8* %UbitcastLocalsToType45)
 ret i64 %Ucall46
}

define i64 @Ufoo52 (i8* %statLq, i64 %Larg155) {
 %Ulocals54 = alloca %TUfoo5253
 %UgepArg56 = getelementptr %TUfoo5253, %TUfoo5253* %Ulocals54, i32 0, i32 0
 store i8* %statLq, i8** %UgepArg56
 %UgepArg57 = getelementptr %TUfoo5253, %TUfoo5253* %Ulocals54, i32 0, i32 1
 store i64 %Larg155, i64* %UgepArg57
 %Uarg158 = getelementptr %TUfoo5253, %TUfoo5253* %Ulocals54, i32 0, i32 1
 %UloadVar59 = load i64, i64* %Uarg158
 ret i64 %UloadVar59
}

define i64 @Uc5 (i8* %statLq, i64 %Larg149) {
 %Uifthenelse_result71 = alloca i64
 %Ulocals48 = alloca %TUc547
 %UgepArg50 = getelementptr %TUc547, %TUc547* %Ulocals48, i32 0, i32 0
 store i8* %statLq, i8** %UgepArg50
 %UgepArg51 = getelementptr %TUc547, %TUc547* %Ulocals48, i32 0, i32 1
 store i64 %Larg149, i64* %UgepArg51
 %Uarg163 = getelementptr %TUc547, %TUc547* %Ulocals48, i32 0, i32 1
 %UloadVar64 = load i64, i64* %Uarg163
 %Utest70 = icmp ne i64 %UloadVar64, 0
 br i1 %Utest70, label %Lthen60, label %Lelse61
Lthen60:
 %Ugetpointer65 = getelementptr %TUc547, %TUc547* %Ulocals48, i32 0, i32 0
 %UloadVarPtr66 = load i8*, i8** %Ugetpointer65
 %UbitcastLocals67 = bitcast i8* %UloadVarPtr66 to %Ttigermain0*
 %UbitcastLocalsToType68 = bitcast %Ttigermain0* %UbitcastLocals67 to i8*
 %Ucall69 = call i64 @Ua3 (i8* %UbitcastLocalsToType68)
 store i64 %Ucall69, i64* %Uifthenelse_result71
 br label %Lmerge62
Lelse61:
 store i64 1, i64* %Uifthenelse_result71
 br label %Lmerge62
Lmerge62:
 %Ures72 = load i64, i64* %Uifthenelse_result71
 ret i64 %Ures72
}

define i64 @tigermain () {
 %Ulocals1 = alloca %Ttigermain0
 %Ui2 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 store i64 0, i64* %Ui2
 %UbitcastLocalsToType76 = bitcast %Ttigermain0* %Ulocals1 to i8*
 %Ucall77 = call i64 @Ub4 (i8* %UbitcastLocalsToType76, i64 2, i64 3)
 %UbitcastLocalsToType78 = bitcast %Ttigermain0* %Ulocals1 to i8*
 %Ucall79 = call i64 @Uc5 (i8* %UbitcastLocalsToType78, i64 1)
 %Ubinop80 = add i64 %Ucall77, %Ucall79
 %UbitcastLocalsToType81 = bitcast %Ttigermain0* %Ulocals1 to i8*
 %Ucall82 = call i64 @Uc5 (i8* %UbitcastLocalsToType81, i64 0)
 %Ubinop83 = mul i64 %Ubinop80, %Ucall82
 %Ui84 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %UloadVar85 = load i64, i64* %Ui84
 %Ubinop86 = add i64 %Ubinop83, %UloadVar85
 ret i64 %Ubinop86
}

declare i8* @allocRecord(i64)
declare i8* @initArray (i64, i64, i8*)
declare i64 @stringEqual (i8*, i8*)
declare i64 @stringNotEq (i8*, i8*)
declare i64 @stringLess (i8*, i8*)
declare i64 @stringLessEq (i8*, i8*)
declare i64 @stringGreater (i8*, i8*)
declare i64 @stringGreaterEq (i8*, i8*)
declare void @print    (i8*, i8*)
declare void @flush    (i8*)
declare i8*  @getChar  (i8*)
declare i64  @ord      (i8*, i8*)
declare i8*  @chr      (i8*, i64)
declare i64  @size     (i8*, i8*)
declare i8*  @substring(i8*, i8*, i64, i64)
declare i8*  @concat   (i8*, i8*, i8*)
declare i64  @not      (i8*, i64)
declare void @exit     (i8*, i64)
declare i64  @exponent (i64, i64)
target triple = "x86_64-pc-linux-gnu"
