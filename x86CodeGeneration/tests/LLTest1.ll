%Ttigermain0 = type { i1, i64, i64, i64, i64 }

define i64 @tigermain () {
 %Ulocals1 = alloca %Ttigermain0
 %Ua2 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 store i64 5, i64* %Ua2
 %Ub3 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 store i64 6, i64* %Ub3
 %Uc4 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 3
 store i64 2, i64* %Uc4
 %Ud5 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 4
 store i64 0, i64* %Ud5
 %Ua6 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %Ua8 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %UloadVar9 = load i64, i64* %Ua8
 %Ubinop10 = mul i64 %UloadVar9, 3
 store i64 %Ubinop10, i64* %Ua6
 %Ub11 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 %Ub13 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 %UloadVar14 = load i64, i64* %Ub13
 %Ubinop15 = sub i64 %UloadVar14, 2
 store i64 %Ubinop15, i64* %Ub11
 %Uc16 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 3
 %Uc18 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 3
 %UloadVar19 = load i64, i64* %Uc18
 %Uexpop20 = call i64 @exponent (i64 %UloadVar19, i64 3)
 store i64 %Uexpop20, i64* %Uc16
 %Ua21 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %Ubinop23 = sdiv i64 4, 2
 store i64 %Ubinop23, i64* %Ua21
 %Ub24 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 %Uexpop26 = call i64 @exponent (i64 2, i64 2)
 store i64 %Uexpop26, i64* %Ub24
 %Uc27 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 3
 %Uc30 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 3
 %UloadVar31 = load i64, i64* %Uc30
 %Uc32 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 3
 %UloadVar33 = load i64, i64* %Uc32
 %Ubinop34 = add i64 %UloadVar31, %UloadVar33
 %Uc35 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 3
 %UloadVar36 = load i64, i64* %Uc35
 %Ubinop37 = add i64 %Ubinop34, %UloadVar36
 store i64 %Ubinop37, i64* %Uc27
 %Ud38 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 4
 %Ua41 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %UloadVar42 = load i64, i64* %Ua41
 %Ub43 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 %UloadVar44 = load i64, i64* %Ub43
 %Ubinop45 = add i64 %UloadVar42, %UloadVar44
 %Uc46 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 3
 %UloadVar47 = load i64, i64* %Uc46
 %Ubinop48 = add i64 %Ubinop45, %UloadVar47
 store i64 %Ubinop48, i64* %Ud38
 %Ud49 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 4
 %UloadVar50 = load i64, i64* %Ud49
 ret i64 %UloadVar50
}

declare i8* @allocRecord(i64)
declare i8* @initArray (i64, i64, i8*)
declare i64 @stringEqual (i8*, i8*)
declare i64 @stringNotEq (i8*, i8*)
declare i64 @stringLess (i8*, i8*)
declare i64 @stringLessEq (i8*, i8*)
declare i64 @stringGreater (i8*, i8*)
declare i64 @stringGreaterEq (i8*, i8*)
declare void @print    (i8*, i8*)
declare void @flush    (i8*)
declare i8*  @getChar  (i8*)
declare i64  @ord      (i8*, i8*)
declare i8*  @chr      (i8*, i64)
declare i64  @size     (i8*, i8*)
declare i8*  @substring(i8*, i8*, i64, i64)
declare i8*  @concat   (i8*, i8*, i8*)
declare i64  @not      (i8*, i64)
declare void @exit     (i8*, i64)
declare i64  @exponent (i64, i64)
target triple = "x86_64-pc-linux-gnu"
