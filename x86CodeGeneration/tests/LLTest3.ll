%Ttigermain0 = type { i1, i64, i64 }

define i64 @tigermain () {
 %Ulocals1 = alloca %Ttigermain0
 %Ua2 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 store i64 1, i64* %Ua2
 %Ub3 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 store i64 1, i64* %Ub3
 br label %Ltest4
Ltest4:
 %Ua8 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %UloadVar9 = load i64, i64* %Ua8
 %Ucmpop_bool10 = icmp slt i64 %UloadVar9, 20
 %Ucmpop_i11 = zext i1 %Ucmpop_bool10 to i64
 %UtestLoop40 = icmp ne i64 %Ucmpop_i11, 0
 br i1 %UtestLoop40, label %Lbody5, label %Lmerge6
Lbody5:
 %Ua12 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %Ua14 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 1
 %UloadVar15 = load i64, i64* %Ua14
 %Ubinop16 = add i64 %UloadVar15, 1
 store i64 %Ubinop16, i64* %Ua12
 br label %Ltest17
Ltest17:
 %Ub21 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 %UloadVar22 = load i64, i64* %Ub21
 %Ucmpop_bool23 = icmp slt i64 %UloadVar22, 20
 %Ucmpop_i24 = zext i1 %Ucmpop_bool23 to i64
 %UtestLoop39 = icmp ne i64 %Ucmpop_i24, 0
 br i1 %UtestLoop39, label %Lbody18, label %Lmerge19
Lbody18:
 %Ub28 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 %UloadVar29 = load i64, i64* %Ub28
 %Ucmpop_bool30 = icmp sge i64 %UloadVar29, 5
 %Ucmpop_i31 = zext i1 %Ucmpop_bool30 to i64
 %Utest33 = icmp ne i64 %Ucmpop_i31, 0
 br i1 %Utest33, label %Lthen25, label %Lmerge26
Lthen25:
 br label %Lmerge19
Lextra32:
 br label %Lmerge26
Lmerge26:
 %Ub34 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 %Ub36 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 %UloadVar37 = load i64, i64* %Ub36
 %Ubinop38 = add i64 %UloadVar37, 1
 store i64 %Ubinop38, i64* %Ub34
 br label %Ltest17
Lmerge19:
 br label %Ltest4
Lmerge6:
 %Ub41 = getelementptr %Ttigermain0, %Ttigermain0* %Ulocals1, i32 0, i32 2
 %UloadVar42 = load i64, i64* %Ub41
 ret i64 %UloadVar42
}

declare i8* @allocRecord(i64)
declare i8* @initArray (i64, i64, i8*)
declare i64 @stringEqual (i8*, i8*)
declare i64 @stringNotEq (i8*, i8*)
declare i64 @stringLess (i8*, i8*)
declare i64 @stringLessEq (i8*, i8*)
declare i64 @stringGreater (i8*, i8*)
declare i64 @stringGreaterEq (i8*, i8*)
declare void @print    (i8*, i8*)
declare void @flush    (i8*)
declare i8*  @getChar  (i8*)
declare i64  @ord      (i8*, i8*)
declare i8*  @chr      (i8*, i64)
declare i64  @size     (i8*, i8*)
declare i8*  @substring(i8*, i8*, i64, i64)
declare i8*  @concat   (i8*, i8*, i8*)
declare i64  @not      (i8*, i64)
declare void @exit     (i8*, i64)
declare i64  @exponent (i64, i64)
target triple = "x86_64-pc-linux-gnu"
