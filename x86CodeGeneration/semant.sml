(* AU Compilation 2015.
 *
 * This the main working file for Part 4 - Semantic Analysis
 * Main.compile("../testcases/xTest13.tig", "out/xTest13.ll");
 *)
(* Our signature that for the semantic interface exposes only one function. *)

signature SEMANT =
sig
  val transProg: Absyn.exp -> TAbsyn.exp
end

structure Semant :> SEMANT =
struct


structure S = Symbol
structure A = Absyn
structure E = Env
structure Ty = Types
structure PT = PrintTypes
structure TAbs = TAbsyn

(* Use the extra record to add more information when traversing the tree
   It should become obvious when you actually need it, what to do.
   Alternatively, you have to add extra parameters to your functions *)

type extra = {}
 val func_nest_level : int ref = ref 0
 val loop_nest_level : int ref = ref 0
 val immutables : S.symbol list = []

fun increaseLoopNest () = loop_nest_level := !loop_nest_level + 1
fun decreaseLoopNest () = loop_nest_level := !loop_nest_level - 1

fun increaseFuncNest () = func_nest_level := !func_nest_level + 1
fun decreaseFuncNest () = func_nest_level := !func_nest_level - 1

(* placehloder for declarations, the final code should compile without this *)
val TODO_DECL = TAbs.TypeDec [] (* Delete when possible *)

(* Error messages *)

val err = ErrorMsg.error

fun out msg pos = err pos msg

fun errorInt (pos, ty) =
    err pos ("INT required, " ^ PT.asString ty ^ " provided")

fun errorUnit (pos, ty) =
    err pos ("UNIT required, " ^ PT.asString ty ^ " provided")

fun errorNil (pos, id) =
    err pos ("need to give " ^ S.name id ^ " a type when assigning the value nil")

fun errorSameNameDecl (pos, id) =
    err pos ("The variable " ^ S.name id ^ " is declared multiple times.")

fun errorNotDefined(pos ) =
err pos ("Variable is not defined at position ")

fun errorSameNameRecordField (pos, id) =
    err pos (S.name id ^ " is used multiple times as a field in the record")

fun errorNoReference(pos, name) =
  err pos ("No reference for: " ^ Symbol.name(name))

fun errorFieldNotInRecord(pos, id)=
    err pos ("The field " ^ S.name id ^ " does not exist in the record")

fun errorTypesNotCompatibleRecord (pos, ty) =
    err pos ("Incompatible types: expected RECORD found " ^ PT.asString ty)

fun errorIncompatibleTypes(pos,  ty1, ty2) =
    err pos ("Incompatible types: expected " ^ PT.asString ty1 ^ " found " ^ PT.asString ty2)

fun errorNilType(pos, id) = 
    err pos ("Tried to declare variable as nil: " ^ S.name id)

fun errorStr (pos, ty) =
    err pos ("STRING required, " ^ PT.asString ty ^ " provided")

(* Write additional error messages here *)

exception NotImplemented
exception InvalidOperator
exception TooManyFeatures (* shortcut *)
exception ThisShouldBeProperErrorMessage

(* -- Helper functions -- *)

(* Use the below three funs like:
   throw errorUnit (pos,ty) ifFalse isUnit(_) *)

fun ifTrue test err args = if test
                           then err args
                           else ()

fun ifFalse test err args = if test
                            then ()
                            else err args

fun throw err args testFun test  = testFun test err args


fun EqualTypes (Ty.ARRAY(_, u1), Ty.ARRAY(_, u2)) = u1 = u2
  | EqualTypes (Ty.RECORD(_, u1), Ty.RECORD(_, u2)) = u1 = u2
  | EqualTypes (Ty.RECORD(_, _), Ty.NIL) = true
  | EqualTypes (Ty.NIL, Ty.RECORD(_, _)) = true
  | EqualTypes (a, b) = a = b



fun checkTypesEqual (ty1, ty2, pos, errMsg) = if EqualTypes(ty1, ty2)
                                                  then ()
else err pos (errMsg)




fun lookupTy tenv sym pos =
    let
        val tyOpt = S.look (tenv, sym)
    in
        if tyOpt = NONE 
        then Ty.ERROR
        else valOf tyOpt
    end


fun actualTy (Ty.NAME (s, ty)) pos = let
                                        val ty' = valOf (!ty)
                                     in
                                        actualTy ty' pos
                                     end
  |  actualTy t _ = t

fun lookupActualTy tenv sym pos = actualTy (lookupTy tenv sym pos) pos



fun checkInt (ty, pos) =
    case ty
     of Ty.INT => ()
      | Ty.ERROR => ()
      | _ => errorInt (pos, ty)

fun checkIntAndReturnExp ({exp, ty}, pos) =
    ((case ty
     of Ty.INT => ()
      | Ty.ERROR => ()
      | _ => errorInt (pos, ty));
      {exp = exp, ty = ty})

fun checkString ({exp, ty}, pos) =
    ((case ty of
        Ty.STRING => ()
      | Ty.ERROR => ()
      | _ => errorStr (pos, ty));
      exp)

fun isInt (ty) =
    case ty
     of Ty.INT => true
      | _ => false

fun isUnit (ty) =
    case ty
     of Ty.UNIT => true
      | Ty.ERROR => true
      | _ => false

(* check if the assignment can be done *)

fun checkAssignable (declared: Ty.ty, assigned: Ty.ty, pos, msg) =
    let
        val aDeclared = actualTy declared pos
        val aAssigned = actualTy assigned pos
    in
        if
        aDeclared = aAssigned
        then true
        else
        (case (aDeclared, aAssigned) of
        (Ty.RECORD(_, _), Ty.NIL) => true
      | (Ty.NIL, Ty.RECORD(_, _)) => true
      | (Ty.ERROR, _) => true
      | (_, Ty.ERROR) => true
      | _ => false)
        
    end



fun transTy (tenv, t) = case t 
of A.NameTy(sym, pos) =>lookupTy tenv sym pos
| A.ArrayTy( sym, pos) => let
                            val ty = lookupTy tenv sym pos
                            in Ty.ARRAY(ty,ref())
                            end
|A.RecordTy(fieldlist)  => 
 let
              fun createRecord (usedNames, {name, escape, typ = (t, p), pos}::records) =
                    let
                       val _ = throw errorSameNameRecordField (pos, name) ifTrue (List.exists (fn y => name = y) usedNames)
                       val ty = lookupTy tenv t pos
                       val record_list = (name, ty)
                    in
                      record_list :: createRecord (name :: usedNames, records)
                    end
                | createRecord (_, []) = []
           in
             Ty.RECORD (createRecord ([], fieldlist), ref ())
           end                          


fun checkRecordField (name, Ty.RECORD (fields, ty)) =
    let
      val foundfield = List.find  (fn (fieldName, _) => fieldName = name) fields
    in
      foundfield <> NONE
    end
| checkRecordField(name, Ty.NAME(sym, tyOpt)) = checkRecordField(name, Option.valOf(!tyOpt))
| checkRecordField(name, _) = false

fun transExp (venv, tenv, extra : extra) =
    let
        (* this is a placeholder value to get started *)
        val TODO = {exp = TAbs.ErrorExp, ty = Ty.ERROR}

        fun trexp (A.NilExp) = { exp = TAbs.NilExp, ty = Ty.NIL }
          | trexp(A.IntExp i) = { exp = TAbs.IntExp i, ty = Ty.INT }
          | trexp(A.StringExp (s,pos)) = { exp = TAbs.StringExp s, ty = Ty.STRING }
        
            | trexp (A.VarExp var) = let
                                      val typed_var as {var = tcpvar, ty = tyvar} = trvar var
                                   in
                                   {exp = TAbs.VarExp typed_var ,ty = tyvar }
                                   end


             | trexp (A.SeqExp exps) =
               let
                  fun checkExps ((exp, pos)::typed_expressions) = (trexp exp) :: (checkExps (typed_expressions))
                    | checkExps [] = []

                  val typed_expressions = checkExps (exps)
                  val typ =
                    case typed_expressions
                     of [] => Ty.UNIT
                      | _ => (#ty (List.last(typed_expressions)))
               in
                  {exp = TAbs.SeqExp typed_expressions, ty = typ}
               end

           | trexp (A.WhileExp({test, body, pos})) = 
                let
                val typed_test as {exp = test_exp, ty = test_type} = trexp test 

                val _ = checkTypesEqual(test_type, Ty.INT, pos, "expected type to be int for the test");
                val _ = increaseLoopNest();
                val tbody as {exp = body_exp, ty = tybody} = transExp (venv, tenv, extra) body

                val _ = checkTypesEqual(tybody, Ty.UNIT, pos, "body type is not unit but type " ^ PT.asString(tybody));
                val _ = decreaseLoopNest();

                val twhiledata = {test = typed_test, body = tbody}
                in
                {exp = TAbs.WhileExp twhiledata, ty = tybody}
                end
          | trexp (A.ForExp {var, escape, lo, hi, body, pos}) =
              let
                 val venv' = S.enter (venv, var, E.VarEntry {ty = Ty.INT})
                 val tclo as {exp = e, ty = tylo}= trexp lo
                 val tchi as {exp = e, ty = tyhi} = trexp hi
                 val _ = checkTypesEqual(tylo, Ty.INT, pos, "error : lo expr is not int");
                 val _ = checkTypesEqual(tyhi, Ty.INT, pos, "error : hi expr is not int");

                 val _ = increaseLoopNest();
                 val tcbody as {exp = e, ty = tybody} = transExp (venv', tenv, extra) body
                 val _ = checkTypesEqual(tybody, Ty.UNIT, pos, "error : body of while not unit");

                 val _ = decreaseLoopNest();

                 val typ = tybody
                 val fordata = {var = var,
                                escape = escape,
                                lo = tclo,
                                hi = tchi,
                                body = tcbody}

                fun addToImmutables (var' : S.symbol) = 
                    immutables @ [var']

              in
                 (addToImmutables(var);
                 {exp = TAbs.ForExp fordata, ty = typ})
              end

            | trexp (A.BreakExp pos) = if !loop_nest_level > 0 then
                                           if !func_nest_level = 0 then
                                               {exp = TAbs.BreakExp, ty = Ty.UNIT}
                                           else
                                               (err pos ("Error:Break expression used inside nested function, func_nest_level: " ^ Int.toString(!func_nest_level));
                                                                         {exp = TAbs.BreakExp, ty = Ty.ERROR})
                                       else
                                           (err pos ("Error:Break expression used outside of loop");
                                                              {exp = TAbs.BreakExp, ty = Ty.ERROR})

          | trexp ( A.OpExp {left, oper, right, pos} ) =
            if oper = A.DivideOp orelse oper = A.TimesOp orelse oper = A.PlusOp orelse oper = A.MinusOp orelse oper = A.ExponentOp
            then
                let
                    val l = trexp left
                    val r = trexp right
                in  
                    (checkIntAndReturnExp(l, pos);
                     checkIntAndReturnExp(r, pos);
                    {exp= TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Types.INT})
                end
            else if oper = A.LeOp orelse oper = A.GtOp orelse oper = A.GeOp orelse oper = A.LtOp
            then
                let
                    val l = trexp left
                    val r = trexp right
                in
                    (case #ty l of
                        Ty.STRING =>
                        (checkString(l, pos);
                         checkString(r, pos);
                         {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT})
                      | Ty.INT =>
                        (checkIntAndReturnExp(l, pos);
                         checkIntAndReturnExp(r, pos);
                         {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT})
                      | _ => (err pos "cannot perform this comparison due to typing mismatch";
                              {exp = TAbs.ErrorExp, ty = Ty.ERROR})) 
                end
            else if oper = A.EqOp orelse oper = A.NeqOp
            then
                let
                    val l = trexp left
                    val r = trexp right
                in
                    (case actualTy (#ty l) pos of
                        Ty.STRING =>
                        (checkString(l, pos);
                         checkString(r, pos);
                         {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT})
                      | Ty.INT =>
                        (checkIntAndReturnExp(l, pos);
                         checkIntAndReturnExp(r, pos);
                         {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT})
                      | Ty.ARRAY(_, _) => 
                                (if checkAssignable(#ty l, actualTy (#ty r) pos, pos, "comparing array with invalid type")
                                then
                                    {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT} 
                                else
                                    (err pos "array types dont match";
                                    {exp = TAbs.ErrorExp, ty = Ty.ERROR})
                                )
                      | Ty.RECORD(_ , _) =>
                            (if checkAssignable(#ty l, #ty r, pos, "comparing record with invalid type")
                            then
                                {exp = TAbs.OpExp (createOpExp(l, oper, r, pos)), ty = Ty.INT}
                            else 
                                (err pos "record types dont match";
                                {exp = TAbs.ErrorExp, ty = Ty.ERROR}))
                      | _ => (err pos "cannot perform this comparison due to typing mismatch";
                              {exp = TAbs.ErrorExp, ty = Ty.ERROR}))
                end
            else
                (err pos "error in operation"; {exp = TAbs.ErrorExp, ty = Ty.ERROR})
            | trexp (A.IfExp{test, thn, els = SOME elseExp, pos}) =
                (let   
                    val test' = trexp test
                    val thn' = trexp thn
                    val elseExp' = trexp elseExp
                    val testType = #ty test' 
                    val thenType = #ty thn'
                    val elseType = #ty elseExp'
                in
                    (case testType of
                        Ty.INT => ()
                      | _ => (err pos "if-test is not an int"));
                    
                    if (checkAssignable(thenType, elseType, pos,
                        "then type is " ^ PT.asString(thenType) ^ " and else type is " ^ PT.asString(elseType)))
                    then
                        {exp = TAbs.IfExp {test = test', thn = thn', els = SOME elseExp' }, ty= thenType}
                    else
                        (err pos ("then type is " ^ PT.asString(thenType) ^ " and else type is " ^
                            PT.asString(elseType));
                        {exp = TAbs.ErrorExp, ty = Ty.ERROR})
                end)
          | trexp (A.IfExp{test, thn, els = NONE, pos}) =
                (let   
                    val test' = trexp test
                    val thn' = trexp thn
                    val testType = #ty test' 
                    val thenType = #ty thn'
                in
                    (case testType of
                        Ty.INT => ()
                      | _ => (err pos "if-test is not an int"));
                    if (thenType = Ty.UNIT)
                    then
                        {exp = TAbs.IfExp {test = test', thn = thn', els = NONE }, ty= thenType}
                    else
                        (err pos ("then type is not unit but type: " ^ PT.asString(thenType)); {exp = TAbs.ErrorExp, ty = Ty.ERROR})
                end) 
            | trexp (A.AssignExp {var, exp, pos}) = let
                                                     val typed_var as {var = v, ty = tyvar} = trvar var
                                                     val typed_exp as {exp = e, ty = tyexp} = trexp exp

                                                     val assignable = checkAssignable (tyvar, tyexp, pos, ("Type mismatch, trying to assign " ^ PT.asString tyvar ^ " to " ^ PT.asString tyexp))
                                                     val data = {var = typed_var, exp = typed_exp}
                                                     val typ = if assignable
                                                               then Ty.UNIT
                                                               else Ty.ERROR

                                                    fun isImmutable (item, lst : S.symbol list) = 
                                                        if null lst 
                                                        then false
                                                        else if hd lst = item then true
                                                        else isImmutable(item, tl lst)
                                                    
                                                        
                                                  in
                                                     case var
                                                      of A.SimpleVar (s, p) => 
                                                        if isImmutable(s, immutables) then
                                                        (err pos "attempt to assign the looping index in for-loop"; {exp = TAbs.ErrorExp, ty = Ty.ERROR})
                                                        else
                                                        {exp = TAbs.AssignExp(data), ty = typ}
                                                      | _ => {exp = TAbs.AssignExp(data), ty = typ}
                                                  end
             | trexp (A.LetExp {decls, body, pos}) = let
                                                     val tcdecs as {decls = d, tenv = t, venv = v} = transDecs (venv, tenv, decls, {})
                                                     val tcexp as {exp = e, ty = t}= transExp (v, t, {}) body
                                                     val letdata = {decls = d, body = tcexp}
                                                  in
                                                     {exp = TAbs.LetExp letdata, ty = t}
            end  

            | trexp (A.CallExp {func, args, pos}) = 
                (case S.look (venv, func) of
                    SOME (E.FunEntry {formals, result}) =>
                    (let
                        val args_list = List.map(fn (exp, pos) => (#ty (trexp exp))) args
                    in
                        if args_list = formals
                        then
                        {exp= TAbs.CallExp({func = func, args = List.map(fn (exp, pos) => (trexp exp)) args}),
                            ty=actualTy result  pos} 
                        else
                        (err pos "function arguments doesnt match signature formals"; {exp = TAbs.ErrorExp, ty = Ty.ERROR})
                    end)
                  | _ => (err pos ("function undeclared " ^ S.name(func)); {exp = TAbs.ErrorExp, ty = Ty.ERROR}))

             | trexp (A.ArrayExp{typ, size, init, pos}) =
                let
                    val size' = trexp size
                    val init' = trexp init
                    val typ' =  lookupActualTy tenv typ pos
                in
                    {exp = TAbs.ArrayExp {size = size', init = init'}, ty = typ'}
                end
             | trexp (A.RecordExp {fields, typ, pos}) =
              let
                 fun type_check_fields ((symbol, exp, pos) :: tl) = (symbol, trexp (exp)) :: (type_check_fields tl)
                   | type_check_fields ([]) = []

                 val typ' = lookupActualTy tenv typ pos
                 val fields' = type_check_fields (fields)

                 val ty = case typ'
                           of Ty.RECORD (list_fields, unique) => Ty.RECORD (list_fields, unique)
                            | _ => (errorTypesNotCompatibleRecord (pos, typ'); Ty.ERROR)
                 val temptyp = lookupTy tenv typ pos
              in
                 {exp = TAbs.RecordExp {fields = fields'}, ty = temptyp}
              end

         and trvar (A.SimpleVar (id, pos)) =
             let
                                               val t = S.look (venv, id)
                                               val typ = case t
                                                          of NONE => (Ty.UNIT)
                                                           | SOME (E.VarEntry {ty}) => actualTy ty pos
                                                           | SOME (E.FunEntry _) => Ty.ERROR
                                            in
                                              {var = TAbs.SimpleVar id , ty =typ}
                                           end 


          | trvar (A.FieldVar (var, id, pos)) =
              let
                 val tcvar as {var = v, ty = t} = trvar var
                 fun getType (sym, (s, ty)::liste) = if sym = s
                                                     then ty
                                                     else getType(sym, liste)
                   | getType (sym, []) = (errorFieldNotInRecord (pos, sym); Ty.ERROR)
                 val ty = actualTy t pos
                 val typ = case ty
                            of Ty.RECORD (liste, unique) => (getType (id, liste))
                             | _ => (errorTypesNotCompatibleRecord (pos, ty); Ty.ERROR)
              in
                 {var = TAbs.FieldVar (tcvar, id), ty = typ}
              end
              
            | trvar (A.SubscriptVar (var, exp, pos)) =
              let
                 val tcvar as {var = v, ty = tyvar} = trvar var
                 val tcexp as {exp = e, ty = tyexp} = trexp exp
                 val typ = case tyvar
                            of Ty.ARRAY (ty, unique) => actualTy ty pos
                             | _ => ( err pos ("Variable "^  PT.asString tyvar ^" is not an array."); Ty.ERROR)
              in
                 throw errorInt (pos, tyexp) ifFalse (isInt tyexp);
                 {var = TAbs.SubscriptVar (tcvar, tcexp), ty = tyexp}
              end
            and createOpExp (left, oper, right, pos) =
                case oper
                (* These operators take only ints *)
                of A.PlusOp => ({left = left, oper = TAbs.PlusOp, right = right})
                |  A.MinusOp => ({left = left, oper = TAbs.MinusOp, right = right})
                |  A.TimesOp => ({left = left, oper = TAbs.TimesOp, right = right})
                |  A.DivideOp => ({left = left, oper = TAbs.DivideOp, right = right})
                |  A.ExponentOp =>({left = left, oper = TAbs.ExponentOp, right = right})
                (* These operators take ints or strings, but only one of them *)
                |  A.LtOp => ({left = left, oper = TAbs.LtOp, right = right})
                |  A.LeOp => ({left = left, oper = TAbs.LeOp, right = right})
                |  A.GtOp => ({left = left, oper = TAbs.GtOp, right = right})
                |  A.GeOp => ({left = left, oper = TAbs.GeOp, right = right})
                (* These operators work on ints, strings, records or arrays *)
                |  A.EqOp => ({left = left, oper = TAbs.EqOp, right = right}) 
                |  A.NeqOp => ({left = left, oper = TAbs.NeqOp, right = right})
    in
        trexp
    end

and 
transDec ( venv, tenv
             , A.VarDec {name, escape, typ = NONE, init, pos}, extra : extra) =
     let
        val typed_exp as {exp = e, ty = t} = transExp (venv, tenv, extra) init
        val venv' = S.enter (venv, name, E.VarEntry {ty = t})
        val vardecldata =  { name = name
                               , escape = escape
                               , ty = t
                               , init = typed_exp}
        val errorExp = {exp = TAbs.ErrorExp, ty = Ty.ERROR}
        val errorData = { name = name
                               , escape = escape
                               , ty = Ty.ERROR
                               , init = errorExp}
     in 
        (case t
         of Ty.NIL => (throw errorNilType; {decl = TAbs.VarDec errorData, tenv = tenv, venv = venv'} )
          | _ => {decl = TAbs.VarDec vardecldata, tenv = tenv, venv = venv'})
     end
  

  | transDec ( venv, tenv, A.VarDec {name, escape, typ = SOME (s, pos), init, pos=pos1}, extra) =
     let
        val typed_exp as {exp = e, ty = t} = transExp (venv, tenv, extra) init
        val assign_type = lookupActualTy tenv s pos
        val a = checkAssignable (t, assign_type, pos, "Invalid assignment")
        val venv' = S.enter (venv, name, E.VarEntry {ty = assign_type})
        val vardecldata = { name = name
                               , escape = escape
                               , ty = assign_type
                               , init = typed_exp}
     in
        {decl = TAbs.VarDec vardecldata, tenv = tenv, venv = venv'}
     end


 | transDec (venv, tenv, A.TypeDec tydecls, extra) =
      let 

        fun actualTy' (Ty.NAME (s, ty)) pos iter limit =    
                let
                    val ty' = valOf (!ty)
                in  
                    if (iter > limit)
                    then
                        ((err pos "infinite loop detected"); Ty.ERROR)
                    else
                        actualTy' ty' pos (iter + 1) limit
                end
         |  actualTy' t _ _ _ = t 
        
        fun lookupActualTy' tenv sym pos = actualTy' (lookupTy tenv sym pos) pos 0 (S.numItems tenv)

         fun loadTypes (tenv', usedNames, {name, ty, pos}::tydecls', k) =
             let
                val rty = ref NONE
                val tenv'' = S.enter (tenv', name, Ty.NAME (name, rty))
                val _ = throw errorSameNameDecl (pos, name) ifTrue (List.exists (fn y => name = y) usedNames)
             in
                loadTypes (tenv'', name :: usedNames, tydecls',
                              fn (tenv, acc) => let
                                                   val t = transTy (tenv, ty)
                                                in
                                                   rty := SOME t;
                                                   k (tenv, (name, pos)::acc)
                                                end)
             end
          | loadTypes (tenv', usedNames, [], k) = k (tenv', [])
          fun buildTydeclData (tenv, (name, pos)::names, acc) =
               let
                  val t = lookupActualTy' tenv name pos
               in
                  buildTydeclData (tenv, names, {name = name, ty = t}::acc)
               end
            | buildTydeclData (tenv', [], acc) = List.rev acc
          val (tenv', typedecls) = loadTypes (tenv, [], tydecls, fn (tenv, acc) => (tenv, acc))
      in
         {decl = TAbs.TypeDec (buildTydeclData (tenv', typedecls, [])), tenv = tenv', venv = venv}
      end

  | transDec (venv, tenv, A.FunctionDec fundecls, extra) =
      let

         val _ = increaseFuncNest()
         fun loadParams (venv', {name, escape, ty}::params) =
             let
                val venv'' = S.enter (venv', name, E.VarEntry {ty = ty})
             in
                loadParams(venv'', params)
             end
           | loadParams (venv', nil) = venv'
         fun checkParams ({name, escape, typ = (s, pos), pos = _}::params) =
             let
                val ty = lookupActualTy tenv s pos
                val data = {name = name, escape = escape, ty = ty}
                val (datas, tys) = checkParams (params)
             in
                (data :: datas, ty :: tys)
             end
           | checkParams (nil) = ([], [])
         fun enterFunctions (venv', usedNames, {name, params, result, body, pos}::fundecls', k) =
             let
                val (cparams, tys) = checkParams(params)
                val returntype = case result
                                  of NONE => Ty.UNIT
                                   | SOME (s, p) => lookupActualTy tenv s p
                val venv'' = S.enter (venv', name, E.FunEntry {formals = tys, result = returntype})
                val _ = throw errorSameNameDecl (pos, name) ifTrue (List.exists (fn y => name = y) usedNames)
             in
                enterFunctions(venv'', name::usedNames, fundecls',
                                (fn (venv, acc) =>
                                       let
                                          val v = loadParams (venv, cparams)
                                          val b as {exp = e, ty = tyb} = (transExp (v, tenv, extra)) body
                                          val a = {name = name,
                                                   params = cparams,
                                                   resultTy = returntype,
                                                   body = b}
                                       in
                                          k (venv, a::acc)
                                       end))
             end
           | enterFunctions (venv', usedNames, [], k) = k (venv', [])
             val (venv', cfunc) = enterFunctions(venv, [], fundecls,
                                                  fn (venv, acc) => (venv, acc))
         val _ = decreaseFuncNest();
      in
         {decl = TAbs.FunctionDec cfunc, tenv = tenv, venv = venv'}
      end

and transDecs (venv, tenv, decls, extra : extra) =
    let fun visit venv tenv decls result =
            case decls
             of [] => {decls = result, venv = venv, tenv = tenv}
              | (d::ds) =>
                let
                    val { decl = decl
                        , venv = venv'
                        , tenv = tenv'} = transDec (venv, tenv, d, extra)
                in
                    visit venv' tenv' ds (result @ (decl :: []))
                end
    in
        visit venv tenv decls []
    end

fun transProg absyn =
    transExp (Env.baseVenv, Env.baseTenv, {}) absyn

end (* Semant *)
