(* Aarhus University, Compilation 2018  *)
(* DO NOT DISTRIBUTE                    *)
(* SKELETON FILE �                      *)


(* LLCodegen -- Tiger to LLVM-- code generator *)


signature LLCODEGEN =
sig
  val codegen_prog : { exp : OAbsyn.exp , locals: (int * Types.ty) list } -> ll.prog
  val codegen_ll  :  { exp : OAbsyn.exp , locals: (int * Types.ty) list } -> string
end


structure LLCodegen :> LLCODEGEN =
struct

structure A'' = OAbsyn
structure Ty = Types
structure S = Symbol
structure B = CfgBuilder
structure E = CgEnv


exception CodegenFatal (* should not happen *)

exception NotImplemented

type venv = E.enventry S.table
type tenv = (Ty.ty * ll.ty) S.table

type cg_ctxt = { venv : venv
               , tenv : tenv
               , locals  : (ll.tid * ll.ty) list  (* the locals *)
               , lid : ll.uid (* the llvm structure keeping the
                                           locals of the current function *)
                                     (* ... there is probably more *)
               }


(* tranlate Tiger type to LL type *)
fun ty_to_llty ( ty: Ty.ty) : ll.ty =
  case ty
   of Ty.NIL                    => ll.Ptr ll.I8
    | Ty.INT                    => ll.I64
    | Ty.STRING                 => ll.Ptr (ll.I8)
    | Ty.ARRAY _                => ll.Ptr (ll.I8)
    | Ty.RECORD _               => ll.Ptr (ll.I8)
    | Ty.NAME (_, ref (SOME t)) => ty_to_llty t
    | Ty.UNIT                   => ll.Void
    | _                         => raise CodegenFatal

(* -------------------------------------------------- *)
(* The result of code generation for expressions is an LL operand
   paired with a function from CFG builders to CFG builders *)


(* -- A brief introduction to Buildlets ------------- 


One of the challenges of generating LLVM code from a tree-like
representation such as Tiger ASTs is that while AST expressions are
tree-like, the LL code is linear. That means that the order in which
we translate different Tiger subexpressions starts to matter.

The "buildlet" approach separates the challenge of code generation for
a given a Tiger expression into two mostly orthogonal concerns:

Concern 1: What is the code for each of the subexpressions?

Concern 2: What is the order in which the code for the subexpressions
must be put together in order to produce the correct code for the
parent expression?

To implement this separation of concerns, we design the translation
function to be a function from a Tiger AST to *a CFG transformer*. CFG
transformers themselves are functions that take a CFG builder
(representing the state of the CFG at the time of putting the LL code
together) as an argument and return a CFG builder (the modified CFG).
The idea is that each subexpression transformer "knows" how to
generate the code for that subexpression, and can be invoked in a
black-box manner as long as we give it the state of the current
translation as an argument.

This approach allows us to translate the subexpressions (concern 1) in
a way that allows us to delay the decision of how to put them together
(concern 2). The benefit is that we can be compositional when working
with the transformers, as the code tends to be a lot less fragile.

----------------------------------------------------- *)


(* we call our CFG transformers buildlets *)
                          
type buildlet = B.cfg_builder -> B.cfg_builder

(* the operand expresses where to find the result assuming that
   the buildlet is invoked *)
type cg_res = (ll.operand * buildlet)


(* some auxiliary functions *)

fun emit_insn insn =
  fn b => B.add_insn b insn

fun emit_term tr =
  fn b => B.term_block b tr

fun emit_lbl lbl =
  fn b => B.set_label b lbl

fun emit_alloca t =
  fn b => B.add_alloca b t

val id_buildlet = fn b => b

fun seq_buildlets (bls: buildlet list) :buildlet =
  fn cfgb_init =>
     List.foldl
         (fn (b, cfgb') => b cfgb') (* apply the next buildlet to the threading CFGBuilder *)
         cfgb_init
         bls


fun build_cfg buildlet = B.get_cfg (buildlet (B.new_env()))

val build_cfg_from_buildlets = (build_cfg o seq_buildlets)

fun mapseq f ls = seq_buildlets (map f ls)



fun i64toInt (ll.Const i) = i
(* should not happen *)
| i64toInt _ = 0

fun emit_uid' (a, b) = emit_insn (SOME a, b)
fun build_store args = emit_insn (NONE, ll.Store args)





(* -------------------------------------------------- *)


val mk_uid = LLEnv.mk_uid
val add_named_type = LLEnv.add_named_type
val mk_label_name  = LLEnv.mk_label_name
val mk_type_name  = LLEnv.mk_type_name


(* for custom instructions *)
fun emit_mk ll_env (s, i) =
  let val uid = mk_uid ll_env s
  in (uid, emit_insn (SOME uid, i))
  end

fun emit_mk' ll_env (s, i) =
  let val uid = mk_uid ll_env s
  in (uid, emit_uid' (uid,i))
  end

fun emit_mk'' ll_env (s, i) =
  let val uid = mk_uid ll_env s
  in (ll.Id uid, emit_uid' (uid,i))
  end

fun build_load_op ll_env s args =
  let val uid = mk_uid ll_env s
  in (ll.Id uid, emit_uid' (uid, ll.Load args))
  end

(* -------------------------------------------------- *)

fun getTypeSize ll_env (ty) =
let
  val (gep_res, b_gep) = emit_mk ll_env ("pointer_size"
    , ll.Gep(ty, ll.Null, [ ll.Const(1) ]))
  val (size_res, build_size) = emit_mk' ll_env ("integer_of_pointer"
    , ll.Ptrtoint (ty, ll.Id gep_res, ll.I64))
  val b_ = seq_buildlets [b_gep, build_size]
in
(size_res, b_)
end


fun getSubscriptType (ty) =
  case ty
    of Ty.ARRAY(returnTy, _) => returnTy
    | ty => ty

fun getFieldType (ty, s) =
  case ty
    of Ty.ARRAY(returnTy, _) => getFieldType (returnTy, s)
    | Ty.RECORD(fields, _) =>
      let
        val i = ref 0
        val fTy = List.find (fn (x, _) => (i := !i + 1; x = s)) fields
      in
        (fTy, !i)
      end
      | _ => (NONE, 0)

fun RecordBitcastAction cg_ctxt ll_env (var_ptr, ll.Ptr(ll.I8), unique) =
let
  val {venv = venv, tenv = tenv, locals = locals, lid = lid, level = ctxLvl, tyMappings = tm, break_link = _} = cg_ctxt
  val SOME(_, t) = List.find (fn (Types.RECORD(_, u), _) => u = unique) tm
  val (newPtr, b_bitcast) = emit_mk ll_env
          ("recordDoBitcast", ll.Bitcast (ll.Ptr(ll.I8), var_ptr, ll.Ptr(t)))
in
  SOME(ll.Id newPtr, b_bitcast, ll.Ptr(t))
end


fun bitcastRecordIfNecessary cg_ctxt ll_env (var_ptr, ty, Types.RECORD(_, unique)) =
  RecordBitcastAction cg_ctxt ll_env (var_ptr, ty, unique)
| bitcastRecordIfNecessary cg_ctxt ll_env (var_ptr, ty, Types.ARRAY(Ty.RECORD(_, unique), _)) =
  RecordBitcastAction cg_ctxt ll_env (var_ptr, ty, unique)
|  bitcastRecordIfNecessary cg_ctxt  ll_env  (var_ptr, val_ty, _) = NONE

fun bitcastOneOfTwoIfNecessary cg_ctxt ll_env (var_ptr, ty, ty1, ty2) =
let
  val bitcast1 = bitcastRecordIfNecessary cg_ctxt ll_env (var_ptr, ty, ty1)
  val bitcast2 = bitcastRecordIfNecessary cg_ctxt ll_env (var_ptr, ty, ty2)
in
  if Option.isSome(bitcast1) then
    Option.valOf(bitcast1)
  else if Option.isSome(bitcast2) then
    Option.valOf(bitcast2)
  else
   (var_ptr, id_buildlet, ty)
end


fun findOneOfTwoFieldTy (s, ty1, ty2) =
  let
    val (tyOp1, offset1) = getFieldType(ty1, s)
    val (tyOp2, offset2) = getFieldType(ty2, s)
  in
    if Option.isSome(tyOp1) then
      ((#2 (Option.valOf(tyOp1))), offset1)
    else if Option.isSome(tyOp2) then
      ((#2 (Option.valOf(tyOp2))), offset2)
    else
      (print("ERROR: Could not find a valid type option"); raise CodegenFatal)
  end

fun findLocals ll_env ([], _, _, _) = raise CodegenFatal (* You can't search for a local when having no locals *)
 | findLocals ll_env ([loc], (lid, b_finds), ctxtLvl, varLvl) =
    ((lid, b_finds), loc)
 | findLocals ll_env (loc::nLoc::locals, (lid, b_finds), ctxtLvl, varLvl) =
  if ctxtLvl = varLvl then
    ((lid, b_finds), loc)
  else
    let
      val (localsTyName, _) = loc
      val (gep_res, b_gep) = emit_mk ll_env ("getpointer"
          , ll.Gep(ll.Namedt(localsTyName), lid, [ ll.Const(0), ll.Const(0) ]))
      val (load_res, b_load) = build_load_op ll_env "loadVarPtr" (ll.Ptr(ll.I8), ll.Id gep_res)

      val (nextLocalsName, _) = nLoc
      val namedLocalsTy = ll.Namedt(nextLocalsName)
      val (bitcast_locals, b_bitcast) = emit_mk ll_env ("bitcastLocals"
          , ll.Bitcast(ll.Ptr(ll.I8), load_res, ll.Ptr(namedLocalsTy)))

      val b_ = seq_buildlets [b_gep, b_load, b_bitcast]
    in
      findLocals ll_env (nLoc::locals, (ll.Id bitcast_locals, b_finds@[b_]), ctxtLvl - 1, varLvl)
    end

fun findAndBitcastLocals ll_env  bitcastTy (locals, (lid, b_finds), ctxtLvl, varLvl) =
let
(*get locals*)
    val ((true_lid, b_finds), (localsTyName, types_list)) =
      findLocals ll_env (locals, (lid, b_finds), ctxtLvl, varLvl) 
    val namedLocalsTy = ll.Namedt(localsTyName)
      (*bitcast locals*)
    val (bitcast_locals, b_bitcast) = emit_mk ll_env ("bitcastLocalsToType"
        , ll.Bitcast(ll.Ptr(namedLocalsTy), true_lid, bitcastTy))
    val bs_w_bitcast = b_finds@[b_bitcast]
in
  ((ll.Id bitcast_locals, bs_w_bitcast), (localsTyName, types_list))
end

fun addfundecltoenv ll_env ({name = name'
                  , args = args'
                  , aug = { ty = resultTy, locals = fun_locals}
                  , body = body'}, cg_ctxt) =
let
  val {venv = venv, tenv = tenv, locals = locals, lid = lid, level = level, tyMappings = tm, break_link = llb} = cg_ctxt
  (* get uid for fun decl *)
  val uid = LLEnv.mk_uid ll_env (S.name(name'))
  (* extract types from args *)
  val argTypes = (map (fn {name = _, escape = _, aug = { ty = ty, offset = _ }} => ty) args')

  (* get info from locals *)
  val (outerLocals, _)::_ = locals
(* get the type of the locals *)
  val outerLocalsTy = ll.Ptr(ll.Namedt(outerLocals))

(* add function to the env *)
  val venv' = S.enter(venv, name', E.FunEntry({
              formals = argTypes
              , result = resultTy
              , gid = uid
              , sl_type = outerLocalsTy
              , level =  level}))
       (* update context with the new env *)
  val cg_ctxt' = {venv = venv'
        , tenv = tenv
        , locals = locals
        , lid = lid
        , level = level
        , tyMappings = tm
       ,break_link = llb}
(* return the new env *)
  in cg_ctxt'
  end

fun emitArgAlloc ll_env locals_name lid (arg_ty, arg_name, offset) =
let
  val (argPtr, b_gep_arg) = emit_mk ll_env ("gepArg"
    , ll.Gep(ll.Namedt(locals_name), ll.Id(lid), [ll.Const 0, ll.Const offset]))
  val b_store_arg = build_store (arg_ty, ll.Id(arg_name), ll.Id(argPtr) )
  val b_ = seq_buildlets [b_gep_arg, b_store_arg]
in
  (argPtr, b_)
end


fun pushFieldToEnv level ({ name   = name
                            , escape = escape
                            , aug   = { ty = ty, offset = offset }}, venv) =
  S.enter (venv, name, E.VarEntry{ ty = ty, escape = escape, level = level, offset = offset})

fun getVarPointer ll_env cg_ctxt name =
  let
    val {venv = venv, tenv = tenv, locals = locals, lid = lid, level = level, tyMappings = _, break_link = _} = cg_ctxt
    (* get variable offset *)
    val SOME(E.VarEntry({ ty = ty
            , escape = escape
            , level  = varLvl
            , offset = offset})) = S.look(venv, name)

(* look for the variable in the locals collection of the llvm *)
    val ((true_lid, b_finds), (localsTyName, ll.Struct(types_list))) =
        findLocals ll_env (locals, (ll.Id lid, []), level, varLvl) (*(uId, ll.Struct types_list)*)

(* use the gep to find the variable pointer *)
    val (gep_res, b_gep) = emit_mk ll_env ((S.name name)
          , ll.Gep(ll.Namedt localsTyName, true_lid, [ ll.Const(0), ll.Const(offset + 1) ]))
    val val_type = List.nth(types_list, offset + 1)
    val b_ = seq_buildlets(b_finds@[ b_gep])
  in
    (gep_res, b_, val_type)
  end


fun emitRecordStructure ll_env ({name = name', ty = Types.RECORD(types, unique)}, cg_ctxt) =
  let
    val paramTypes = (map ty_to_llty (map (fn (_, ty) => ty) types))
    val tyName = LLEnv.mk_type_name ll_env (Symbol.name name')
    val {venv = venv, tenv = tenv, locals = locals, lid = lid, level = level, tyMappings = tm, break_link = lLB} = cg_ctxt
    val tenv' = S.enter (tenv, tyName, (Types.RECORD(types, unique), ll.Namedt tyName))
    val tm' = (Types.RECORD(types, unique), ll.Namedt(tyName))::tm
    val cg_ctxt' = {venv = venv, tenv = tenv', locals = locals, lid = lid, level = level , tyMappings = tm', break_link = lLB}
  in
    (* emit the type declaration *)
    (LLEnv.add_named_type ll_env (tyName, ll.Struct paramTypes); cg_ctxt')
  end
| emitRecordStructure ll_env (_, cg_ctxt) = cg_ctxt


fun cgExp ll_env cg_ctxt (aug_exp:A''.exp) =
  let val exp = #exp aug_exp
      val ty = (#ty o #aug) aug_exp
      val emit_mk'  = emit_mk' ll_env
      val emit_mk'' = emit_mk'' ll_env
      val build_load_op = build_load_op ll_env
      val (cgE : (A''.exp -> cg_res)) = cgExp ll_env cg_ctxt
      
  in case exp
      of A''.NilExp   => (ll.Null, id_buildlet) (* returned operand should be ll.Null      *)
       | A''.IntExp i => (ll.Const i, id_buildlet) (* returned operand should be (ll.Const i) *)
       | A''.OpExp {left, oper, right} =>
            let 
                val cmpop = 
                 case oper
                  of A''.EqOp => SOME ll.Eq
                   | A''.NeqOp => SOME ll.Ne
                   | A''.LtOp => SOME ll.Slt
                   | A''.LeOp => SOME ll.Sle
                   | A''.GtOp => SOME ll.Sgt
                   | A''.GeOp => SOME ll.Sge
                   | _ => NONE 

                val binop =
                 case oper
                  of A''.PlusOp => SOME ll.Add 
                   | A''.MinusOp => SOME ll.Sub
                   | A''.TimesOp => SOME ll.Mul
                   | A''.DivideOp => SOME ll.SDiv
                   | _ => NONE

                val expop =
                 case oper
                  of A''.ExponentOp => SOME () 
                   | _ => NONE 

                val uid = mk_uid ll_env "binop_result"
                val llty = ty_to_llty ty

                val (l_oprnd, l_b) = cgE left
                val (r_oprnd, r_b) = cgE right

                val (op', build_op) =
                    case cmpop
                     of SOME i =>
                        let
                          val (cmpop_bool, boolb) = emit_mk' ("cmpop_bool", ll.Icmp (i, ll.I64, l_oprnd, r_oprnd))
                          val (cmpop_int, intb) = emit_mk' ("cmpop_i", ll.Zext(ll.I1, ll.Id cmpop_bool ,ll.I64))
                          val bop = seq_buildlets [boolb, intb]
                        in 
                          (cmpop_int, bop)

                        end
                      | NONE =>
                        case binop
                         of SOME i => emit_mk' ("binop",
                                      ll.Binop (i, llty, l_oprnd, r_oprnd))
                          | NONE => 
                           case expop
                            of SOME _ => emit_mk' ("expop",
                                      ll.Call (ll.I64, ll.Gid (S.symbol "exponent"), [(ll.I64, l_oprnd), (ll.I64, r_oprnd)]))
                             | NONE => raise NotImplemented

                val b_ =
                    seq_buildlets [
                        l_b,
                        r_b, 
                        build_op]
            in
                (ll.Id op', b_)
            end
       | A''.SeqExp exps               =>
            (case exps
             of [] => (ll.Null, id_buildlet)
              | _ => (
            let
                val exps' = List.map cgE exps
                val buildlets' = List.map #2 exps'
                
                val lastValue = #1 (List.last  exps')
            
                val b_ = seq_buildlets (buildlets')
            in  
                (lastValue, b_)
            end))

       | A''.LetExp {decls, body} =>
           let
                fun foldDecls(x, (ctxt, bs)) =
                    let
                        val (ctxt', b') = cgDecl ll_env ctxt x
                        val bs' = bs@[b']
                    in
                        (ctxt', bs')
                    end

                val (cg_ctxt', b_decls) = foldl foldDecls (cg_ctxt, []) decls
                val (res, b_body) = cgExp ll_env cg_ctxt' (body)

                val sq_b_decls = seq_buildlets b_decls
                val b_ = seq_buildlets [sq_b_decls, b_body]
             in
               (res, b_)
             end
            

       | A''.VarExp var =>  cgVar ll_env cg_ctxt var (*delegate to a dedicated function *)


       | A''.AssignExp {var, exp}                    => cgAssignExp( var, exp, ll_env, cg_ctxt) 
       | A''.IfExp {test, thn, els as (SOME els') }  =>
         let val then_lbl = mk_label_name ll_env "then"
             val else_lbl = mk_label_name ll_env "else"
             val merg_lbl = mk_label_name ll_env "merge"

             val (test_oprnd, test_b) = cgE test
             val (then_oprnd, then_b) = cgE thn
             val (else_oprnd, else_b) = cgE els'

             (* branch on test_oprnd, and depending on the branch,
                jump to either then_lbl or else_lbl *)

             val (test_op', build_icmp) =
		      emit_mk'  ("test"
			   , ll.Icmp ( ll.Ne
				     , ll.I64
				     , test_oprnd
				     , ll.Const 0))

             val b_cbr =
                 emit_term (ll.Cbr ( ll.Id test_op'
				   , then_lbl
				   , else_lbl ))

             val uid = mk_uid ll_env "ifthenelse_result"
             val llty = ty_to_llty ty
             val b_res_alloca = emit_alloca (uid, llty)
             fun b_store_r r = build_store (llty, r , ll.Id uid)

	     val (res_val, b_load) =
    		 build_load_op "res" (llty, ll.Id uid)

	     val b_br_merge = emit_term (ll.Br merg_lbl)

             val b_ = seq_buildlets [
                     b_res_alloca

                   , test_b
                   , build_icmp
                   , b_cbr

                   , emit_lbl then_lbl
                   , then_b
                   , b_store_r then_oprnd
                   , b_br_merge

                   , emit_lbl else_lbl
                   , else_b
                   , b_store_r else_oprnd
                   , b_br_merge

                   , emit_lbl merg_lbl
                   , b_load ]
         in
             (res_val, b_) (* *)
         end
       | A''.CallExp {func, args}                    => codegenCallExp ll_env cg_ctxt (func, args)
       | A''.RecordExp {fields}                      => cgRecExp ll_env cg_ctxt fields ty
       | A''.IfExp {test, thn, els as NONE }         =>
          let val then_lbl = mk_label_name ll_env "then"
              val merg_lbl = mk_label_name ll_env "merge"

              val (test_oprnd, test_b) = cgE test
              val (then_oprnd, then_b) = cgE thn

              (* branch on test_oprnd, and depending on the branch,
                  jump to either then_lbl or merge_lbl *)

              val (test_op', build_icmp) = emit_mk' ("test", ll.Icmp ( ll.Ne, ll.I64, test_oprnd, ll.Const 0))

              val b_cbr = emit_term (ll.Cbr ( ll.Id test_op', then_lbl, merg_lbl ))

              val b_br_merge = emit_term (ll.Br merg_lbl)

               val b_ = seq_buildlets [ test_b, build_icmp, b_cbr,
                                 emit_lbl then_lbl, then_b,
                                 b_br_merge, emit_lbl merg_lbl ]
          in
              (ll.Null, b_) (* *)
          end
       | A''.WhileExp { test, body}                  =>
          let
              val test_lbl = mk_label_name ll_env "test"
              val body_lbl = mk_label_name ll_env "body"
              val merg_lbl = mk_label_name ll_env "merge"

              val cg_ctxt' = { venv = #venv cg_ctxt, tenv = #tenv cg_ctxt, locals = #locals cg_ctxt,
                               lid = #lid cg_ctxt, level = #level cg_ctxt, tyMappings = #tyMappings cg_ctxt,
                               break_link = merg_lbl}

              val (test_oprnd, test_b) = cgE test
              val (body_oprnd, body_b) = cgExp ll_env cg_ctxt' body 

              (* branch on test_oprnd, and depending on the branch,
                jump to either body_lbl or merge_lbl *)

              val (test_op', build_icmp) = emit_mk' ("testLoop", ll.Icmp ( ll.Ne, ll.I64, test_oprnd, ll.Const 0))

              val b_cbr = emit_term (ll.Cbr ( ll.Id test_op', body_lbl, merg_lbl ))
              val b_br_test = emit_term (ll.Br test_lbl)

              val b_ = seq_buildlets [ b_br_test, emit_lbl test_lbl, test_b, build_icmp, b_cbr,
                                emit_lbl body_lbl, body_b,
                                b_br_test, emit_lbl merg_lbl ]
          in
              (ll.Null, b_) (* *)
          end
       | A''.BreakExp                                => 
          let
            val merg_lbl = #break_link cg_ctxt
            val b_br_merg = emit_term (ll.Br merg_lbl)
            val extra_merg = emit_lbl (mk_label_name ll_env "extra")
            val b_ = seq_buildlets [b_br_merg, extra_merg]
          in
            (ll.Null, b_)
          end
       | A''.ForExp {var, escape, lo, hi, body, aug} => 
          let 
              val test_lbl = mk_label_name ll_env "test"
              val body_lbl = mk_label_name ll_env "body"
              val merg_lbl = mk_label_name ll_env "merge"

              val (t_cg_ctxt, b_ctxt) = cgVarDecls ll_env cg_ctxt
                        { name = var, escape = escape, aug = aug, init = lo }

              (* create the new context that now has lo declared and also knows where the latest break point is *)
              val cg_ctxt' = {venv = (#venv t_cg_ctxt),
                              tenv = (#tenv t_cg_ctxt),
                              locals = (#locals t_cg_ctxt),
                              lid = (#lid t_cg_ctxt),
                              level = (#level t_cg_ctxt),
                              tyMappings = (#tyMappings t_cg_ctxt),
                              break_link = merg_lbl}


              val (it_uid, b_gep, _) = getVarPointer ll_env cg_ctxt' var
              

              val hi_uid = mk_uid ll_env "hi"
              val b_hi_alloca = emit_alloca (hi_uid, ll.I64)

              (* have to make a b_store r that takes registers since we need to store several values in different regs *)
              fun b_store_r r reg = build_store (ll.I64, r , reg)
              val (hi_oprnd, hi_b) = cgExp ll_env cg_ctxt hi

              (* load iterator and increment *)
              val (it, b_load_var) = build_load_op "it" (ll.I64, ll.Id it_uid)
              val (inc, b_add) = emit_mk' ("inc",
                                  (ll.Binop (ll.Add, ll.I64, it, ll.Const 1)))

              (* checks if our counter, it, has reached the limit, hi *)
              val (it_comp, b_it_comp) = build_load_op "it_comp" (ll.I64, ll.Id it_uid)
              val (hi_comp, b_hi_comp) = build_load_op "hi_comp" (ll.I64, ll.Id hi_uid)
              val (test_op', build_icmp) = emit_mk' ("test",
                                            ll.Icmp ( ll.Sle, ll.I64, it_comp, hi_comp))

              val b_br_test = emit_term (ll.Br test_lbl)
              val b_cbr =  emit_term (ll.Cbr ( ll.Id test_op', body_lbl, merg_lbl ))


              val (body_oprnd, build_body) = cgExp ll_env cg_ctxt' body
              val b_ = seq_buildlets [ b_ctxt, b_gep, b_hi_alloca, hi_b, b_store_r hi_oprnd (ll.Id hi_uid),
                                       b_br_test, emit_lbl test_lbl, b_it_comp, b_hi_comp, build_icmp, b_cbr,
                                       emit_lbl body_lbl, build_body,
                                       b_load_var, b_add, b_store_r (ll.Id inc) (ll.Id it_uid), b_br_test,
                                       emit_lbl merg_lbl ]
          in
              (ll.Null, b_) (* *)
          end
       | A''.ArrayExp {size, init}                   => 
       let
(* expose init for type  *)
  val {exp = initExp, aug = initAug} = init
  val init_ty = (#ty initAug)
  val llinit_ty = ty_to_llty init_ty
  (* generate code for size and initializzation*)
  val (size_res, build_size) = cgExp ll_env cg_ctxt size
  val (init_res, build_init) = cgExp ll_env cg_ctxt init
  (* find the array type and allocate space *)
  val array_type = ll.Array (i64toInt size_res, ty_to_llty init_ty)
  val initUid = mk_uid ll_env "ptr_alloc"
  val (b_alloc) = emit_alloca (initUid, llinit_ty)
  val b_store_init = build_store (llinit_ty, init_res, ll.Id initUid)

  val (res, b_cast_ptr) = emit_mk ll_env ("resPtr"
        , ll.Bitcast(ll.Ptr(llinit_ty), ll.Id initUid, ll.Ptr(ll.I8)))

(* get size for element*)
  val getTypeSizeTuple = getTypeSize ll_env (llinit_ty)
  val (call_res, b_call) = emit_mk ll_env ("call"
    , ll.Call(ll.Ptr(ll.I8), ll.Gid (S.symbol "initArray"), [
                  (ll.I64, size_res)
                  , (ll.I64, ll.Id (#1 getTypeSizeTuple))
                  , (ll.Ptr(ll.I8), ll.Id res)]))

  val b_ = seq_buildlets([
                build_size
              , build_init
              , b_alloc
              , b_store_init
              , b_cast_ptr
              , (#2 getTypeSizeTuple)
              , b_call  ])
in
  (ll.Id call_res, b_)
end
       | A''.StringExp s                             => 
         let
         val name = mk_uid ll_env "string"
          val size = String.size(s)
          val typ = ll.Array (size,ll.I8)
          val recTyp = ll.Struct([ll.I64, typ])
          val global = LLEnv.add_gid_decl ll_env (name, (recTyp
              , ll.GStruct [(ll.I64, ll.GInt size), (typ, ll.GString s)]))

          val (res_bitcast, build_bitcast) = emit_mk ll_env
          ("stringBitcast", ll.Bitcast (ll.Ptr(recTyp), ll.Gid name, ll.Ptr(ll.I8)))

          in
          (ll.Id res_bitcast, build_bitcast)
          end
            | A''.ErrorExp => raise CodegenFatal
        end


and cgDecl ll_env cg_ctxt aug_decl =
    case aug_decl
     of A''.VarDec vardec => cgVarDecls ll_env cg_ctxt vardec
      | A''.FunctionDec fns => cgFunDecls ll_env cg_ctxt fns
      | A''.TypeDec tds => codegenTypeDecls ll_env cg_ctxt tds


and cgRecExp ll_env cg_ctxt fields (Ty.RECORD(_, unique)) = 
let
  val {venv = venv, tenv = tenv, locals = locals, lid = lid, level = ctxLvl, tyMappings = tm, break_link = _} = cg_ctxt
  val SOME(_, recordTy) = List.find (fn (Types.RECORD(_, u), _) => u = unique) tm
  (* allocate record *)
  val recordUid = mk_uid ll_env "record"

  val getTypeSizeTuple = getTypeSize ll_env (recordTy)
  val (call_res,b_call) = emit_mk ll_env ("call"
          , ll.Call(ll.Ptr(ll.I8), ll.Gid(S.symbol "allocRecord"), [
           (ll.I64, ll.Id (#1 getTypeSizeTuple))
          ]))

  val (res_bitcast, b_cast_ptr) = emit_mk ll_env ("resPtr"
          , ll.Bitcast(ll.Ptr(ll.I8), ll.Id call_res, ll.Ptr(recordTy)))


  (* now evaluate expressions and store them in record*)
  val fieldExps : OAbsyn.exp list = map (fn (field) => (#2 field)) fields
  val fieldAugs = map (fn (exp) => (#aug exp)) fieldExps
  val fieldTys = map (fn (aug) => (#ty aug)) fieldAugs
  val recordLLTys = map ty_to_llty fieldTys


  val codegenExps = map (fn (exp) => cgExp ll_env cg_ctxt exp) fieldExps
  val codegenExps_res = map (fn (e) => (#1 e)) codegenExps
  val b_codegenExps = map (fn (e) => (#2 e)) codegenExps
  (* geping and storing *)
  val gepStoreListFieldList = gepAndStore(ll_env, cg_ctxt, recordTy, res_bitcast, codegenExps_res, recordLLTys)
  val gepFieldList = map (fn (f) => (#1 f)) gepStoreListFieldList
  val gepStoreList = map (fn (f) => (#2 f)) gepStoreListFieldList

(*
build_size
, build_init
, b_alloc
, b_store_init
, b_cast_ptr
, (#2 getTypeSizeTuple)
, b_call  ])
*)
  val b_ = seq_buildlets(
                [(#2 getTypeSizeTuple)]
               @ [b_call]
               @ [b_cast_ptr]
               @ b_codegenExps
               @ gepFieldList
               @ gepStoreList
               )
in
  (ll.Id call_res, b_)
end

(* cgVarLoc returns the location and a type for the variable to be
   used for subsequent load /saves --------------------------------------- *)
   

and gepAndStore(ll_env, cg_ctxt, recordTy, recordUid, expList, tyList) =
let
  fun gepStore(i, (x::xs), (t::ty)) =
  let
    val (res_gep,b_gep) = emit_mk ll_env ("gep", ll.Gep(recordTy, ll.Id recordUid, [ll.Const 0, ll.Const i]))
    val b_store = build_store(t, x, ll.Id res_gep)
    val returnList = (b_gep, b_store)
  in
    returnList :: gepStore(i+1, xs, ty)
  end
  | gepStore(i, [], []) = []
  | gepStore _ = []
  val gepStoreList = gepStore(0, expList, tyList)
in
  gepStoreList
end

 and getFieldVarPointer ll_env cg_ctxt (var, s, baseTy) =
  let
   
    val (fieldType, fieldOffset) = findOneOfTwoFieldTy(s, baseTy, ((#ty o #aug) var))
    val (rec_uid, b_rec) = cgVar ll_env cg_ctxt var
    val (bitC_Id, b_bitcast, ll.Ptr(actualTy)) = bitcastOneOfTwoIfNecessary cg_ctxt ll_env
        (rec_uid, ll.Ptr(ll.I8), baseTy, ((#ty o #aug) var))
    val (ptr_res, b_ptr) = emit_mk ll_env ("fieldVarPointer",
      ll.Gep(actualTy, bitC_Id, [ll.Const(0), ll.Const(fieldOffset - 1)] ))
    val b_ = seq_buildlets ([b_rec, b_bitcast, b_ptr])
  in
    (ptr_res, b_, ty_to_llty fieldType)
  end

 and getPointerForSubscript ll_env cg_ctxt (var, exp, baseTy) =
  let
    val (res_index, b_index) = cgExp ll_env cg_ctxt exp
    val (arr_uid, b_arr, arr_type) = cgVarLoc ll_env cg_ctxt var
    val (returnTy) = getSubscriptType(baseTy)
    val llRetTy = ty_to_llty returnTy
    val (res_load, load_b) = build_load_op ll_env "subscript" (arr_type, ll.Id arr_uid)

    val (res_bitcast, build_bitcast) = emit_mk ll_env
            ("subscriptVarBitcast", ll.Bitcast (arr_type, res_load, ll.Ptr(llRetTy)))
    val (ptr_res, b_ptr) = emit_mk ll_env ("gep", ll.Gep(llRetTy, ll.Id res_bitcast
                        , [res_index]))
    val b_ = seq_buildlets ([b_index, b_arr, load_b, build_bitcast, b_ptr])
  in
    (ptr_res, b_, llRetTy)
  end   
and cgVarLoc ll_env cg_ctxt (var_aug:A''.var) =
    let val var = #var var_aug
                val baseTy = (#ty o #aug) var_aug

    in case var
	of A''.SimpleVar s => getVarPointer ll_env cg_ctxt s
	 | A''.FieldVar (var, s) => getFieldVarPointer  ll_env cg_ctxt (var,s, baseTy)
	 | A''.SubscriptVar (var, exp) => (print "Hit a TODO at subscriptvar \n"; raise NotImplemented)
    end

and cgVar ll_env  cg_ctxt var_aug =
    let
    (* get variable location type and pointer *)
      val (var_ptr, b_get_loc, val_ty) = cgVarLoc ll_env cg_ctxt var_aug
      (* load the variable into ll *)
      val (loaded_var, b_load) = build_load_op ll_env "loadVar" (val_ty, ll.Id var_ptr)
      (* construct buildlet with the new loaded variable *)
      val b_ = seq_buildlets [b_get_loc, b_load]
    in
      (loaded_var, b_)
    end

and cgAssignExp (var, exp, ll_env, cg_ctxt) =
  let
    val (varPtr, b_gep, val_ty) = cgVarLoc ll_env cg_ctxt var
    val (exp_value, b_exp_value) = cgExp ll_env cg_ctxt exp
    val b_store_value = build_store (val_ty, exp_value, ll.Id varPtr)
    val b_ = seq_buildlets[ b_gep, b_exp_value, b_store_value ]
  in
    (ll.Id varPtr, b_)
  end

and cgVarDecls ll_env cg_ctxt { name, escape, aug as {ty, offset}, init } =
let
(* expose the context variables *)
  val {venv = venv, tenv = tenv, locals = locals, lid = lid, level = level, tyMappings = tm, break_link = llb:S.symbol} = cg_ctxt
  (* add the variable to the venv variable  *)
  val venv' = S.enter (venv, name, E.VarEntry{ ty = ty, escape = escape, level = level, offset = offset})
  (* add the new venv variable to the context  *)
  val cg_ctxt' = {venv = venv', tenv = tenv, locals = locals, lid = lid, level = level , tyMappings = tm, break_link = llb:S.symbol}

(* evaluate and get the value of the var decl expression *)
  val (init_value, b_init_value) = cgExp ll_env cg_ctxt init
  (* get the pointer for the new variable name *)
  val (varPtr, b_gep, val_ty) = getVarPointer ll_env cg_ctxt' name
  (* store the pointer value for the variable in the buildlet *)
  val b_store_value = build_store (val_ty, init_value, ll.Id varPtr)
  (* add the new stored value to the current buildlet *)
  val b_ = seq_buildlets[ b_init_value, b_gep, b_store_value ]
in
  (cg_ctxt', b_)
end

and codegenCallExp ll_env cg_ctxt (func, args) =
  let
    val cGedArgs = map (cgExp ll_env cg_ctxt) args
    val argNames = map (fn (operand, _) => operand) cGedArgs
    val argBuildlets = map (fn (_, buildlet) => buildlet) cGedArgs

    val {venv = venv, tenv = tenv, locals = locals, lid = lid, level = level, tyMappings = _, break_link = _} = cg_ctxt
    val SOME(E.FunEntry({ formals = argTypes
              , result = tempFuncTy
              , gid    = funcId
              , sl_type = _
              , level = funLevel })) = S.look(venv, func)
    val function_type = ty_to_llty tempFuncTy

    val ((funOuterLocals, b_find_locals), (funOuterLocalsTy, _)) =
        findAndBitcastLocals ll_env (ll.Ptr(ll.I8)) (locals, (ll.Id lid, []), level, funLevel)
    val argPairs = ListPair.zip((map ty_to_llty argTypes), argNames)

    val pairs = (ll.Ptr(ll.I8), funOuterLocals)::argPairs
    val b_void = emit_insn (NONE, ll.Call(function_type, ll.Gid funcId, pairs))

    val (call_res, b_call) = emit_mk ll_env ("call"
      , ll.Call(function_type, ll.Gid funcId, pairs))

    val b_ = seq_buildlets (b_find_locals@argBuildlets@[ b_call])
    val b_void = seq_buildlets (b_find_locals@argBuildlets@[b_void])
  in
    if function_type = ll.Void then

      (ll.Id call_res, b_void)
    else
      (ll.Id call_res, b_)
  end

and cgFunDecls ll_env cg_ctxt fundecls =
  let
  (* add the decls to the llenv *)
    val cg_ctxt' = foldl (addfundecltoenv ll_env) cg_ctxt fundecls
    (* //add ll code represting the body of the function *)
    val _ = app (emitFunBody ll_env cg_ctxt') fundecls
  in
    (cg_ctxt', id_buildlet)
  end

and codegenTypeDecls ll_env cg_ctxt tdecls =
  let
    val cg_ctxt' = foldl (emitRecordStructure ll_env) cg_ctxt tdecls
  in
    (cg_ctxt', id_buildlet)
  end

and emitFunBody ll_env cg_ctxt
          ({name = name'
          , args = args'
          , aug = { ty = resultTy, locals = fun_locals}
          , body = body'}) =
let
    val {venv = venv, tenv = tenv, locals = locals, lid = lid, level = outerLevel, tyMappings = tm, break_link = llb} = cg_ctxt
    val (outerLocals, _)::_ = locals
    val outerLocalsTy = ll.Ptr(ll.I8)
    val types_list: ll.ty list = outerLocalsTy :: (map ty_to_llty ((#2 o ListPair.unzip) fun_locals))

    val SOME(E.FunEntry({ formals = argTypes
              , result = _
              , gid    = funName
              , sl_type = _
              , level = _ })) = S.look(venv, name')

    val locs_struct = ll.Struct types_list
    val locs_name   = LLEnv.mk_type_name ll_env (S.name(funName))
    val lid         = LLEnv.mk_uid ll_env "locals"

    (* emit the type declaration *)
    val _ = LLEnv.add_named_type ll_env (locs_name, locs_struct)

    (* builder for allocating the locals in the current block *)
    val b_alloca_locals = emit_alloca (lid, ll.Namedt(locs_name))
    (* prepare the params from the args var*)
    val statLqName = S.symbol("statLq")

    val params_list = statLqName::(map (fn {name = pN, escape = _, aug = _} => LLEnv.mk_label_name ll_env (S.name(pN))) args')
    val params_types = outerLocalsTy::(map ty_to_llty argTypes)
    val pairs = ListPair.zip(params_types, params_list) 
    (* get args offsets *)
    val offsets = 0::(map (fn {name = _, escape = _, aug = { ty = _, offset = off}} => off  + 1) args')
    (* format offsets for the emitArgALloc *)
    val offsets_pairs = ListPair.map (fn ((x, y), z) => (x, y, z)) (pairs, offsets)
    val alloca_params = map (emitArgAlloc ll_env locs_name lid) offsets_pairs
    val b_alloca_params = map (fn (_, x) => x) alloca_params
    val b_alloc = seq_buildlets (b_alloca_locals::b_alloca_params)


    val venv' = foldl (pushFieldToEnv (outerLevel + 1)) venv args'
    val cg_ctxt_body = {venv = venv'
                  , tenv = tenv
                  , locals = (locs_name, locs_struct)::locals
                  , lid = lid
                  , level = outerLevel + 1
                , tyMappings = tm
                , break_link = llb}

    (* codegen the rest of the expression *)
    val (oprnd, build_exp) = cgExp ll_env cg_ctxt_body body'

    (* terminate the last (potentially the same as the first) block *)
    val (rt, terminator) = case (( #ty o #aug) body' ) of
                       Ty.UNIT => (ll.Void, ll.Ret (ll.Void, NONE))
                     | t       => (ty_to_llty t,
                                   ll.Ret (ty_to_llty t, SOME oprnd))

    (* make the terminator *)
    val build_ret = emit_term terminator

    (* get the final cfg *)
    val cfg = build_cfg_from_buildlets [  b_alloc
                                        , build_exp
                                        , build_ret ]

    val (fd:ll.fdecl) = { fty = ( params_types, rt), param = params_list, cfg = cfg}
in
  LLEnv.add_fun_decl ll_env (funName, fd)
end

fun cgMain (ll_env: LLEnv.ll_env)
	   { exp:A''.exp, locals: (int * Ty.ty) list } =
  let val ty_: ll.ty list = (* we put I1 as a placeholder *)
	  ll.I1 :: (map ty_to_llty ((#2 o ListPair.unzip) locals))

      val locs_struct = ll.Struct ty_
      val locs_name   = LLEnv.mk_type_name ll_env "tigermain"
      val lid         = LLEnv.mk_uid ll_env "locals"

      val cg_ctxt = {
          venv = E.baseVenv
        , tenv = E.baseTenv
        , locals = [ (locs_name, locs_struct) ]
        , lid = lid
        , level = 0
        , tyMappings = []
        , break_link = (S.symbol "none") : S.symbol
      }

      (* emit the type declaration *)
      val _ = LLEnv.add_named_type ll_env (locs_name, locs_struct)

      (* builder for allocating the locals in the current block *)
      val build_alloca = emit_alloca (lid, ll.Namedt locs_name)

      (* codegen the rest of the expression *)
      val (oprnd, build_exp) = cgExp ll_env cg_ctxt exp

      (* terminate the last (potentially the same as the first) block *)
      val (rt, tr) = case (( #ty o #aug) exp ) of
                         Ty.UNIT => (ll.Void, ll.Ret (ll.Void, NONE))
                       | t       => (ty_to_llty t,
                                     ll.Ret (ty_to_llty t, SOME oprnd))


      (* make the terminator *)
      val build_ret = emit_term tr

      (* put things together to get the final CFG for the main *)

      val cfg = build_cfg_from_buildlets [  build_alloca
                                          , build_exp
                                          , build_ret ]

      (* generate the function declaration *)
      val (fd:ll.fdecl) = { fty = ( [], rt), param = [], cfg = cfg}
  in  LLEnv.add_fun_decl ll_env (S.symbol "tigermain", fd)
  end


(* ------------------------------------------------------------------ *)
(* The rest of the code below does not require any changes            *)
(* though you are free to change it if needed e.g., for debugging     *)

fun newline s = s^"\n"

val runtime_fns =
    let val fns = [ "i8* @allocRecord(i64)"   (* runtime functions *)
		  , "i8* @initArray (i64, i64, i8*)"
		  , "i64 @stringEqual (i8*, i8*)"
		  , "i64 @stringNotEq (i8*, i8*)"
		  , "i64 @stringLess (i8*, i8*)"
		  , "i64 @stringLessEq (i8*, i8*)"
		  , "i64 @stringGreater (i8*, i8*)"
		  , "i64 @stringGreaterEq (i8*, i8*)"


		  , "void @print    (i8*, i8*)"   (* user visible functions; note SL argument *)
		  , "void @flush    (i8*)"
		  , "i8*  @getChar  (i8*)"
		  , "i64  @ord      (i8*, i8*)"
		  , "i8*  @chr      (i8*, i64)"
		  , "i64  @size     (i8*, i8*)"
		  , "i8*  @substring(i8*, i8*, i64, i64)"
		  , "i8*  @concat   (i8*, i8*, i8*)"
		  , "i64  @not      (i8*, i64)"
		  , "void @exit     (i8*, i64)"
		  , "i64  @exponent (i64, i64)"
		  ]
	fun mkDeclare s = "declare " ^ s ^ "\n"
    in String.concat (map mkDeclare fns)
    end

val target_triple =
  case LocalOS.os of
     LocalOS.Darwin => "target triple = \"x86_64-apple-macosx10.12.0\""
   | LocalOS.Linux  => "target triple = \"x86_64-pc-linux-gnu\""


fun codegen_prog (aug_offset as { exp:A''.exp, locals: (int * Ty.ty) list }) =
  let val ll_env  = LLEnv.init_ll_env ()
      val _    = cgMain ll_env aug_offset
      val prog = LLEnv.prog_of_env ll_env
  in prog
  end

(* a wrapper around codegen_prog *)
fun codegen_ll aug_offset =
  let val prog = codegen_prog aug_offset
      val s = ll.string_of_prog prog
      val s' = s ^ "\n" ^ runtime_fns ^ (newline target_triple)
  in s'
  end

end (* struct LLCodegen *)
